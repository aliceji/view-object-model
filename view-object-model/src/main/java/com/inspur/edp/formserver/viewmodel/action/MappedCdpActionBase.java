

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.action;

import java.io.Serializable;

/**
 * The Definition Of The Parameter With Component
 */
public abstract class MappedCdpActionBase extends ViewModelAction implements Serializable {
    ///#region 属性
    private String componentEntityId = "";

    /**
     * 对应构件实体Id
     */
    public String getComponentEntityId() {
        return componentEntityId;
    }

    public void setComponentEntityId(String value) {
        this.componentEntityId = value;
    }

    /**
     * 对应构件实体包名
     */
    private String privateComponentPkgName;

    public String getComponentPkgName() {
        return privateComponentPkgName;
    }

    public void setComponentPkgName(String value) {
        privateComponentPkgName = value;
    }

    private boolean privateIsGenerateComponent;

    public boolean getIsGenerateComponent() {
        return privateIsGenerateComponent;
    }

    public void setIsGenerateComponent(boolean value) {
        privateIsGenerateComponent = value;
    }

    ///#endregion

    ///#region 方法

    /**
     * 当前对象是否等于同一类型的另一个对象。
     *
     * @param other 与此对象进行比较的对象。
     * @return 如果当前对象等于 other 参数，则为 true；否则为 false。
     */
    @Override
    public boolean equals(ViewModelAction other) {

        MappedCdpActionBase cdpAction = (MappedCdpActionBase) ((other instanceof MappedCdpActionBase) ? other : null);
        if (cdpAction == null) {
            return false;
        }
        if (getID().equals(cdpAction.getComponentEntityId()) && getCode().equals(cdpAction.getCode()) && getName().equals(cdpAction.getName()) && getType() == cdpAction.getType() && getComponentPkgName().equals(cdpAction.getComponentPkgName()) && getComponentName().equals(cdpAction.getComponentName()) && getIsGenerateComponent() == cdpAction.getIsGenerateComponent() && getComponentEntityId().equals(cdpAction.getComponentEntityId())) {
            return true;
        }

        return false;
    }

    /**
     * 重载ToString方法
     *
     * @return 描述
     */
    @Override
    public String toString() {
        return String.format("[VM action] ID:%1$s, Code:%2$s, Name:%3$s", getID(), getCode(), getName());
    }

    ///#endregion
}
