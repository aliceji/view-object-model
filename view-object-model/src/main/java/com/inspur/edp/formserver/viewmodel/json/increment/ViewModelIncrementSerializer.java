

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.increment;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.entity.increment.CommonEntityIncrement;
import com.inspur.edp.cef.designtime.api.entity.increment.json.GspDataTypeIncrementSerializer;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.entity.increment.CommonModelIncrement;
import com.inspur.edp.das.commonmodel.json.increment.CommonModelIncrementSerializer;
import com.inspur.edp.das.commonmodel.json.increment.CommonObjectIncrementSerializer;
import com.inspur.edp.formserver.viewmodel.increment.ViewModelIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.HelpConfigIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.VoActionIncrement;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import lombok.var;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelIncrementSerializer extends CommonModelIncrementSerializer {
    @Override
    protected CommonObjectIncrementSerializer getObjectIncrementSerializer() {
        return new ViewObjectIncrementSerializer();
    }

    @Override
    protected void writeExtendInfo(CommonModelIncrement value, JsonGenerator gen) {
        ViewModelIncrement increment = (ViewModelIncrement)value;
        writeHelpConfigs(increment.getValueHelpConfigs(), gen);
        writeVoActions(increment.getActions(), gen);
    }

    private void writeHelpConfigs(HashMap<String, HelpConfigIncrement> helpConfigs, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, ViewModelJsonConst.HelpIncrements);
        SerializerUtils.WriteStartArray(gen);
        for (var item : helpConfigs.entrySet()) {
            try {
                SerializerUtils.writeStartObject(gen);
                SerializerUtils.writePropertyValue(gen, CefNames.Id, item.getKey());

                SerializerUtils.writePropertyName(gen, ViewModelJsonConst.HelpIncrement);
                HelpConfigIncrementSerializer serializer = new HelpConfigIncrementSerializer();
                serializer.serialize(item.getValue(), gen, null);
                SerializerUtils.writeEndObject(gen);
            } catch (IOException e) {
                throw new RuntimeException("帮助配置信息增量序列化失败", e);
            }
        }
        SerializerUtils.WriteEndArray(gen);
    }

    private void writeVoActions(HashMap<String, VoActionIncrement> actions, JsonGenerator gen){
        if(actions == null)
            return;
        SerializerUtils.writePropertyName(gen, ViewModelJsonConst.Actions);
        SerializerUtils.WriteStartArray(gen);
        for (var item : actions.entrySet()) {
            try {
                SerializerUtils.writeStartObject(gen);
                SerializerUtils.writePropertyValue(gen, CefNames.Id, item.getKey());

                SerializerUtils.writePropertyName(gen, ViewModelJsonConst.Action);
                VoActionIncrementSerializer serializer = new VoActionIncrementSerializer();
                serializer.serialize(item.getValue(), gen, null);
                SerializerUtils.writeEndObject(gen);
            } catch (IOException e) {
                throw new RuntimeException("Vo动作增量序列化失败", e);
            }
        }
        SerializerUtils.WriteEndArray(gen);
    }
}
