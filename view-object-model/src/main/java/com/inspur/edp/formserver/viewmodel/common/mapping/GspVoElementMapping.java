

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.common.mapping;

import com.inspur.edp.formserver.viewmodel.common.ViewModelMapping;

/**
 * The Definition Of View Model ELement Mapping
 *
 * @ClassName: GspVoElementMapping
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspVoElementMapping extends ViewModelMapping
{
	/** 
	 映射节点ID
	 
	*/
	private String targetObjectId;
	public final String getTargetObjectId()
	{
		return targetObjectId;
	}
	public final void setTargetObjectId(String value)
	{
		targetObjectId = value;
	}

	/** 
	 映射字段ID
	 
	*/
	private String targetElementId;
	public final String getTargetElementId()
	{
		return targetElementId;
	}
	public final void setTargetElementId(String value)
	{
		targetElementId = value;
	}

	/** 
	 数据源（be或qo）
	 
	*/
	private GspVoElementSourceType sourceType =
			GspVoElementSourceType.forValue(0);
	public final GspVoElementSourceType getSourceType()
	{
		return sourceType;
	}
	public final void setSourceType(GspVoElementSourceType value)
	{
		sourceType = value;
	}

	public GspVoElementMapping clone() {
		return (GspVoElementMapping)super.clone();
	}

}
