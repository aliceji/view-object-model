

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs;

public class ChangingVoFieldDataTypeEventArgs extends AbstractVofieldEventArgs {

  protected String newDateType;
  protected String originalDataType;

  public ChangingVoFieldDataTypeEventArgs() {
  }

  public ChangingVoFieldDataTypeEventArgs(String newDateType, String originalDataType) {
    this.newDateType = newDateType;
    this.originalDataType = originalDataType;
  }

  public String getNewDateType() {
    return newDateType;
  }

  public void setNewDateType(String newDateType) {
    this.newDateType = newDateType;
  }

  public String getOriginalDataType() {
    return originalDataType;
  }

  public void setOriginalDataType(String originalDataType) {
    this.originalDataType = originalDataType;
  }
}
