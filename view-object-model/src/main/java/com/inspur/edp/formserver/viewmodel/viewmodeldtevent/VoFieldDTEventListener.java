

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.viewmodeldtevent;

import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.ChangingVoFieldDataTypeEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.ChangingVoFieldLabelIdEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.ChangingVoFieldObjectTypeEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.RemovingVoFieldEventArgs;
/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VoFieldDTEventListener implements IVoFieldDTEventListener{


  @Override
  public ChangingVoFieldDataTypeEventArgs changingVoFieldDataType(
      ChangingVoFieldDataTypeEventArgs args) {
    return null;
  }

  @Override
  public ChangingVoFieldLabelIdEventArgs changingVoFieldLabelId(
      ChangingVoFieldLabelIdEventArgs args) {
    return null;
  }

  @Override
  public ChangingVoFieldObjectTypeEventArgs changingVoFieldObjectType(
      ChangingVoFieldObjectTypeEventArgs args) {
    return null;
  }

  @Override
  public RemovingVoFieldEventArgs removingVoField(RemovingVoFieldEventArgs args) {
    return null;
  }
}
