

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.object;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.json.element.CmElementSerializer;
import com.inspur.edp.das.commonmodel.json.object.CmObjectSerializer;
import com.inspur.edp.formserver.viewmodel.GspViewObject;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import com.inspur.edp.formserver.viewmodel.json.element.ViewElementSerializer;
import com.inspur.edp.formserver.viewmodel.json.mapping.GspQoObjectMappingSerializer;
import com.inspur.edp.formserver.viewmodel.json.mapping.GspVoObjectMappingSerializer;
import lombok.var;

/**
 * The Josn Serializer Of View Object
 *
 * @ClassName: ViewObjectSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewObjectSerializer extends CmObjectSerializer {

    public ViewObjectSerializer(){}
    public ViewObjectSerializer(boolean full){
        super(full);
        isFull= full;
    }
    @Override
    protected CmElementSerializer gspCommonDataTypeSerializer() {
        return new ViewElementSerializer(isFull);
    }

    @Override
    protected void writeExtendObjectBaseProperty(JsonGenerator jsonGenerator, IGspCommonObject iGspCommonObject) {

    }

    @Override
    protected void writeExtendObjectSelfProperty(JsonGenerator writer, IGspCommonObject commonObject) {
        GspViewObject viewObject = (GspViewObject) commonObject;
        if(isFull||viewObject.getIsBeckendOnly())
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.IsBeckendOnly, viewObject.getIsBeckendOnly());
        if(isFull||(viewObject.getPrimaryKey()!=null&&!"".equals(viewObject.getPrimaryKey()))){
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.PrimaryKey, viewObject.getPrimaryKey());}
        if(isFull||viewObject.getDefaultPageSize()!=0){
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.DefaultPageSize, viewObject.getDefaultPageSize());}
        if(isFull||(viewObject.getOrderbyCondition()!=null&&!"".equals(viewObject.getOrderbyCondition()))){
        SerializerUtils.writePropertyValue(writer,ViewModelJsonConst.OrderbyCondition,viewObject.getOrderbyCondition());}
        if(isFull||(viewObject.getFilterCondition()!=null&&!"".equals(viewObject.getFilterCondition()))){
        SerializerUtils.writePropertyValue(writer,ViewModelJsonConst.FilterCondition,viewObject.getFilterCondition());}
        writeMapping(writer, viewObject);
        WriteExtendProperties(writer, viewObject);
    }

    private void writeMapping(JsonGenerator writer, GspViewObject vo) {
        if (vo.getMapping() == null)
            return;

        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.Mapping);
        switch (vo.getMapping().getSourceType()) {
            case QoObject:
                GspQoObjectMappingSerializer qoConvertor = new GspQoObjectMappingSerializer(isFull);
                qoConvertor.serialize(vo.getMapping(), writer,  null);
                break;
            default:
                GspVoObjectMappingSerializer convertor = new GspVoObjectMappingSerializer(isFull);
                convertor.serialize(vo.getMapping(), writer, null);
                break;
        }
    }

    private void WriteExtendProperties(JsonGenerator writer, GspViewObject vo) {
        var dic = vo.getExtendProperties();
        if (isFull||(dic != null && dic.size() > 0)) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ExtendProperties);
            SerializerUtils.writeStartObject(writer);
            for (var item : dic.entrySet()) {
                SerializerUtils.writePropertyValue(writer, item.getKey(), item.getValue());
            }
            SerializerUtils.writeEndObject(writer);
        }
    }

}
