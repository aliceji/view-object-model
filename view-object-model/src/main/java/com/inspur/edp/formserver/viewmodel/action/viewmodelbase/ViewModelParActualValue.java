

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.action.viewmodelbase;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.inspur.edp.cef.spi.jsonser.base.StringUtils;

/**
 * The Definition Of View Model Parameter Action Value.In A View Model Action,The Parameter Value From The Component Can Be Transfer From Outer Invoke Or Fixed
 * When The Value Is Fixed, The Actual Value Is Required.
 *
 * @ClassName: ViewModelParActualValue
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class  ViewModelParActualValue {

  private String value = "";

  @JsonProperty("Value")
  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  private boolean hasValue = false;

  @JsonProperty("HasValue")
  @Deprecated
  public boolean getHasValue() {
    if(hasValue && "".equals(value))
      hasValue = false;
    return hasValue;
  }

  @Deprecated
  public void setHasValue(boolean value) {
    hasValue = value;
  }

  private boolean enable = false;

  @JsonProperty("Enable")
  public boolean isEnable() {
    return enable;
  }

  public void setEnable(boolean enable) {
    this.enable = enable;
  }

  private ViewModelParActualValueType valueType = ViewModelParActualValueType.Constant;

  @JsonProperty("ValueType")
  public ViewModelParActualValueType getValueType() {
    return valueType;
  }

  public void setValueType(ViewModelParActualValueType value) {
    this.valueType = value;
  }


}
