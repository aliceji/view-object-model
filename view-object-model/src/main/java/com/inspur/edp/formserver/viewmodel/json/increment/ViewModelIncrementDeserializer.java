

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.increment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.inspur.edp.cef.designtime.api.entity.increment.CommonEntityIncrement;
import com.inspur.edp.cef.designtime.api.entity.increment.json.GspDataTypeIncrementDeserializer;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.das.commonmodel.entity.increment.CommonModelIncrement;
import com.inspur.edp.das.commonmodel.json.increment.CommonModelIncrementDeserializer;
import com.inspur.edp.das.commonmodel.json.increment.CommonObjectIncrementDeserializer;
import com.inspur.edp.formserver.viewmodel.increment.ViewModelIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.HelpConfigIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.VoActionIncrement;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;

import java.io.IOException;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelIncrementDeserializer extends CommonModelIncrementDeserializer {
    @Override
    protected CommonObjectIncrementDeserializer getObjectIncrementDeserializer() {
        return new ViewObjectIncrementDeserializer();
    }

    protected CommonModelIncrement createCommonModelIncrement() {
        return new ViewModelIncrement();
    }

    @Override
    protected void readExtendInfo(CommonModelIncrement value, JsonNode node) {
        ViewModelIncrement increment = (ViewModelIncrement)value;
        JsonNode valueConfigIncrements = node.get(ViewModelJsonConst.HelpIncrements);
        if(valueConfigIncrements != null)
            readValueConfigs(increment, valueConfigIncrements);
        JsonNode actionIncrements = node.get(ViewModelJsonConst.Actions);
        if(actionIncrements != null)
            readActions(increment, actionIncrements);

    }

    private void readValueConfigs(ViewModelIncrement increment, JsonNode node){

        ArrayNode array = trans2Array(node);
        for (JsonNode childNode : array){
            HelpConfigIncrementDeserializer serializer = new HelpConfigIncrementDeserializer();
            ObjectMapper mapper = new ObjectMapper();
            SimpleModule module = new SimpleModule();
            module.addDeserializer(HelpConfigIncrement.class, serializer);
            mapper.registerModule(module);
            try {
                String key = childNode.get(CefNames.Id).textValue();
                JsonNode valueNode = childNode.get(ViewModelJsonConst.HelpIncrement);

                HelpConfigIncrement helpIncrement = mapper.readValue(mapper.writeValueAsString(valueNode), HelpConfigIncrement.class);
                helpIncrement.setElementId(key);
                increment.getValueHelpConfigs().put(key, helpIncrement);
            }  catch (IOException e) {
                throw new RuntimeException("CommonEntityIncrement反序列化失败", e);
            }
        }

    }

    private void readActions(ViewModelIncrement increment, JsonNode node){
        ArrayNode array = trans2Array(node);
        for (JsonNode childNode : array){
            VoActionIncrementDeserializer serializer = new VoActionIncrementDeserializer();
            ObjectMapper mapper = new ObjectMapper();
            SimpleModule module = new SimpleModule();
            module.addDeserializer(VoActionIncrement.class, serializer);
            mapper.registerModule(module);
            try {
                String key = childNode.get(CefNames.Id).textValue();
                JsonNode valueNode = childNode.get(ViewModelJsonConst.Action);

                VoActionIncrement voActionIncrement = mapper.readValue(mapper.writeValueAsString(valueNode), VoActionIncrement.class);
                voActionIncrement.setActionId(key);
                increment.getActions().put(key, voActionIncrement);
            }  catch (IOException e) {
                throw new RuntimeException("VoActionIncrement反序列化失败", e);
            }
        }
    }

}
