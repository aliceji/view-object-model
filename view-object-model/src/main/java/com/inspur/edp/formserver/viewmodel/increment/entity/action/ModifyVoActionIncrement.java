

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.increment.entity.action;

import com.inspur.edp.cef.designtime.api.increment.IncrementType;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;

public class ModifyVoActionIncrement extends VoActionIncrement {

    private ViewModelAction action;

    @Override
    public IncrementType getIncrementType() {
        return IncrementType.Modify;
    }

    public ViewModelAction getAction() {
        return action;
    }

    public void setAction(ViewModelAction action) {
        this.action = action;
    }
}
