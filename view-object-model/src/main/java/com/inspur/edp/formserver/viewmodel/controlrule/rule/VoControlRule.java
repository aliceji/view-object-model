

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.controlrule.rule;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.das.commonmodel.controlrule.CmControlRule;
import com.inspur.edp.das.commonmodel.controlrule.CmEntityControlRule;
import com.inspur.edp.formserver.viewmodel.controlrule.rule.parser.VoControlRuleParser;
import com.inspur.edp.formserver.viewmodel.controlrule.rule.serializer.VoRuleSerializer;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.VoControlRuleDefNames;

@JsonSerialize(using = VoRuleSerializer.class)
@JsonDeserialize(using = VoControlRuleParser.class)
public class VoControlRule extends CmControlRule {
    protected final CmEntityControlRule createMainEntityControlRule() {
        return new VoObjControlRule();
    }

    public ControlRuleItem getAddCustomActonControlRule() {
        return super.getControlRule(VoControlRuleDefNames.AddCustomAction);
    }

    public void setAddCustomActonControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(VoControlRuleDefNames.AddCustomAction, ruleItem);
    }

    public ControlRuleItem getAddVariableControlRule() {
        return super.getControlRule(VoControlRuleDefNames.AddVariableDtm);
    }

    public void setAddVariableControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(VoControlRuleDefNames.AddVariableDtm, ruleItem);
    }
}
