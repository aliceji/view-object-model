

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.common.HelpExtendAction;
import com.inspur.edp.formserver.viewmodel.common.ValueHelpConfig;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import com.inspur.edp.formserver.viewmodel.json.operation.VmActionCollectionSerializer;

/**
 * The Josn Serializer Of View Model Help Configuration
 *
 * @ClassName: ValueHelpConfigSerizlizer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ValueHelpConfigSerizlizer extends JsonSerializer<ValueHelpConfig> {

  protected boolean isFull = true;
  public ValueHelpConfigSerizlizer(){}
  public ValueHelpConfigSerizlizer(boolean full){
    isFull = full;
  }

  @Override
  public void serialize(ValueHelpConfig helpConfig, JsonGenerator writer,
      SerializerProvider serializers) {
    SerializerUtils.writeStartObject(writer);
    SerializerUtils
        .writePropertyValue(writer, ViewModelJsonConst.ElementId, helpConfig.getElementId());
    SerializerUtils
        .writePropertyValue(writer, ViewModelJsonConst.HelperId, helpConfig.getHelperId());
    SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.FilterExpression,
        helpConfig.getFilterExpression());
    SerializerUtils
        .writePropertyValue(writer, CefNames.CustomizationInfo, helpConfig.getCustomizationInfo());
    SerializerUtils.writePropertyName(writer,ViewModelJsonConst.EnableCustomHelpAuth);
    SerializerUtils.writePropertyValue_boolean(writer,helpConfig.getEnableCustomHelpAuth());
    writeHelpExtend(writer, helpConfig.getHelpExtend());
    SerializerUtils.writeEndObject(writer);
  }

  private void writeHelpExtend(JsonGenerator writer, HelpExtendAction helpExtend) {
    SerializerUtils.writePropertyName(writer, ViewModelJsonConst.HelpExtend);

    SerializerUtils.writeStartObject(writer);
    if (helpExtend != null) {
      SerializerUtils.writePropertyName(writer, ViewModelJsonConst.BeforeHelp);
      getVMActionCollectionConvertor().serialize(helpExtend.getBeforeHelp(), writer, null);
    }
    SerializerUtils.writeEndObject(writer);
  }

  private VmActionCollectionSerializer getVMActionCollectionConvertor() {
    return new VmActionCollectionSerializer(isFull);
  }

}
