

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.mapping;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.GspViewObject;
import com.inspur.edp.formserver.viewmodel.common.MappingType;
import com.inspur.edp.formserver.viewmodel.common.ViewModelMapping;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;

/**
 * The Json Serializer Of View Model Mapping
 *
 * @ClassName: ViewModelMappingSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelMappingSerializer extends JsonSerializer<ViewModelMapping> {

    protected boolean isFull = true;
    public ViewModelMappingSerializer(){}
    public ViewModelMappingSerializer(boolean full){
        isFull = full;
    }
    @Override
    public void serialize(ViewModelMapping mapping, JsonGenerator writer, SerializerProvider serializers) {
        if (mapping == null)
            return;
        SerializerUtils.writeStartObject(writer);
        if(isFull||mapping.getMapType()!= MappingType.ViewObject){
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.MapType, mapping.getMapType().toString());
        }
        if(isFull||mapping.getTargetMetadataId()!=null&&!"".equals(mapping.getTargetMetadataId())){
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.TargetMetadataId, mapping.getTargetMetadataId());
        }
        if(isFull||mapping.getTargetMetadataPkgName()!=null&&!"".equals(mapping.getTargetMetadataPkgName())) {
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.TargetMetadataPkgName, mapping.getTargetMetadataPkgName());
        }
        if(isFull||mapping.getTargetObjId()!=null&&!"".equals(mapping.getTargetObjId())){
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.TargetObjId, mapping.getTargetObjId());
        }
        //WritePropertyValue(writer, ViewModelJsonConst.TargetMetadataType, mapping.TargetMetadataType.ToString());
        //扩展模型属性
        writeExtendMappingProperty(writer, mapping);
        SerializerUtils.writeEndObject(writer);
    }

    protected void writeExtendMappingProperty(JsonGenerator writer, ViewModelMapping mapping) {
    }
}
