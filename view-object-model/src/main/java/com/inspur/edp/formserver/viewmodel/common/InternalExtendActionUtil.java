

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.common;

import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.action.internalexternalaction.*;

/**
 * The Tool Of Internal Extend Action
 *
 * @ClassName: InternalExtendActionUtil
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class InternalExtendActionUtil {
	//Internal ExtendActionIDs
	public static final String DeleteActionId = "9a17e935-7366-489d-b110-0ae103e5648e";
	public static final String ModifyActionId = "47dd3752-72a3-4c56-81c0-ae8ccfe5eb98";
	public static final String QueryActionId = "6fe68bfa-7c1b-4d6b-a7ef-14654168ae75";
	public static final String CreateActionId = "52479451-8e22-4751-8684-80489ce5786b";
	public static final String RetrieveActionId = "7a02f472-5bbd-424b-9d9e-f82e3f9f448e";

	public static final String DataReversalMappingActionId = "991bf216-f55b-40bf-bb42-1b831b6ef3e9";
	public static final String DataMappingActionId = "42221ca3-9ee4-40af-89d2-ff4c8b466ac3";
	public static final String ChangesetReversalMappingActionId = "301c5991-a32d-4221-88bf-8c9d07bdd884";
	public static final String ChangesetMappingActionId = "5798f884-c222-47f4-8bbe-685c7013dee4";
	public static final String MultiDeleteActionId="7b1c3c4l-t1a4-4dyc-b75b-7695hcb3we7e";

	public java.util.ArrayList<String> InternalActionIds = new java.util.ArrayList<String>(java.util.Arrays.asList(new String[]{DeleteActionId, ModifyActionId, QueryActionId, CreateActionId, RetrieveActionId, DataReversalMappingActionId, DataMappingActionId, ChangesetReversalMappingActionId, ChangesetMappingActionId}));

	public final MappedCdpAction GetInternalExtendActionById(String actionId) {
		if (!InternalActionIds.contains(actionId)) {
			throw new RuntimeException(String.format("无id='%1$s'的内置扩展操作。", actionId));
		}

		switch (actionId) {
			case DeleteActionId:
				return new DeleteAction();
			case ModifyActionId:
				return new ModifyAction();
			case QueryActionId:
				return new QueryAction();
			case CreateActionId:
				return new CreateAction();
			case RetrieveActionId:
				return new RetrieveAction();
			case DataMappingActionId:
				return new DataMappingAction();
			case DataReversalMappingActionId:
				return new DataReversalMappingAction();
			case ChangesetReversalMappingActionId:
				return new ChangesetReversalMappingAction();
			case ChangesetMappingActionId:
				return new ChangesetMappingAction();
			case MultiDeleteActionId:
				return new MultiDeleteAction();
			default:
				throw new RuntimeException(String.format("无id='%1$s'的内置扩展操作。", actionId));
		}
	}
}
