

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.action.internalexternalaction.*;
import com.inspur.edp.formserver.viewmodel.collection.VMActionCollection;
import com.inspur.edp.formserver.viewmodel.common.InternalExtendActionUtil;
import com.inspur.edp.formserver.viewmodel.dataextendinfo.VoDataExtendInfo;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import com.inspur.edp.formserver.viewmodel.json.operation.VmActionCollectionDeserializer;

/**
 * The  Josn Deserializer Of View Model Data Extendion Info
 *
 * @ClassName: VoDataExtendInfoDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VoDataExtendInfoDeserializer extends JsonDeserializer<VoDataExtendInfo> {
    @Override
    public VoDataExtendInfo deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        VoDataExtendInfo info = new VoDataExtendInfo();
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readVoDataExtendInfo(jsonParser, propName, info);
        }
        SerializerUtils.readEndObject(jsonParser);
        return info;
    }

    private void readVoDataExtendInfo(JsonParser reader, String propName, VoDataExtendInfo info) {
        switch (propName) {
            case ViewModelJsonConst.DataMappingActions:
                info.setDataMappingActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.BeforeQueryActions:
                info.setBeforeQueryActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.QueryActions:
                info.setQueryActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.AfterQueryActions:
                info.setAfterQueryActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.BeforeRetrieveActions:
                info.setBeforeRetrieveActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.RetrieveActions:
                info.setRetrieveActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.AfterRetrieveActions:
                info.setAfterRetrieveActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.BeforeModifyActions:
                info.setBeforeModifyActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.ModifyActions:
                info.setModifyActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.AfterModifyActions:
                info.setAfterModifyActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.ChangesetMappingActions:
                info.setChangesetMappingActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.BeforeCreateActions:
                info.setBeforeCreateActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.CreateActions:
                info.setCreateActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.AfterCreateActions:
                info.setAfterCreateActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.BeforeDeleteActions:
                info.setBeforeDeleteActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.DeleteActions:
                info.setDeleteActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.AfterDeleteActions:
                info.setAfterDeleteActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.BeforeSaveActions:
                info.setBeforeSaveActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.DataReversalMappingActions:
                info.setDataReversalMappingActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.AfterSaveActions:
                info.setAfterSaveActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.ChangesetReversalMappingActions:
                info.setChangesetReversalMappingActions(readVMActions(reader));
                break;
                //TODO Java版暂不支持vo扩展动作批量删除及前后
            case ViewModelJsonConst.BeforeMultiDeleteActions:
                info.setBeforeMultiDeleteActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.MultiDeleteActions:
                info.setMultiDeleteActions(readVMActions(reader));
                break;
            case ViewModelJsonConst.AfterMultiDeleteActions:
                info.setAfterMultiDeleteActions(readVMActions(reader));
                break;
            default:
                throw new RuntimeException(String.format("未定义的VoDataExtendInfo属性：'%1$s'", propName));
        }
    }

    private VMActionCollection readVMActions(JsonParser jsonParser) {
        VmActionCollectionDeserializer deserializer = new VmActionCollectionDeserializer();
        VMActionCollection collection = deserializer.deserialize(jsonParser, null);
        dealWithInternalExtendActions(collection);
        return collection;
    }

    private InternalExtendActionUtil extendActionUtil = new InternalExtendActionUtil();

    private void dealWithInternalExtendActions(VMActionCollection actions) {
        if (actions.size() > 0) {
            ViewModelAction action = actions.get(0);
            if (extendActionUtil.InternalActionIds.contains(action.getID())) {
                ViewModelAction newAction = extendActionUtil.GetInternalExtendActionById(action.getID());
                actions.removeAt(0);
                actions.add(0, newAction);
            }
        }
    }

}
