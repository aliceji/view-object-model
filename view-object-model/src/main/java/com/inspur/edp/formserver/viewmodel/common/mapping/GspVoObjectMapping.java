

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.common.mapping;

import com.inspur.edp.formserver.viewmodel.common.ViewModelMapping;

/**
 * The Definition Of View Model Object Mapping
 *
 * @ClassName: GspVoObjectMapping
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspVoObjectMapping extends ViewModelMapping {
    private GspVoObjectSourceType type =
            GspVoObjectSourceType.forValue(0);

    /**
     * 数据源（be或qo或vo）
     */
    public GspVoObjectSourceType getSourceType() {
        return type;
    }

    public void setSourceType(GspVoObjectSourceType value) {
        if (value == GspVoObjectSourceType.QoObject) {
            throw new RuntimeException("Qo类型映射，请使用GspQoObjectMapping。");
        }
        type = value;
    }

    public GspVoObjectMapping clone() {
        return (GspVoObjectMapping)super.clone();
    }
}
