

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.controlrule.ruledefine;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefinition;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleValue;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeRuleNames;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmEntityControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmEntityRuleNames;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmRuleNames;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.parser.VoObjectControlRuleDefParser;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.serializer.VoObjectControlRuleDefSerializer;

@JsonSerialize(using = VoObjectControlRuleDefSerializer.class)
@JsonDeserialize(using = VoObjectControlRuleDefParser.class)
public class VoObjectControlRuleDef extends CmEntityControlRuleDef {
    public VoObjectControlRuleDef(ControlRuleDefinition parentRuleDefinition) {
        super(parentRuleDefinition);
        super.setRuleObjectType(VoObjectRuleDefNames.VoObjectRuleObjectType);
        init();
    }

    private void init(){

        ControlRuleDefItem nameRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CmRuleNames.Name);
                this.setRuleDisplayName("名称");
                this.setDescription("节点名称");
                this.setDefaultRuleValue(ControlRuleValue.Allow);
            }
        };
        setNameControlRule(nameRule);

        ControlRuleDefItem addChildEntityRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CmEntityRuleNames.AddChildEntity);
                this.setRuleDisplayName("添加子节点");
                this.setDescription("添加子节点");
                this.setDefaultRuleValue(ControlRuleValue.Allow);
            }
        };
        setAddChildEntityControlRule(addChildEntityRule);

        ControlRuleDefItem modifyChildEntityRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CmEntityRuleNames.ModifyChildEntities);
                this.setRuleDisplayName("修改子节点信息");
                this.setDescription("修改子节点信息");
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setModifyChildEntitiesControlRule(modifyChildEntityRule);

        ControlRuleDefItem addElementRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonDataTypeRuleNames.AddField);
                this.setRuleDisplayName("添加字段");
                this.setDescription("添加字段");
                this.setDefaultRuleValue(ControlRuleValue.Allow);
            }
        };
        setAddFieldControlRule(addElementRule);

        ControlRuleDefItem modifyElementRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonDataTypeRuleNames.ModifyFields);
                this.setRuleDisplayName("修改字段信息");
                this.setDescription("修改字段信息");
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setModifyFieldsControlRule(modifyElementRule);

//        BeEntityControlRuleDef objectRuleDef = new BeEntityControlRuleDef(this);
//        getChildControlRules().put(CommonModelNames.MainObject, objectRuleDef);
        VoFieldControlRuleDef fieldRuleDef = new VoFieldControlRuleDef(this);
        getChildControlRules().put(CommonModelNames.Element, fieldRuleDef);
    }
}
