

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.action;

import java.io.Serializable;

/**
 * The Definition Of Action Format Parameter
 *
 * @ClassName: ActionFormatParameter
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ActionFormatParameter implements Cloneable, Serializable
{
		///#region 属性

	/** 
	 ID
	 
	*/
	private String privateID;
	public final String getID()
	{

		return privateID;
	}
	public final void setID(String value)
	{

		privateID = value;
	}

	/** 
	 Code
	 
	*/
	private String privateCode;
	public final String getCode()
	{
		return privateCode;
	}
	public final void setCode(String value)
	{
		privateCode = value;
	}

	/** 
	 Name
	 
	*/
	private String privateName;
	public final String getName()
	{
		return privateName;
	}
	public final void setName(String value)
	{
		privateName = value;
	}

		///#endregion

		///#region 方法

	/** 
	 克隆
	 
	 @return Action执行参数
	*/
	public final ActionFormatParameter clone()
	{
		Object tempVar = null;
		try {
			tempVar = super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		ActionFormatParameter obj = (ActionFormatParameter)((tempVar instanceof ActionFormatParameter) ? tempVar : null);

		return obj;
	}

		///#endregion
}
