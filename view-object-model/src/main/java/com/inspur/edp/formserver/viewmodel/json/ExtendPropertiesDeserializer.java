

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.util.HashMap;
/**
 * The Josn Deserializer Of View Model Extend Properties
 *
 * @ClassName: ExtendPropertiesDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ExtendPropertiesDeserializer extends JsonDeserializer<HashMap<String, String>> {
    @Override
    public HashMap<String, String> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        HashMap<String, String> map = new HashMap<>();
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
            String key = SerializerUtils.readPropertyName(jsonParser);
            String value = SerializerUtils.readPropertyValue_String(jsonParser);
            map.put(key, value);
        }
        SerializerUtils.readEndObject(jsonParser);
        return map;
    }
}
