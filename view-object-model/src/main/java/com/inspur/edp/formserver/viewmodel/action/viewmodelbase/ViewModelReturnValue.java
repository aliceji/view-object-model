

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.action.viewmodelbase;



import com.inspur.edp.formserver.viewmodel.common.VMParameterMode;

/**
 * The Definition Of View Model Return Value
 *
 * @ClassName: ViewModelReturnValue
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelReturnValue extends ViewModelParameter
{
	/** 
	 参数名
	 
	*/
	 @Override
	 public String getParamName(){
	 	return null;
	 }
	@Override
	public void setParamName(String value)
	{

	}

	/** 
	 参数模式
	 
	*/
	 @Override
     public VMParameterMode getMode(){
	 	return VMParameterMode.OUT;
	 }
	@Override
	public void setMode(VMParameterMode value)
	{
	}
}
