

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;

/**
 * The Definition Of View Model Template Info
 *
 * @ClassName: TemplateVoInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class TemplateVoInfo {

	private String templateVoServiceUnit;

	/**
	 * 模板VO信息
	 */
	@JsonProperty(ViewModelJsonConst.TemplateVoServiceUnit)
	public final String getTemplateVoServiceUnit() {
		return templateVoServiceUnit;
	}

	public void setTemplateVoServiceUnit(String value) {
		this.templateVoServiceUnit = value;
	}

	private String templateVoPkgName;

	/**
	 * 模板VO信息
	 */
	@JsonProperty(ViewModelJsonConst.TemplateVoPkgName)
	public final String getTemplateVoPkgName() {
		return templateVoPkgName;
	}

	public void setTemplateVoPkgName(String value) {
		this.templateVoPkgName = value;
	}

	private String templateVoId;

	/**
	 * 模板VO信息
	 */
	@JsonProperty(ViewModelJsonConst.TemplateVoId)
	public final String getTemplateVoId() {
		return templateVoId;
	}

	public void setTemplateVoId(String value) {
		this.templateVoId = value;
	}
}
