

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//package com.inspur.edp.formserver.viewmodel.Interface;
package com.inspur.edp.formserver.viewmodel.action;

import com.inspur.edp.formserver.viewmodel.action.viewmodelbase.ViewModelParActualValue;
import com.inspur.edp.formserver.viewmodel.common.VMCollectionParameterType;
import com.inspur.edp.formserver.viewmodel.common.VMParameterMode;
import com.inspur.edp.formserver.viewmodel.common.VMParameterType;

/**
 * The Definition Of View Model Parameter
 *
 * @ClassName: IViewModelParameter
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface IViewModelParameter
{

	/**
	 * @return The Id Of The Parameter
	 */
	String getID();

	/**
	 * @param value The Id Of The Parameter
	 */
	void setID(String value);

	/**
	 * @return The Code Of The Parameter
	 */
	String getParamCode();

	/**
	 * @param value The Code Of The Parameter
	 */
	void setParamCode(String value);

	/**
	 * @return The Name Of The Parameter
	 */
	String getParamName();

	/**
	 * @param value The Name Of The Parameter
	 */
	void setParamName(String value);

	/**
	 * @return The Type Of The Parameter
	 */
	VMParameterType getParameterType();

	/**
	 * @param value The Type Of The Parameter
	 */
	void setParameterType(VMParameterType value);

	/**
	 * @return The Assembly Of The Parameter，It`s Useful For Dotnet
	 */
	String getAssembly();

	/**
	 * @param value The Assembly Of The Parameter，It`s Useful For Dotnet
	 */
	void setAssembly(String value);

	/**
	 * @return The Class Name Of The  Parameter Type
	 */
	String getClassName();

	/**
	 * @param value The Class Name Of The  Parameter Type
	 */
	void setClassName(String value);

	/**
	 * @return The Parameter Mode Of The Parameter,The Default Value Is In
	 */
	VMParameterMode getMode();

	/**
	 * @param value The Parameter Mode Of The Parameter,The Default Value Is In
	 */
	void setMode(VMParameterMode value);

	/**
	 * @return The Description Of The Parameter
	 */
	String getParamDescription();

	/**
	 * @param value The Description Of The Parameter
	 */
	void setParamDescription(String value);

	/**
	 * @return The Collection Type Of The Parameter
	 */
	VMCollectionParameterType getCollectionParameterType();

	/**
	 * @param value The Collection Type Of The Parameter
	 */
	void setCollectionParameterType(VMCollectionParameterType value);

	/**
	 * @return The Actual Value  Of The Parameter,It`s Not Required
	 */
	ViewModelParActualValue getActualValue();

	/**
	 * @param value The Actual Value  Of The Parameter,It`s Not Required
	 */
	void setActualValue(ViewModelParActualValue value);
}
