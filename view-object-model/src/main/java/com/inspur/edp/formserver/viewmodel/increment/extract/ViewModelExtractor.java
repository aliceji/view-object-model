

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.increment.extract;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.das.commonmodel.controlrule.CmControlRule;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmControlRuleDef;
import com.inspur.edp.das.commonmodel.entity.GspCommonModel;
import com.inspur.edp.das.commonmodel.entity.increment.CommonModelIncrement;
import com.inspur.edp.das.commonmodel.entity.increment.extract.CommonModelExtractor;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.collection.VMActionCollection;
import com.inspur.edp.formserver.viewmodel.increment.ViewModelIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.ModifyHelpIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.AddedVoActionIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.DeletedVoActionIncrement;
import com.inspur.edp.formserver.viewmodel.increment.extract.action.ActionIncrementExtractor;
import lombok.var;

import javax.swing.text.StyledEditorKit;

public class ViewModelExtractor extends CommonModelExtractor {

    public ViewModelExtractor(){
        super();
    }

    public ViewModelExtractor(boolean includeAll){
        super(includeAll);
    }
    @Override
    protected void extractExtendInfo(CommonModelIncrement increment, GspCommonModel oldModel, GspCommonModel newModel, CmControlRule rule, CmControlRuleDef def) {
        extractViewModelInfo(increment, (GspViewModel) oldModel, (GspViewModel) newModel, rule, def);
    }

    private void extractViewModelInfo(CommonModelIncrement increment, GspViewModel oldVo, GspViewModel newVo, CmControlRule rule, CmControlRuleDef def) {
        extractHelpConfigs((ViewModelIncrement) increment, oldVo, newVo, rule, def);
        extractVoActions((ViewModelIncrement) increment, oldVo, newVo, rule, def);
    }

    protected CommonModelIncrement createCommonModelIncrement() {
        return new ViewModelIncrement();
    }

    private void extractHelpConfigs(ViewModelIncrement increment, GspViewModel oldVo, GspViewModel newVo, CmControlRule rule, CmControlRuleDef def) {
        var newHelpConfigs = newVo.getValueHelpConfigs();
        var oldHelpConfigs = oldVo.getValueHelpConfigs();
        if (newHelpConfigs.size() < 1 && oldHelpConfigs.size() < 1)
            return;
        var extractor = new HelpIncrementExtractor();

        if (newHelpConfigs.size() < 1) {
            //TODO 暂不支持删除帮助
//            extractAllDeleteIncrement(increment, oldObj, extractor, rule, def);
            return;
        }

//        ArrayList<String> updateObjs = new ArrayList<>();

        for (var newHelpConfig : newHelpConfigs) {
            var oldHelpConfig = oldHelpConfigs.getItem(newHelpConfig.getElementId());
            if (oldHelpConfig != null) {
                if(!oldHelpConfig.getHelperId().equals(newHelpConfig.getHelperId())){
                    ModifyHelpIncrement modifyIncrement = new ModifyHelpIncrement();
                    modifyIncrement.setHelpConfig(newHelpConfig);
                    increment.getValueHelpConfigs().put(newHelpConfig.getElementId(), modifyIncrement);

                }else{
                    //todo 帮助不变，只修改过滤或者帮助前事件
                }
                continue;
            }

            var helpConfigIncrement = extractor.extractorIncrement(oldHelpConfig, newHelpConfig, rule, def);
            if (helpConfigIncrement == null)
                continue;
            increment.getValueHelpConfigs().put(newHelpConfig.getElementId(), helpConfigIncrement);
        }

//        for (var oldChildObj : oldHelpConfigs) {
//            if (updateObjs.contains(oldChildObj.getID()))
//                continue;
//            CmEntityControlRule childObjRule = (CmEntityControlRule)rule.getChildRules().get(CommonModelNames.ChildObject).get(oldChildObj.getID());
//            var childIncrement = extractor.extractorIncrement((GspCommonDataType) oldChildObj, null, childObjRule, def);
//            increment.getChildEntitis().put(oldChildObj.getID(), childIncrement);
//        }

    }

    //region merge Action
    private void extractVoActions(ViewModelIncrement increment, GspViewModel oldVo, GspViewModel newVo, CmControlRule rule, CmControlRuleDef def){

        VMActionCollection oldActions = oldVo.getActions();
        VMActionCollection newActions = newVo.getActions();
        if(oldActions.size() < 1 && newActions.size()< 1)
            return;

        if (newActions.size() < 1) {
            //TODO 暂不支持删除动作
//            extractAllDeleteIncrement(increment, oldObj, extractor, rule, def);
            return;
        }
        var extractor = new ActionIncrementExtractor();
        for(ViewModelAction action : newActions) {
            ViewModelAction oldAction = oldActions.getItem(action.getID());

            var actionIncrement = extractor.extractorIncrement(oldAction, action, rule,def);
            if (actionIncrement == null)
                continue;
            increment.getActions().put(action.getID(), actionIncrement);
        }
    }

    //endregion
}
