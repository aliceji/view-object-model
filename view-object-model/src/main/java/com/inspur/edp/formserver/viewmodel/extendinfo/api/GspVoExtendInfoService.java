

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.extendinfo.api;
import com.inspur.edp.formserver.viewmodel.extendinfo.entity.GspVoExtendInfo;

import java.util.List;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface GspVoExtendInfoService {
    /**
     * 根据ID获取某条Vo扩展信息
     * @param id
     * @return
     */
    GspVoExtendInfo getVoExtendInfo(String id);

    /**
     * 根据configId获取某条Vo扩展信息
     * @param configId
     * @return
     */
    GspVoExtendInfo getVoExtendInfoByConfigId(String configId);

    /**
     * 获取所有BE扩展信息
     * @return
     */
    List<GspVoExtendInfo> getVoExtendInfos();

    /**
     * 保存
     * @param infos
     */
    void saveGspVoExtendInfos(List<GspVoExtendInfo> infos);

    void deleteVoExtendInfo(String id);

    /**
     * 根据BEId获取某条VoID
     * @param Id
     * @return
     */
    List<GspVoExtendInfo> getVoId(String Id);

}
