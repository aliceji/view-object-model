

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.voextendinfo.server.api;

import com.inspur.edp.formserver.viewmodel.extendinfo.entity.GspVoExtendInfo;
import io.iec.edp.caf.rpc.api.annotation.GspServiceBundle;
import io.iec.edp.caf.rpc.api.annotation.RpcParam;
import java.util.List;

/**
 * Vo扩展Rpc服务接口
 *
 * @author hanll02
 */
@GspServiceBundle(applicationName = "runtime", serviceUnitName = "Lcm", serviceName = "GspVoExtendInfoRpcService")
public interface GspVoExtendInfoRpcService {
  /**
   * 根据Vo元数据id获取指定id的Vo扩展信息
   *
   * @param id Vo元数据id
   * @return 指定id的Vo扩展信息
   */
  GspVoExtendInfo getVoExtendInfo(@RpcParam(paramName = "id")String id);

  /**
   * 根据Vo元数据configid获取指定configid的Vo扩展信息
   *
   * @param configId configid
   * @return 指定configid的Vo扩展信息
   */
  GspVoExtendInfo getVoExtendInfoByConfigId(@RpcParam(paramName = "configId")String configId);

  /**
   * 获取所有Be扩展信息
   *
   * @return 所有Be扩展信息列表
   */
  List<GspVoExtendInfo> getVoExtendInfos();

  /**
   * 根据beId获取对应的VoId
   *
   * @param beId Be元数据id
   * @return 指定Be元数据id对应的所有的Vo扩展信息列表
   */
  List<GspVoExtendInfo> getVoId(@RpcParam(paramName = "beId")String beId);
  /**
   * 保存
   *
   * @param infos 要保存的Vo扩展信息
   */
  void saveGspVoExtendInfos(List<GspVoExtendInfo> infos);

  /**
   * 根据id删除数据
   *
   * @param id 要删除的数据id
   */
  void deleteVoExtendInfo(String id);
}
