

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.object;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.das.commonmodel.collection.GspElementCollection;
import com.inspur.edp.das.commonmodel.collection.GspObjectCollection;
import com.inspur.edp.das.commonmodel.entity.GspCommonObject;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.json.element.CmElementDeserializer;
import com.inspur.edp.das.commonmodel.json.object.CmObjectDeserializer;
import com.inspur.edp.formserver.viewmodel.GspViewObject;
import com.inspur.edp.formserver.viewmodel.collection.VMElementCollection;
import com.inspur.edp.formserver.viewmodel.collection.ViewObjectCollection;
import com.inspur.edp.formserver.viewmodel.common.MappingType;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoObjectMapping;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoObjectSourceType;
import com.inspur.edp.formserver.viewmodel.json.ExtendPropertiesDeserializer;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import com.inspur.edp.formserver.viewmodel.json.element.ViewElementDeserializer;
import com.inspur.edp.formserver.viewmodel.json.mapping.GspVoObjectMappingDeserializer;

import java.util.HashMap;

/**
 * The Josn Deserializer Of View Object
 *
 * @ClassName: ViewObjectDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewObjectDeserializer extends CmObjectDeserializer {
    @Override
    protected void beforeCMObjectDeserializer(GspCommonObject commonObject){
        GspViewObject viewObject = (GspViewObject) commonObject;
        viewObject.setIsBeckendOnly(false);
        viewObject.setPrimaryKey("");
        viewObject.setDefaultPageSize(0);
        viewObject.setOrderbyCondition("");
        viewObject.setFilterCondition("");
        viewObject.setExtendProperties(new HashMap<>());
    }

    @Override
    protected CmObjectDeserializer createCmObjectDeserializer() {
        return new ViewObjectDeserializer();
    }

    @Override
    protected GspCommonObject CreateCommonObject() {
        return new GspViewObject();
    }

    @Override
    protected CmElementDeserializer CreateElementDeserializer() {
        return new ViewElementDeserializer();
    }

    @Override
    protected boolean ReadExtendObjectProperty(GspCommonObject commonObject, String propertyName, JsonParser reader) {
        GspViewObject viewObject = (GspViewObject) commonObject;
        boolean hasProperty = true;
        switch (propertyName) {
            case ViewModelJsonConst.IsBeckendOnly:
                viewObject.setIsBeckendOnly(SerializerUtils.readPropertyValue_boolean(reader));
                break;
            case ViewModelJsonConst.PrimaryKey:
                viewObject.setPrimaryKey(SerializerUtils.readPropertyValue_String(reader));
                break;
            case ViewModelJsonConst.Mapping:
                readMapping(reader, viewObject);
                break;
            case ViewModelJsonConst.DefaultPageSize:
                viewObject.setDefaultPageSize(SerializerUtils.readPropertyValue_Integer(reader));
                break;
            case ViewModelJsonConst.ExtendProperties:
                readExtendProperties(reader, viewObject);
                break;
            case ViewModelJsonConst.OrderbyCondition:
                viewObject.setOrderbyCondition(SerializerUtils.readPropertyValue_String(reader));
                break;
            case ViewModelJsonConst.FilterCondition:
                viewObject.setFilterCondition(SerializerUtils.readPropertyValue_String(reader));

            default:
                hasProperty = false;
                break;
        }
        return hasProperty;
    }

    @Override
    protected GspObjectCollection createChildObjectCollection() {
        return new ViewObjectCollection(getGspCommonDataType());
    }

    @Override
    protected GspElementCollection createCommonElementCollection() {
        return new VMElementCollection((GspViewObject) getGspCommonDataType());
    }

    private void readExtendProperties(JsonParser jsonParser, GspViewObject viewObject) {
        ExtendPropertiesDeserializer deserializer = new ExtendPropertiesDeserializer();
        viewObject.setExtendProperties(deserializer.deserialize(jsonParser, null));
    }

    private void readMapping(JsonParser jsonParser, GspViewObject viewObject) {
        GspVoObjectMappingDeserializer deserializer = new GspVoObjectMappingDeserializer();
        GspVoObjectMapping mapping = (GspVoObjectMapping) deserializer.deserialize(jsonParser, null);
        viewObject.setMapping(mapping);
    }
}
