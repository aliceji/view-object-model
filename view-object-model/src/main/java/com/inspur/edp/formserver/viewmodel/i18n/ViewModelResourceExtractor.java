

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.i18n;

import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.das.commonmodel.IGspCommonModel;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.i18n.CommonModelResourceExtractor;
import com.inspur.edp.das.commonmodel.i18n.CommonObjectResourceExtractor;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.GspViewObject;
import com.inspur.edp.formserver.viewmodel.i18n.names.VoResourceDescriptionNames;

public class ViewModelResourceExtractor extends CommonModelResourceExtractor {

    public ViewModelResourceExtractor(GspViewModel model, ICefResourceExtractContext context) {
        super(model, context);
    }

    @Override
    protected final String getModelDescriptionName() {
        return VoResourceDescriptionNames.ViewModel;
    }

    @Override
    protected final CommonObjectResourceExtractor getObjectResourceExtractor(
            ICefResourceExtractContext context,
            CefResourcePrefixInfo modelPrefixInfo,
            IGspCommonObject obj) {
        return new ViewObjResourceExtractor((GspViewObject) obj, context, modelPrefixInfo);
    }

    protected final void extractExtendProperties(IGspCommonModel model) {

    }
}
