

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel;

public class VoThreadLoacl {
    public static final ThreadLocal voThreadLocal = new ThreadLocal();

    public static void set(Context user) { voThreadLocal.set(user);
    }
    public static void unset() {
        voThreadLocal.remove();
    }

    public static Context get() {
        return (Context)voThreadLocal.get();
    }
}
