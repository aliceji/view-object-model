

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.IGspCommonModel;
import com.inspur.edp.das.commonmodel.json.model.CommonModelSerializer;
import com.inspur.edp.das.commonmodel.json.object.CmObjectSerializer;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.VoThreadLoacl;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.collection.VMActionCollection;
import com.inspur.edp.formserver.viewmodel.common.InternalExtendActionUtil;
import com.inspur.edp.formserver.viewmodel.common.TemplateVoInfo;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import com.inspur.edp.formserver.viewmodel.json.mapping.ViewModelMappingSerializer;
import com.inspur.edp.formserver.viewmodel.json.object.ViewObjectSerializer;
import com.inspur.edp.formserver.viewmodel.json.operation.VmActionCollectionSerializer;
import lombok.var;

/**
 * The Josn Serializer Of View Model Definition
 *
 * @ClassName: ViewModelSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelSerializer extends CommonModelSerializer {

    public ViewModelSerializer(){
        if(VoThreadLoacl.get()!=null)
            isFull = VoThreadLoacl.get().getfull();
    }
    public ViewModelSerializer(boolean full){
        super(full);
        isFull = full;
    }
    //region BaseProp
    @Override
    protected void writeExtendModelProperty(IGspCommonModel commonModel, JsonGenerator writer) {
        GspViewModel vm = (GspViewModel) commonModel;
        writeAutoMergeMessage(writer, vm);
        writeVMActions(writer, vm);
        writeExtendModelProperty(writer, vm);
    }

    private void writeAutoMergeMessage(JsonGenerator writer, GspViewModel vm) {
        if(isFull||!vm.getAutoConvertMessage()) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ConvertMsg);
            SerializerUtils.writePropertyValue_boolean(writer, vm.getAutoConvertMessage());
        }
    }

    private void writeVMActions(JsonGenerator writer, GspViewModel vm) {
        if (!isFull&&vm.getActions() == null || vm.getActions().size() < 1)
            return;
        boolean isHaveSelfAction=false;
        InternalExtendActionUtil internalExtendActionUtil = new InternalExtendActionUtil();
        VMActionCollection actionList = vm.getActions();
        for(ViewModelAction a : actionList){
            if(!internalExtendActionUtil.InternalActionIds.contains(a.getID())){
                isHaveSelfAction=true;
                break;
            }
        }
        if(isFull||isHaveSelfAction){
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.Actions);
        getVMActionCollectionConvertor().serialize(vm.getActions(), writer, null);
        }
    }

    private VmActionCollectionSerializer getVMActionCollectionConvertor() {
        return new VmActionCollectionSerializer(isFull);
    }

    private void writeExtendModelProperty(JsonGenerator writer, GspViewModel vm) {
        TemplateVoInfo s = vm.getTemplateVoInfo();
        if(isFull||!vm.getEnableStdTimeFormat()){
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.EnableStdTimeFormat, vm.getEnableStdTimeFormat());}
        if((vm.getSource()!=null && !"".equals(vm.getSource()))){
            SerializerUtils.writePropertyValue(writer,ViewModelJsonConst.Source,vm.getSource());
        }
        if(isFull ||!vm.getIsGenFilterConvertor()){
            SerializerUtils.writePropertyValue(writer,ViewModelJsonConst.IsGenFilterConvertor,vm.getIsGenFilterConvertor());
        }
        if(isFull||(vm.getDescription()!=null&&!"".equals(vm.getDescription()))){
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.Description, vm.getDescription());}
        if(isFull||(!vm.getExtendType().equals("GspViewModel"))){
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ExtendType, vm.getExtendType());}
        if(isFull||(s.getTemplateVoServiceUnit()!=null&&!"".equals(s.getTemplateVoServiceUnit()))){
            if(isFull||(s.getTemplateVoId()!=null&&!"".equals(s.getTemplateVoId()))){
                if(isFull||(s.getTemplateVoPkgName()!=null&&!"".equals(s.getTemplateVoPkgName()))){
                    SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.TemplateVoInfo, vm.getTemplateVoInfo());
                }
            }
        }
        writeMapping(writer, vm);
        writeValueHelpConfigs(writer, vm);
        writeExtendProperties(writer, vm);
        writeVoDataExtendInfo(writer, vm);
    }

    private void writeMapping(JsonGenerator writer, GspViewModel vm) {
        if (vm.getMapping() == null) {
            return;
        }
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.Mapping);
        ViewModelMappingSerializer convertor = new ViewModelMappingSerializer(isFull);
        convertor.serialize(vm.getMapping(), writer, null);
    }

    private void writeValueHelpConfigs(JsonGenerator writer, GspViewModel vm) {
        if(isFull||(vm.getValueHelpConfigs()!=null&&vm.getValueHelpConfigs().size()!=0)){
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ValueHelpConfigs);
            SerializerUtils.writeArray(writer,new ValueHelpConfigSerizlizer(isFull), vm.getValueHelpConfigs());
        }
    }


    private void writeExtendProperties(JsonGenerator writer, GspViewModel vm) {
        if(!isFull&&vm.getExtendProperties()==null&&vm.getExtendProperties().size()==0)
            return;
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ExtendProperties);
        var dic = vm.getExtendProperties();
        SerializerUtils.writeStartObject(writer);
        if (dic != null && dic.size() > 0) {
            for (var item : dic.entrySet()) {
                SerializerUtils.writePropertyValue(writer, item.getKey(), item.getValue());
            }
        }
        SerializerUtils.writeEndObject(writer);
    }

    private void writeVoDataExtendInfo(JsonGenerator writer, GspViewModel vm) {
        if(isFull||(vm.getDataExtendInfo()!=null&&!vm.getDataExtendInfo().isAllNull())) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.DataExtendInfo);
            var converter = new VoDataExtendInfoSerializer(isFull);
            converter.serialize(vm.getDataExtendInfo(), writer, null);
        }
    }


    //endregion

    //region SelfProp
    @Override
    protected void writeExtendModelSelfProperty(IGspCommonModel commonModel, JsonGenerator writer) {

    }

    //endregion
    @Override
    protected CmObjectSerializer getCmObjectSerializer() {
        return new ViewObjectSerializer(isFull);
    }
}
