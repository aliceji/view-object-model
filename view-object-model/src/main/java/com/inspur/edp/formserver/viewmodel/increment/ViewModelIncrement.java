

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.increment;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.element.increment.GspCommonFieldIncrement;
import com.inspur.edp.das.commonmodel.entity.increment.CommonModelIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.HelpConfigIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.VoActionIncrement;
import com.inspur.edp.formserver.viewmodel.json.increment.ViewModelIncrementDeserializer;
import com.inspur.edp.formserver.viewmodel.json.increment.ViewModelIncrementSerializer;

import java.util.HashMap;
import java.util.LinkedHashMap;

@JsonSerialize(using = ViewModelIncrementSerializer.class)
@JsonDeserialize(using = ViewModelIncrementDeserializer.class)
public class ViewModelIncrement extends CommonModelIncrement {

    private HashMap<String, HelpConfigIncrement> valueHelpConfigs = new HashMap<>();

    public HashMap<String, HelpConfigIncrement> getValueHelpConfigs() {
        return this.valueHelpConfigs;
    }

    private HashMap<String, VoActionIncrement> actions = new HashMap<>();

    public HashMap<String, VoActionIncrement> getActions() {
        return actions;
    }
}
