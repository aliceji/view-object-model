

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.action.internalexternalaction;

import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
/**
 * The  Definition Of Modify Action
 *
 * @ClassName: ModifyAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ModifyAction extends MappedCdpAction implements IInternalExtendAction
{
	public static final String id = "47dd3752-72a3-4c56-81c0-ae8ccfe5eb98";
	public static final String code = "Modify";
	public static final String name = "内置修改操作";
	public ModifyAction()
	{
		setID(id);
		setCode(code);
		setName(name);
	}

}
