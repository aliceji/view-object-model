

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json;
/**
 * The Serializer Property Names Of
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelJsonConst {
    //视图对象
    public static final String Category = "Category";
    public static final String SourceEntity = "SourceEntity";
    public static final String DataLockType = "DataLockType";
    public static final String ExtendType = "ExtendType";
    public static final String DependentEntityId = "DependentEntityId";
    public static final String DependentEntityPackageName = "DependentEntityPackageName";
    public static final String BizMgrActions = "BizMgrActions";
    public static final String ComponentAssemblyName = "ComponentAssemblyName";
    public static final String AssemblyName = "AssemblyName";
    public static final String Actions = "Actions";
    public static final String Mapping = "Mapping";
    public static final String ValueHelpConfigs = "ValueHelpConfigs";
    public static final String Description = "Description";
    public static final String EnableStdTimeFormat = "EnableStdTimeFormat";
    public static final String ExtendProperties = "ExtendProperties";
    public static final String ConvertMsg = "ConvertMsg";
    // vo扩展操作
    public static final String DataExtendInfo = "DataExtendInfo";
    public static final String TemplateVoServiceUnit = "TemplateVoServiceUnit";
    public static final String TemplateVoId = "TemplateVoId";
    public static final String TemplateVoPkgName = "TemplateVoPkgName";
    public static final String TemplateVoInfo = "TemplateVoInfo";

    public static final String DataMappingActions = "DataMappingActions";
    public static final String BeforeQueryActions = "BeforeQueryActions";
    public static final String QueryActions = "QueryActions";
    public static final String AfterQueryActions = "AfterQueryActions";
    public static final String BeforeRetrieveActions = "BeforeRetrieveActions";

    public static final String RetrieveActions = "RetrieveActions";
    public static final String AfterRetrieveActions = "AfterRetrieveActions";
    public static final String BeforeModifyActions = "BeforeModifyActions";
    public static final String ModifyActions = "ModifyActions";
    public static final String AfterModifyActions = "AfterModifyActions";

    public static final String ChangesetMappingActions = "ChangesetMappingActions";
    public static final String BeforeCreateActions = "BeforeCreateActions";
    public static final String CreateActions = "CreateActions";
    public static final String AfterCreateActions = "AfterCreateActions";
    public static final String BeforeDeleteActions = "BeforeDeleteActions";

    public static final String DeleteActions = "DeleteActions";
    public static final String AfterDeleteActions = "AfterDeleteActions";
    public static final String BeforeSaveActions = "BeforeSaveActions";
    public static final String DataReversalMappingActions = "DataReversalMappingActions";
    public static final String AfterSaveActions = "AfterSaveActions";

    public static final String BeforeMultiDeleteActions="BeforeMultiDeleteActions";
    public static final String MultiDeleteActions="MultiDeleteActions";
    public static final String AfterMultiDeleteActions="AfterMultiDeleteActions";

    public static final String ChangesetReversalMappingActions = "ChangesetReversalMappingActions";

    //视图对象对象
    public static final String IsBeckendOnly = "IsBeckendOnly";
    public static final String IsRootNode = "IsRootNode";
    public static final String PrimaryKey = "PrimaryKey";
    public static final String BelongModel = "BelongModel";
    public static final String ParentObject = "ParentObject";
    public static final String ParentObjectID = "ParentObjectID";
    public static final String HasChildNode = "HasChildNode";
    public static final String DefaultPageSize = "DefaultPageSize";
    public static final String OrderbyCondition="OrderbyCondition";
    public static final String FilterCondition="FilterCondition";

    //视图对象字段
    //internal const string IsReadOnly = "IsReadOnly";
    public static final String ShowInFilter = "ShowInFilter";
    public static final String ShowInSort = "ShowInSort";
    public static final String ImmediateSubmission = "ImmediateSubmission";
    public static final String IsVirtualViewElement = "IsVirtualViewElement";
    public static final String EnableMultiLanguageInput = "EnableMultiLanguageInput";

    //ValueHelpConfig
    public static final String FilterExpression = "FilterExpression";
    public static final String HelperId = "HelperID";
    public static final String ElementId = "ElementID";
    public static final String HelpExtend = "HelpExtend";
    public static final String BeforeHelp = "BeforeHelp";
    public static final String EnableCustomHelpAuth = "EnableCustomHelpAuth";

    //ViewModelMapping
    public static final String MapType = "MapType";
    public static final String TargetMetadataId = "TargetMetadataId";
    public static final String TargetMetadataPkgName = "TargetMetadataPkgName";
    public static final String TargetObjId = "TargetObjId";
    public static final String TargetObjectId = "TargetObjectId";
    public static final String TargetElementId = "TargetElementId";
    //internal const string TargetMetadataType = "TargetMetadataType";
    public static final String SourceType = "SourceType";
    public static final String IndexVoId = "IndexVoId";

    //VMAction
    public static final String Type = "Type";
    public static final String ParameterCollection = "ParameterCollection";
    public static final String ReturnValue = "ReturnValue";

    //MappedBizAction
    //MappedCdpAction
    //MappedCdpActionBase
    public static final String ComponentEntityId = "ComponentEntityId";
    public static final String ComponentPkgName = "ComponentPkgName";
    public static final String ComponentName = "ComponentName";
    public static final String IsGenerateComponent = "IsGenerateComponent";
    public static final String IsAutoSave = "IsAutoSave";
    public static final String BelongElement = "BelongElement";

    //paras
    public static final String ParamCode = "ParamCode";
    public static final String ParamName = "ParamName";
    public static final String ParameterType = "ParameterType";
    public static final String Assembly = "Assembly";
    public static final String ClassName = "ClassName";
    public static final String JavaClassName = "JavaClassName";
    public static final String Mode = "Mode";
    public static final String ParamDescription = "ParamDescription";
    public static final String HelpId = "HelpId";
    public static final String VMHelpConfig = "VMHelpConfig";
    // internal const string VMActionCollection = "VMActionCollection";
    public static final String HelpActions = "HelpActions";
    public static final String CollectionParameterType = "CollectionParameterType";
    public static final String ParamActualValue = "ParamActualValue";

    //Increment
    public static final String HelpIncrements = "HelpIncrements";
    public static final String HelpIncrement = "HelpIncrement";
    public static final String AddedAction = "AddedAction";
    public static final String ModifyAction = "ModifyAction";
    public static final String DelAction = "DelAction";
    public static final String Action = "Action";

    public static final String Source="Source";
    public static final String IsGenFilterConvertor="IsGenFilterConvertor";

}
