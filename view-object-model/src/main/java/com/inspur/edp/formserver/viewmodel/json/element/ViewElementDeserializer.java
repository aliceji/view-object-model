

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.element;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.inspur.edp.das.commonmodel.entity.GspCommonElement;
import com.inspur.edp.formserver.viewmodel.common.MappingType;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoElementSourceType;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoObjectSourceType;
import com.inspur.edp.formserver.viewmodel.json.ExtendPropertiesDeserializer;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import com.inspur.edp.formserver.viewmodel.json.mapping.GspVoElementMappingDeserializer;
import com.inspur.edp.formserver.viewmodel.json.operation.VmActionCollectionDeserializer;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.json.element.CmElementDeserializer;
import com.inspur.edp.formserver.viewmodel.GspViewModelElement;
import com.inspur.edp.formserver.viewmodel.collection.VMActionCollection;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoElementMapping;
import com.inspur.edp.formserver.viewmodel.common.VMHelpConfig;

import java.util.HashMap;

/**
 * The  Josn Deserializer Of View Model Element
 *
 * @ClassName: ViewElementDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewElementDeserializer extends CmElementDeserializer {
    @Override
    protected GspCommonElement createElement() {
        return new GspViewModelElement();
    }

    @Override
    protected void beforeCMElementDeserializer(GspCommonElement item){
        GspViewModelElement field = (GspViewModelElement) item;
        field.setIsBeckendOnly(false);
        field.setImmediateSubmission(false);
        field.setIsVirtualViewElement(false);
        field.setExtendProperties(new HashMap<>());
        VMHelpConfig fig = new VMHelpConfig();
        fig.setHelpId("");
        field.setVMHelpConfig(fig);
        field.setHelpActions(new VMActionCollection());
    }

    @Override
    protected boolean readExtendElementProperty(GspCommonElement item, String propName, JsonParser jsonParser) {
        boolean result = true;
        GspViewModelElement field = (GspViewModelElement) item;
        switch (propName) {
            case ViewModelJsonConst.IsBeckendOnly:
                field.setIsBeckendOnly(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case ViewModelJsonConst.Mapping:
                readMapping(jsonParser, field);
                break;
            case ViewModelJsonConst.HelpActions:
                readHelpActions(jsonParser, field);
                break;
            case ViewModelJsonConst.ImmediateSubmission:
                field.setImmediateSubmission(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case ViewModelJsonConst.IsVirtualViewElement:
                field.setIsVirtualViewElement(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case ViewModelJsonConst.EnableMultiLanguageInput:
                field.setEnableMultiLanguageInput(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case ViewModelJsonConst.ShowInFilter:
                field.setShowInFilter(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case ViewModelJsonConst.ShowInSort:
                field.setShowInSort(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case ViewModelJsonConst.ExtendProperties:
                readExtendProperties(jsonParser, field);
                break;
            case ViewModelJsonConst.VMHelpConfig:
                readVMHelpConfig(jsonParser, field);
                break;
            default:
                result = false;
                break;
        }
        return result;
    }

    private void readHelpActions(JsonParser jsonParser, GspViewModelElement field) {
        VmActionCollectionDeserializer deserializer = new VmActionCollectionDeserializer();
        VMActionCollection collection = deserializer.deserialize(jsonParser, null);
        field.setHelpActions(collection);
    }

    private void readMapping(JsonParser jsonParser, GspViewModelElement field) {
        GspVoElementMappingDeserializer deserializer = new GspVoElementMappingDeserializer();
        GspVoElementMapping mapping = (GspVoElementMapping) deserializer.deserialize(jsonParser, null);
        field.setMapping(mapping);
    }

    private void readExtendProperties(JsonParser jsonParser, GspViewModelElement field) {
        ExtendPropertiesDeserializer deserializer = new ExtendPropertiesDeserializer();
        field.setExtendProperties(deserializer.deserialize(jsonParser, null));
    }

    private void readVMHelpConfig(JsonParser jsonParser, GspViewModelElement field) {
        VMHelpConfig config = new VMHelpConfig();
        SerializerUtils.readStartObject(jsonParser);
        if (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
            while (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
                readVMHelpConfig(jsonParser, config);
            }
        }
        SerializerUtils.readEndObject(jsonParser);
        field.setVMHelpConfig(config);
    }

    private void readVMHelpConfig(JsonParser jsonParser, VMHelpConfig config) {
        String propName = SerializerUtils.readPropertyName(jsonParser);
        switch (propName) {
            case ViewModelJsonConst.HelpId:
                config.setHelpId(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
        }
    }
}
