

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.operation;

import com.inspur.edp.formserver.viewmodel.action.MappedBizAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.action.mappedbiz.MappedBizActionParameterCollection;
import com.inspur.edp.formserver.viewmodel.action.viewmodelbase.ViewModelParameterCollection;

/**
 * The Json  Deserializer Of The View Model Action Of  Mapped Be Action
 *
 * @ClassName: MappedBizActionDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedBizActionDeserializer extends VmActionDeserializer<MappedBizAction> {
    @Override
    protected MappedBizAction createOp() {
        return new MappedBizAction();
    }

    @Override
    protected void beforeVnactionDeserializer(ViewModelAction op) {}

    @Override
    protected VmParameterDeserializer createPrapDeserializer() {
        return new MappedBizActionParaDeserializer();
    }

    @Override
    protected ViewModelParameterCollection createPrapCollection() {
        return new MappedBizActionParameterCollection();
    }
}
