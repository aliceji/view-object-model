

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.controlrule.ruledefine;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefinition;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleValue;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonFieldRuleNames;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmFieldControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmRuleNames;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.parser.VoFieldControlRuleDefParser;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.serializer.VoFieldControlRuleDefSerializer;

@JsonSerialize(using = VoFieldControlRuleDefSerializer.class)
@JsonDeserialize(using = VoFieldControlRuleDefParser.class)
public class VoFieldControlRuleDef extends CmFieldControlRuleDef {
    public VoFieldControlRuleDef(ControlRuleDefinition parentRuleDefinition) {
        super(parentRuleDefinition);
        super.setRuleObjectType(VoFieldControlRuleNames.VoFieldRuleObjectType);
        init();
    }

    private void init(){
        ControlRuleDefItem nameRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CmRuleNames.Name);
                this.setRuleDisplayName("名称");
                this.setDescription("字段名称");
                this.setDefaultRuleValue(ControlRuleValue.Allow);
            }
        };
        setNameControlRule(nameRule);

        ControlRuleDefItem lengthRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonFieldRuleNames.Length);
                this.setRuleDisplayName("长度");
                this.setDescription("字段长度");
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setLengthControlRule(lengthRule);

        ControlRuleDefItem precisionRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonFieldRuleNames.Precision);
                this.setRuleDisplayName("精度");
                this.setDescription("字段精度");
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setPrecisionControlRule(precisionRule);

        ControlRuleDefItem defaultValueRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonFieldRuleNames.DefaultValue);
                this.setRuleDisplayName("默认值");
                this.setDescription("字段默认值");
                this.setDefaultRuleValue(ControlRuleValue.Allow);
            }
        };
        setDefaultValueControlRule(defaultValueRule);

        ControlRuleDefItem multiLanRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonFieldRuleNames.MultiLanField);
                this.setRuleDisplayName("是否多语");
                this.setDescription("字段多语设置");
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setMultiLanFieldControlRule(multiLanRule);

        ControlRuleDefItem readonlyRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonFieldRuleNames.Readonly);
                this.setRuleDisplayName("是否只读");
                this.setDescription("字段只读设置");
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setReadonlyControlRule(readonlyRule);

        ControlRuleDefItem requiredRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonFieldRuleNames.Required);
                this.setRuleDisplayName("是否必填");
                this.setDescription("字段必填设置");
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setRequiredControlRule(requiredRule);
    }
}
