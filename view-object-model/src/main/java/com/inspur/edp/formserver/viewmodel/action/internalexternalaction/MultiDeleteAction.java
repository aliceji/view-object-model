

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.action.internalexternalaction;
import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;

/**
 * The Definition Of Mutly Delete Action
 *
 * @ClassName: MultiDeleteAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MultiDeleteAction extends MappedCdpAction implements IInternalExtendAction{
    public static final String id = "7b1c3c4l-t1a4-4dyc-b75b-7695hcb3we7e";
    public static final String code= "MultiDelete";
    public static final String name = "内置批量删除操作";
    public MultiDeleteAction()
    {
        setID(id);
        setCode(code);
        setName(name);
    }
}
