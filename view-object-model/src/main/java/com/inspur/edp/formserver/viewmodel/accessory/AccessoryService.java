

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.accessory;

import com.inspur.edp.formserver.viewmodel.GspViewModel;

/**
 * The Approval Log Services,Using By VO Creating Wizard
 *
 * @ClassName: AccessoryService
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface AccessoryService {

  /*
  nodeCode : 父节点编号
  processInstLabelId: 父节点上流程实例字段标签
   */
  void addApprovalComments(GspViewModel model, String nodeCode, String processInstLabelId, boolean includeBacklog);

  /**
   *  @param model
   * @param nodeCode
   * @param processInstLabelId
   * @param includeBacklog
   */
  void addApprovalWorkItenLogs(GspViewModel model, String nodeCode, String processInstLabelId, boolean includeBacklog);

}
