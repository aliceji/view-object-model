

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.action;

/**
 * The Definition Of The View Model Action Type
 */
public enum ViewModelActionType {
    /**
     * BE 的业务操作
     */
    BEAction,

    /**
     * VM 操作
     */
    VMAction,

    /**
     * 自定义操作
     */
    Custom;

    public int getValue() {
        return this.ordinal();
    }

    public static ViewModelActionType forValue(int value) {
        return values()[value];
    }
}
