

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.increment.extract.action;

import com.inspur.edp.cef.designtime.api.increment.extractor.AbstractIncrementExtractor;
import com.inspur.edp.das.commonmodel.controlrule.CmControlRule;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmControlRuleDef;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.ModifyVoActionIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.VoActionIncrement;

public class ActionIncrementExtractor extends AbstractIncrementExtractor {

    public VoActionIncrement extractorIncrement(ViewModelAction oldAction, ViewModelAction newAction, CmControlRule rule, CmControlRuleDef def) {

        if (oldAction == null && newAction == null) {
            return null;
        } else if (oldAction == null) {
            return this.getAddedActionExtractor().extract(newAction);
        } else if (newAction == null) {
            return null;
//            return (CommonEntityIncrement)(oldHelpConfig != null && newAction == null ? this.getDeletedEntityExtractor().extract(oldHelpConfig) : this.getModifyEntityExtractor().extract(oldHelpConfig, newAction, rule, def));
        } else {
            ModifyVoActionIncrement increment = new ModifyVoActionIncrement();
            increment.setActionId(newAction.getID());
            increment.setAction(newAction);
            return increment;
        }
    }

    protected AddedActionExtractor getAddedActionExtractor() {
        return new AddedActionExtractor();
    }

}
