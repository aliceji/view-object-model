

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.controlrule.rule.convert;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.convert.ControlRuleConvertor;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.GspViewObject;
import com.inspur.edp.formserver.viewmodel.controlrule.rule.VoControlRule;
import com.inspur.edp.formserver.viewmodel.controlrule.rule.VoFeildControlRule;
import com.inspur.edp.formserver.viewmodel.controlrule.rule.VoObjControlRule;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.VoControlRuleDef;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.VoFieldControlRuleDef;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.VoObjectControlRuleDef;
import lombok.var;

import java.util.HashMap;

public class VoControlRuleConvertor {
    public static void convert2ControlRule(VoControlRuleDef ruleDef, VoControlRule rule, GspViewModel vo) {

        ControlRuleConvertor.convert2ControlRule(ruleDef, rule);
        rule.setRuleId(vo.getId());

        VoObjControlRule mainObjRule = convert2VoObjControlRule((VoObjectControlRuleDef) ruleDef.getChildControlRules().get(CommonModelNames.MainObject), vo.getMainObject());
        rule.setMainEntityControlRule(mainObjRule);

    }

    private static VoObjControlRule convert2VoObjControlRule(VoObjectControlRuleDef ruleDef, GspViewObject voObject) {

        VoObjControlRule objRule = new VoObjControlRule();
        ControlRuleConvertor.convert2ControlRule(ruleDef, objRule, voObject);

        dealChildObjs(ruleDef, objRule, voObject);

        VoFieldControlRuleDef elementRuleDef = (VoFieldControlRuleDef) ruleDef.getChildControlRules().get(CommonModelNames.Element);
        dealElement(elementRuleDef, objRule, voObject);

        return objRule;
    }

    private static void dealChildObjs(VoObjectControlRuleDef ruleDef, VoObjControlRule objRule, GspViewObject voObject) {
        var childObjs = voObject.getContainChildObjects();
        if (childObjs == null || childObjs.size() < 1)
            return;
        objRule.getChildRules().put(CommonModelNames.ChildObject, new HashMap<>());

        for (var childObj : childObjs) {
            VoObjControlRule childRule = convert2VoObjControlRule(ruleDef, (GspViewObject) childObj);
            objRule.getChildRules().get(CommonModelNames.ChildObject).put(childObj.getID(), childRule);
        }
    }

    private static void dealElement(VoFieldControlRuleDef ruleDef, VoObjControlRule objRule, GspViewObject voObject) {
        var elements = voObject.getContainElements();
        if (elements == null || elements.size() < 1)
            return;
        objRule.getChildRules().put(CommonModelNames.Element, new HashMap<>());

        for (var element : elements) {
            VoFeildControlRule fieldRule = new VoFeildControlRule();
            ControlRuleConvertor.convert2ControlRule(ruleDef, fieldRule, element);
            objRule.getChildRules().get(CommonModelNames.Element).put(element.getID(), fieldRule);
        }
    }

}
