

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.i18n;

import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.i18n.GspAssoRefEleResourceExtractor;
import com.inspur.edp.das.commonmodel.i18n.GspAssoResourceExtractor;
import com.inspur.edp.formserver.viewmodel.GspViewModelElement;

class ViewAssoResourceExtractor extends GspAssoResourceExtractor {

    public ViewAssoResourceExtractor(GspAssociation asso, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(asso, context, parentResourceInfo);
    }

    @Override
    protected final GspAssoRefEleResourceExtractor getAssoRefElementResourceExtractor(
            ICefResourceExtractContext context,
            CefResourcePrefixInfo assoPrefixInfo,
            IGspCommonElement field) {
        return new ViewAssoRefEleResourceExtractor((GspViewModelElement) field, context, assoPrefixInfo);
    }

}
