

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelActionType;
import com.inspur.edp.formserver.viewmodel.collection.VMActionCollection;
import lombok.var;
/**
 * The Josn Serializer Of View Model Action Collection
 *
 * @ClassName: VmActionCollectionSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VmActionCollectionSerializer extends JsonSerializer<VMActionCollection> {

    protected boolean isFull = true;
    public VmActionCollectionSerializer(){}
    public VmActionCollectionSerializer(boolean full){
        isFull = full;
    }

    @Override
    public void serialize(VMActionCollection value, JsonGenerator writer, SerializerProvider serializers) {

        if (value.size() == 0) {
            SerializerUtils.WriteStartArray(writer);
            SerializerUtils.WriteEndArray(writer);
            return;
        }
        SerializerUtils.WriteStartArray(writer);
        for (int i = 0; i < value.size(); i++) {
            ViewModelAction action = value.get(i);
            getActionConvertor(action.getType()).serialize(value.get(i), writer, null);
        }
        SerializerUtils.WriteEndArray(writer);
    }

    private VmActionSerializer getActionConvertor(ViewModelActionType type) {
        switch (type) {
            case BEAction:
                return new MappedBizActionSerializer(isFull);
            case VMAction:
                return new MappedCdpActionSerializer(isFull);
            case Custom:
            default:
                throw new RuntimeException("未定义'" + type + "'类型Action的JSON序列化器。");
        }
    }
}
