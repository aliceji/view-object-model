

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.action;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.formserver.viewmodel.action.mappedcdp.MappedCdpActionParameterCollection;
import com.inspur.edp.formserver.viewmodel.json.operation.MappedCdpActionDeserializer;
import com.inspur.edp.formserver.viewmodel.json.operation.MappedCdpActionSerializer;
import java.io.Serializable;

/**
 * The Definition Of The Parameter With Component
 *
 * @ClassName: MappedCdpAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonSerialize(using = MappedCdpActionSerializer.class)
@JsonDeserialize(using = MappedCdpActionDeserializer.class)
public class MappedCdpAction extends MappedCdpActionBase implements Cloneable, Serializable {
  ///#region 属性

  private MappedCdpActionParameterCollection mappedCdpActionParams;

  /**
   * 类型
   */
  @Override
  public ViewModelActionType getType() {
    return ViewModelActionType.VMAction;
  }

  ///#endregion
  public MappedCdpAction() {
    mappedCdpActionParams = new MappedCdpActionParameterCollection();
  }
  ///#region 方法

  /**
   * 克隆
   *
   * @return VO节点映射
   */
  @Override
  public MappedCdpAction clone() {
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      return objectMapper.readValue(objectMapper.writeValueAsString(this), MappedCdpAction.class);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  protected IViewModelParameterCollection getParameters() {
    return mappedCdpActionParams;
  }
  ///#endregion
}
