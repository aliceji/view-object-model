

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.entity.CustomizationInfo;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import com.inspur.edp.formserver.viewmodel.GspViewObject;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelActionType;
import com.inspur.edp.formserver.viewmodel.action.viewmodelbase.ViewModelParameter;
import com.inspur.edp.formserver.viewmodel.action.viewmodelbase.ViewModelParameterCollection;
import com.inspur.edp.formserver.viewmodel.action.viewmodelbase.ViewModelReturnValue;
import com.inspur.edp.formserver.viewmodel.common.ViewModelMapping;
import com.inspur.edp.formserver.viewmodel.json.ExtendPropertiesDeserializer;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import com.inspur.edp.formserver.viewmodel.json.mapping.ViewModelMappingDeserializer;

import java.io.IOException;
import java.util.HashMap;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;
/**
 * The Josn Deserializer Of View Model Action
 *
 * @ClassName: VmActionDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class VmActionDeserializer<T extends ViewModelAction> extends JsonDeserializer<T> {
    @Override
    public final T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        return deserializeBizOperation(jsonParser);
    }

    private final T deserializeBizOperation(JsonParser jsonParser) {
        T op = createOp();
        op.setComponentName("");
        op.setIsAutoSave(false);
        op.setExtendProperties(new HashMap<>());
        op.setReturnValue(new ViewModelReturnValue());
        beforeVnactionDeserializer(op);
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(op, propName, jsonParser);
        }
        SerializerUtils.readEndObject(jsonParser);

        return op;
    }

    private void readPropertyValue(ViewModelAction op, String propName, JsonParser jsonParser) {
        switch (propName) {
            case CommonModelNames.ID:
                op.setID(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CommonModelNames.Code:
                op.setCode(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CommonModelNames.Name:
                op.setName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case ViewModelJsonConst.Type:
                op.setType(SerializerUtils.readPropertyValue_Enum(jsonParser, ViewModelActionType.class, ViewModelActionType.values(), ViewModelActionType.BEAction));
                break;
            case ViewModelJsonConst.ParameterCollection:
                readParameters(jsonParser, op);
                break;
            case ViewModelJsonConst.ReturnValue:
                readReturnValue(jsonParser, op);
                break;
            case ViewModelJsonConst.Mapping:
                readMapping(jsonParser, op);
                break;
            case ViewModelJsonConst.ComponentName:
                op.setComponentName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case ViewModelJsonConst.ExtendProperties:
                readExtendProperties(jsonParser, op);
                break;
            case ViewModelJsonConst.IsAutoSave:
                op.setIsAutoSave(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case CefNames.CustomizationInfo:
                op.setCustomizationInfo(SerializerUtils.readPropertyValue_Object(CustomizationInfo.class,jsonParser));
                try {
                    jsonParser.nextToken();
                } catch (IOException e) {
                    throw new RuntimeException(String.format("BizOperationDeserializer反序列化错误：%1$s", propName));
                }
                break;
            default:
                if (!readExtendOpProperty(op, propName, jsonParser)) {
                    throw new RuntimeException(String.format("BizOperationDeserializer未识别的属性名：%1$s", propName));
                }
        }
    }

    private void readExtendProperties(JsonParser jsonParser, ViewModelAction action) {
        ExtendPropertiesDeserializer deserializer = new ExtendPropertiesDeserializer();
        action.setExtendProperties(deserializer.deserialize(jsonParser, null));
    }

    private void readParameters(JsonParser jsonParser, ViewModelAction op) {
        VmParameterDeserializer parameterDeserializer = createPrapDeserializer();
        ViewModelParameterCollection<ViewModelParameter> collection = createPrapCollection();
        SerializerUtils.readArray(jsonParser, parameterDeserializer, collection);
        for (ViewModelParameter para : collection) {
            op.getParameterCollection().add(para);
        }
    }

    private void readReturnValue(JsonParser jsonParser, ViewModelAction op) {
        VmReturnValueDeserializer deserializer = new VmReturnValueDeserializer();
        op.setReturnValue(deserializer.deserializePara(jsonParser));
    }

    private void readMapping(JsonParser jsonParser, ViewModelAction op) {
        ViewModelMappingDeserializer deserializer = new ViewModelMappingDeserializer();
        ViewModelMapping mapping = deserializer.deserialize(jsonParser, null);
        op.setMapping(mapping);
    }

    protected boolean readExtendOpProperty(ViewModelAction op, String propName, JsonParser jsonParser) {
        return false;
    }

    protected abstract T createOp();

    protected abstract void beforeVnactionDeserializer(ViewModelAction op);

    protected abstract VmParameterDeserializer createPrapDeserializer();

    protected abstract ViewModelParameterCollection createPrapCollection();
}
