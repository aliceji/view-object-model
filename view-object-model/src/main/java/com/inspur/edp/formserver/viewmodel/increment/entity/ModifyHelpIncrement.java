

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.increment.entity;

import com.inspur.edp.cef.designtime.api.increment.AddedIncrement;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;
import com.inspur.edp.cef.designtime.api.increment.ModifyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;
import com.inspur.edp.formserver.viewmodel.common.ValueHelpConfig;

import java.util.HashMap;

public class ModifyHelpIncrement extends HelpConfigIncrement implements ModifyIncrement {

    private HashMap<String, PropertyIncrement> changeProperties;

    @Override
    public IncrementType getIncrementType() {
        return IncrementType.Modify;
    }

    @Override
    public HashMap<String, PropertyIncrement> getChangeProperties() {
        return null;
    }

    private ValueHelpConfig helpConfig;

    public ValueHelpConfig getHelpConfig() {
        return helpConfig;
    }

    public void setHelpConfig(ValueHelpConfig helpConfig) {
        this.helpConfig = helpConfig;
        setElementId(helpConfig.getElementId());
    }
}
