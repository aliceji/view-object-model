

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.action;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.formserver.viewmodel.action.mappedbiz.MappedBizActionParameterCollection;
import com.inspur.edp.formserver.viewmodel.json.operation.MappedBizActionDeserializer;
import com.inspur.edp.formserver.viewmodel.json.operation.MappedBizActionSerializer;
import java.io.Serializable;

/**
 * The Definition Of The Mapped Biz  Action
 *
 * @ClassName: MappedBizAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonSerialize(using = MappedBizActionSerializer.class)
@JsonDeserialize(using = MappedBizActionDeserializer.class)
public class MappedBizAction extends ViewModelAction implements Cloneable, Serializable {

  ///#region 属性
  private MappedBizActionParameterCollection mappedBizActionParams;
  /**
   * 类型
   */
  //@Override
  public ViewModelActionType Type = ViewModelActionType.BEAction;
//	public ViewModelActionType Type => ViewModelActionType.BEAction;

  ///#endregion
  public MappedBizAction() {
    mappedBizActionParams = new MappedBizActionParameterCollection();
  }
  ///#region 方法

  /**
   * 克隆
   *
   * @return VM节点映射
   */
  @Override
  public final MappedBizAction clone() {
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      return objectMapper.readValue(objectMapper.writeValueAsString(this), MappedBizAction.class);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  protected IViewModelParameterCollection getParameters() {
    return mappedBizActionParams;
  }
  ///#endregion
}
