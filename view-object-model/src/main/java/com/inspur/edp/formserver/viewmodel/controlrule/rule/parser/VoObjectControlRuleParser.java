

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.controlrule.rule.parser;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.das.commonmodel.controlrule.CmEntityControlRule;
import com.inspur.edp.das.commonmodel.controlrule.parser.CmEntityControlRuleParser;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import com.inspur.edp.formserver.viewmodel.controlrule.rule.VoObjControlRule;

public class VoObjectControlRuleParser extends CmEntityControlRuleParser {

    protected JsonDeserializer getChildDeserializer(String childTypeName){
        switch (childTypeName){
            case CommonModelNames.ChildObject:
                return new VoObjectControlRuleParser();
            case CommonModelNames.Element:
                return new VoFieldRuleParser();
        }
        return null;
    }

    @Override
    protected CmEntityControlRule createCmEntityRule() {
        return new VoObjControlRule();
    }

}
