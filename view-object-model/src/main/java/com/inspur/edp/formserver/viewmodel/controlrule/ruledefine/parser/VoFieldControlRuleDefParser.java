

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.parser;

import com.inspur.edp.das.commonmodel.controlruledef.entity.CmFieldControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.parser.CmFieldRuleDefParser;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.VoFieldControlRuleDef;

public class VoFieldControlRuleDefParser extends CmFieldRuleDefParser<VoFieldControlRuleDef> {
    @Override
    protected final CmFieldControlRuleDef createCmFieldRuleDefinition() {
        return new VoFieldControlRuleDef(null);
    }

}
