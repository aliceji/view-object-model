

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.formserver.viewmodel.action.MappedBizAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
/**
 * The Json Serializer Of The View Model Action Of  Mapped Be Action
 *
 * @ClassName: MappedBizActionSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedBizActionSerializer extends VmActionSerializer<MappedBizAction> {
    public MappedBizActionSerializer(){}
    public MappedBizActionSerializer(boolean full){
        super(full);
        isFull = full;
    }
    @Override
    protected VmParameterSerializer getParaConvertor() {
        return new MappedBizActionParaSerializer(isFull);
    }

    @Override
    protected void writeExtendOperationProperty(JsonGenerator writer, ViewModelAction op) {

    }
}
