

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import com.inspur.edp.formserver.viewmodel.action.viewmodelbase.ViewModelParameter;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
/**
 * The Json Serializer Of View MOdel Action Parameter
 *
 * @ClassName: VmParameterSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VmParameterSerializer<T extends ViewModelParameter> extends JsonSerializer<T> {
    protected boolean isFull = true;
    public VmParameterSerializer(){}
    public VmParameterSerializer(boolean full){
        isFull = full;
    }
    @Override
    public void serialize(T value, JsonGenerator writer, SerializerProvider serializers){
        SerializerUtils.writeStartObject(writer);
        SerializerUtils.writePropertyValue(writer, CommonModelNames.ID, value.getID());
        if(isFull||value.getParamCode()!=null&&!"".equals(value.getParamCode()))
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ParamCode, value.getParamCode());
        if(isFull||value.getParamName()!=null&&!"".equals(value.getParamName()))
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ParamName, value.getParamName());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ParameterType, value.getParameterType().toString());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.Assembly, value.getAssembly());
        if(isFull||value.getClassName()!=null&&!"".equals(value.getClassName()))
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.JavaClassName, value.getClassName());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ClassName, value.getDotnetClassName());
        if(isFull||value.getMode().getValue()!=0){
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.Mode, value.getMode().toString());}
        if(isFull||value.getParamDescription()!=null&&!"".equals(value.getParamDescription())){
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ParamDescription, value.getParamDescription());}
        if(isFull||value.getCollectionParameterType().getValue()!=0){
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.CollectionParameterType, value.getCollectionParameterType().toString());}
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ParamActualValue, value.getActualValue());
        SerializerUtils.writeEndObject(writer);
    }
}
