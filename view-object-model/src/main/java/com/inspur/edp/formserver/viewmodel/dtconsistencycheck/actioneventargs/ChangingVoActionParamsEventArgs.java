

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.dtconsistencycheck.actioneventargs;


public class ChangingVoActionParamsEventArgs extends AbstractVoActionEventArgs{
  protected String voActionParameterType;
  protected String newVoActionParameter;
  protected String originalVoActionParameter;

  public ChangingVoActionParamsEventArgs() {
  }

  public String getVoActionParameterType() {
    return voActionParameterType;
  }

  public void setVoActionParameterType(String voActionParameterType) {
    this.voActionParameterType = voActionParameterType;
  }

  public String getNewVoActionParameter() {
    return newVoActionParameter;
  }

  public void setNewVoActionParameter(String newVoActionParameter) {
    this.newVoActionParameter = newVoActionParameter;
  }

  public String getOriginalVoActionParameter() {
    return originalVoActionParameter;
  }

  public void setOriginalVoActionParameter(String originalVoActionParameter) {
    this.originalVoActionParameter = originalVoActionParameter;
  }
}
