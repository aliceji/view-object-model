

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.controlrule.ruledefine;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefinition;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleValue;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmRuleNames;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.parser.VoControlRuleDefParser;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.serializer.VoControlRuleDefSerializer;

@JsonSerialize(using = VoControlRuleDefSerializer.class)
@JsonDeserialize(using = VoControlRuleDefParser.class)
public class VoControlRuleDef extends CmControlRuleDef {
    public VoControlRuleDef() {
        super(null, VoControlRuleDefNames.VoControlRuleObjectType);
        init();
    }

    private void init() {

        ControlRuleDefItem nameRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CmRuleNames.Name);
                this.setRuleDisplayName("名称");
                this.setDescription("实体名称");
                this.setDefaultRuleValue(ControlRuleValue.Allow);
            }
        };
        setNameControlRule(nameRule);

//        setUsingTimeStampControlRule();

        ControlRuleDefItem addActionRule = new ControlRuleDefItem(){
            {
                this.setRuleName(VoControlRuleDefNames.AddCustomAction);
                this.setRuleDisplayName("添加自定义动作");
                this.setDescription("添加自定义动作");
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setAddCustomActonControlRule(addActionRule);

        ControlRuleDefItem modifyActionRule = new ControlRuleDefItem(){
            {
                this.setRuleName(VoControlRuleDefNames.ModifyCustomActions);
                this.setRuleDisplayName("修改自定义动作");
                this.setDescription("修改自定义动作");
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setModifyCustomActonControlRule(modifyActionRule);

        ControlRuleDefItem addVarRule = new ControlRuleDefItem(){
            {
                this.setRuleName(VoControlRuleDefNames.AddVariableDtm);
                this.setRuleDisplayName("添加自定义变量");
                this.setDescription("添加自定义变量");
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setAddVariableDtmControlRule(addVarRule);

        ControlRuleDefItem modifyVarRule = new ControlRuleDefItem(){
            {
                this.setRuleName(VoControlRuleDefNames.ModifyVariableDtm);
                this.setRuleDisplayName("修改自定义变量");
                this.setDescription("修改自定义变量");
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setModifyVariableDtmControlRule(modifyVarRule);

        VoObjectControlRuleDef objectRuleDef = new VoObjectControlRuleDef(this);
        getChildControlRules().put(CommonModelNames.MainObject, objectRuleDef);

    }

    public ControlRuleDefItem getAddCustomActonControlRule() {
        return super.getControlRuleItem(VoControlRuleDefNames.AddCustomAction);
    }

    public void setAddCustomActonControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(VoControlRuleDefNames.AddCustomAction, ruleItem);
    }

    public ControlRuleDefItem getModifyCustomActonControlRule() {
        return super.getControlRuleItem(VoControlRuleDefNames.ModifyCustomActions);
    }

    public void setModifyCustomActonControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(VoControlRuleDefNames.ModifyCustomActions, ruleItem);
    }

    public ControlRuleDefItem getAddVariableDtmControlRule() {
        return super.getControlRuleItem(VoControlRuleDefNames.AddVariableDtm);
    }

    public void setAddVariableDtmControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(VoControlRuleDefNames.AddVariableDtm, ruleItem);
    }

    public ControlRuleDefItem getModifyVariableDtmControlRule() {
        return super.getControlRuleItem(VoControlRuleDefNames.ModifyVariableDtm);
    }

    public void setModifyVariableDtmControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(VoControlRuleDefNames.ModifyVariableDtm, ruleItem);
    }
}
