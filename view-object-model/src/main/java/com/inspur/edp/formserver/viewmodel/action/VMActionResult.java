

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.action;

import java.io.Serializable;

/**
 * The Definition Of The View Model Action Result
 */
public class VMActionResult implements Cloneable, Serializable {

    /**
     * 类型名称
     */
    private String privateTypeName;

    public final String getTypeName() {
        return privateTypeName;
    }

    public final void setTypeName(String value) {
        privateTypeName = value;
    }

    /**
     * 克隆
     *
     * @return Action执行结果
     */
    public final VMActionResult clone() {
        Object tempVar = null;
        try {
            tempVar = super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        VMActionResult obj = (VMActionResult) ((tempVar instanceof ActionFormatParameter) ? tempVar : null);

        return obj;

    }
}
