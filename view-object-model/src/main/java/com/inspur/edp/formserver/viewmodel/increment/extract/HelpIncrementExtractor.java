

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.increment.extract;

import com.inspur.edp.cef.designtime.api.increment.extractor.AbstractIncrementExtractor;
import com.inspur.edp.das.commonmodel.controlrule.CmControlRule;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmControlRuleDef;
import com.inspur.edp.formserver.viewmodel.common.ValueHelpConfig;
import com.inspur.edp.formserver.viewmodel.increment.entity.HelpConfigIncrement;

public class HelpIncrementExtractor extends AbstractIncrementExtractor {
    public HelpIncrementExtractor() {
    }

    public HelpConfigIncrement extractorIncrement(ValueHelpConfig oldHelpConfig, ValueHelpConfig newHelpConfig, CmControlRule rule, CmControlRuleDef def) {
        if (oldHelpConfig == null && newHelpConfig == null) {
            return null;
        } else if (oldHelpConfig == null && newHelpConfig != null) {
            return this.getAddedHelpExtractor().extract(newHelpConfig);
        } else {
            return null;
//            return (CommonEntityIncrement)(oldHelpConfig != null && newHelpConfig == null ? this.getDeletedEntityExtractor().extract(oldHelpConfig) : this.getModifyEntityExtractor().extract(oldHelpConfig, newHelpConfig, rule, def));
        }
    }

    protected AddedHelpExtractor getAddedHelpExtractor() {
        return new AddedHelpExtractor();
    }

//    protected DeletedEntityExtractor getDeletedEntityExtractor() {
//        return new DeletedEntityExtractor();
//    }

//    protected ModifyEntityExtractor getModifyEntityExtractor() {
//        return new ModifyEntityExtractor();
//    }
}
