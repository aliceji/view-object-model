

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.increment.merger;

import com.inspur.edp.das.commonmodel.controlrule.CmControlRule;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmControlRuleDef;
import com.inspur.edp.das.commonmodel.entity.GspCommonModel;
import com.inspur.edp.das.commonmodel.entity.increment.CommonModelIncrement;
import com.inspur.edp.das.commonmodel.entity.increment.merger.CommonModelMerger;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.common.ValueHelpConfig;
import com.inspur.edp.formserver.viewmodel.increment.ViewModelIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.AddedHelpIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.HelpConfigIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.ModifyHelpIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.AddedVoActionIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.ModifyVoActionIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.VoActionIncrement;
import lombok.var;

import java.util.HashMap;

public class ViewModelMerger extends CommonModelMerger {

    public ViewModelMerger(){
        super();
    }

    public ViewModelMerger(boolean includeAll){
        super(includeAll);
    }
    @Override
    protected void mergeExtendInfo(GspCommonModel extendModel, CommonModelIncrement extendIncrement, CommonModelIncrement baseIncrement, CmControlRule rule, CmControlRuleDef def) {
        mergeHelp((GspViewModel) extendModel, (ViewModelIncrement) extendIncrement, (ViewModelIncrement) baseIncrement, rule, def);
        mergeAction((GspViewModel) extendModel, (ViewModelIncrement) extendIncrement, (ViewModelIncrement) baseIncrement, rule, def);
    }

    //region merge help
    private void mergeHelp(GspViewModel extendModel, ViewModelIncrement extendIncrement, ViewModelIncrement baseIncrement, CmControlRule rule, CmControlRuleDef def) {
        var extendHelpConfigs = extendIncrement.getValueHelpConfigs();
        var baseHelpConfigs = baseIncrement.getValueHelpConfigs();
        if (extendHelpConfigs.size() < 1 && baseHelpConfigs.size() < 1)
            return;


        for (var helpConfigPair : baseHelpConfigs.entrySet()) {
            HelpConfigIncrement helpConfigIncrement = helpConfigPair.getValue();
            switch (helpConfigIncrement.getIncrementType()) {
                case Added:
                    mergeAddedHelp(extendModel, (AddedHelpIncrement) helpConfigIncrement, extendHelpConfigs, rule, def);
                    break;
                case Deleted:
                    extendModel.getValueHelpConfigs().remove(helpConfigIncrement);
                    break;
                case Modify:
                    dealModifyHelp(extendModel, (ModifyHelpIncrement) helpConfigIncrement, extendHelpConfigs, rule, def);
            }

        }
    }

    private void mergeAddedHelp(
            GspViewModel extendModel,
            AddedHelpIncrement helpConfigIncrement,
            HashMap<String, HelpConfigIncrement> extendHelpConfigs,
            CmControlRule rule,
            CmControlRuleDef def) {

//        if (isAllowAddChildObj(rule, def))
        extendModel.getValueHelpConfigs().add(helpConfigIncrement.getHelpConfig());
    }

    private void dealModifyHelp(
            GspViewModel extendModel,
            ModifyHelpIncrement helpConfigIncrement,
            HashMap<String, HelpConfigIncrement> extendIncrement,
            CmControlRule rule,
            CmControlRuleDef def) {
        ValueHelpConfig config = helpConfigIncrement.getHelpConfig();
        if(extendModel.getValueHelpConfigs().contains(config)){
            String newHelpId=config.getHelperId();
            String oldHelpId=extendModel.getValueHelpConfigs().getItem(config.getElementId()).getHelperId();
            //todo 帮助数据源变化
            if(!newHelpId.equals(oldHelpId)){
                extendModel.getValueHelpConfigs().remove(config);
                extendModel.getValueHelpConfigs().add(config);
            }else {
               //todo 帮助不变时，暂时不处理，需要后续讨论
            }
        }
    }
    //endregion


    //region merge actions
    private void mergeAction(GspViewModel extendModel, ViewModelIncrement extendIncrement, ViewModelIncrement baseIncrement, CmControlRule rule, CmControlRuleDef def) {
        var extendActions = extendIncrement.getActions();
        var baseActions = baseIncrement.getActions();
        if (extendActions.size() < 1 && baseActions.size() < 1)
            return;

        for (var actionPair : baseActions.entrySet()) {
            VoActionIncrement actionIncrement = actionPair.getValue();
            switch (actionIncrement.getIncrementType()) {
                case Added:
                    mergeAddedAction(extendModel, (AddedVoActionIncrement) actionIncrement, extendActions, rule, def);
                    break;
                case Deleted:
                    extendModel.getActions().removeById(actionIncrement.getActionId());
                    break;
                case Modify:
                    dealModifyAction(extendModel, (ModifyVoActionIncrement)actionIncrement, extendActions, rule, def);
            }

        }
    }


    private void mergeAddedAction(
            GspViewModel extendModel,
            AddedVoActionIncrement actionIncrement,
            HashMap<String, VoActionIncrement> extendHelpConfigs,
            CmControlRule rule,
            CmControlRuleDef def) {

//        if (isAllowAddChildObj(rule, def))
        extendModel.getActions().add(actionIncrement.getAction());
    }

    private void dealModifyAction(
            GspViewModel extendModel,
            ModifyVoActionIncrement baseActionIncrement,
            HashMap<String, VoActionIncrement> extendIncrement,
            CmControlRule rule,
            CmControlRuleDef def) {
        extendModel.getActions().removeById(baseActionIncrement.getActionId());
        extendModel.getActions().add(baseActionIncrement.getAction());
    }
    //endregion
}
