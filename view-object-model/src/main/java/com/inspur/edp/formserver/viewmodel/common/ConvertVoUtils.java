

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.common;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.collection.GspObjectCollection;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.GspViewModelElement;
import com.inspur.edp.formserver.viewmodel.GspViewObject;
import com.inspur.edp.formserver.viewmodel.collection.ViewObjectCollection;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoElementSourceType;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoObjectSourceType;
import com.inspur.edp.formserver.viewmodel.util.ViewModelUtils;
import java.util.ArrayList;
import java.util.Iterator;

//be上新增加的对象，字段等，同步到vo上，IDP用
public  class ConvertVoUtils {

    public static GspViewModel dealViewModel(GspBusinessEntity be, GspViewModel viewModel) {

        GspBizEntityObject bizObject=be.getMainObject();
        GspViewObject viewObject=viewModel.getMainObject();
        dealObject( be,bizObject,viewObject);
        dealActions( be, viewModel);
        return viewModel;
    }

    public static void dealBeRefElements(GspBusinessEntity be, GspViewModel model) {
        LinkBeUtils utils=new LinkBeUtils(true);
        utils.linkBeRefElements(model);

    }

    public static void dealActions(GspBusinessEntity be, GspViewModel model) {
        LinkBeUtils utils=new LinkBeUtils(true);
        utils.linkActions(model);
    }

    public static void dealObject(GspBusinessEntity be,GspBizEntityObject bizObject, GspViewObject viewObject) {
        dealObjectBasicInfo(bizObject,viewObject);

        dealElements(be,bizObject,viewObject);

        dealObjectSelfInfo(bizObject,viewObject);
        //联动中处理
//        dealModifyChildObjects(be,bizObject,viewObject);
        dealAddChildObjects(bizObject,viewObject);

    }

    public static void dealAddChildObjects(GspBizEntityObject bizObject, GspViewObject viewObject) {
        if(bizObject==null || bizObject.getContainChildObjects()==null || bizObject.getContainChildObjects().size()==0)
            return;
        GspObjectCollection beObjectCollection=bizObject.getContainChildObjects();
        for (IGspCommonObject beObj:beObjectCollection){
           if(isExistBizChildObject(viewObject,beObj.getID()))
               continue;
          IGspCommonObject newViewObject= ConvertUtils.toObject(beObj,viewObject.getMapping().getTargetMetadataPkgName(),viewObject.getMapping().getTargetMetadataId(),viewObject.getIDElement().getID(), GspVoObjectSourceType.BeObject);
           viewObject.getContainChildObjects().add(newViewObject);
        }

    }
    public static  boolean isExistedBizObject(GspBusinessEntity be,String objId){
        GspBizEntityObject bizObj = (GspBizEntityObject) be.getNode(node -> node.getID().equals(objId));
        if (bizObj==null)
            return false;
        return  true;
    }
    public static  GspBizEntityObject getBizObject(GspBusinessEntity be,String objId){
        GspBizEntityObject bizObj = (GspBizEntityObject) be.getNode(node -> node.getID().equals(objId));
        return  bizObj;
    }
    public static void dealModifyChildObjects(GspBusinessEntity be,GspBizEntityObject bizObject, GspViewObject viewObject) {
        if(viewObject.getContainChildObjects()==null  || viewObject.getContainChildObjects().size()==0)
            return;
        ViewObjectCollection childObjList = viewObject.getContainChildObjects();
        Iterator<IGspCommonObject> iterators=childObjList.iterator();
        while (iterators.hasNext()){
            GspViewObject childObj= (GspViewObject) iterators.next();
            if (childObj.getIsVirtual()) {
                continue;
            }
            String beChildObjId=((GspViewObject)childObj).getMapping().getTargetObjId();

            if (!isExistedBizObject(be,beChildObjId)) {
                iterators.remove();
                continue;
            }
            GspBizEntityObject childBizObject=getBizObject(be,beChildObjId);
            dealObject(be,childBizObject,(GspViewObject) childObj);
        }
//        for (IGspCommonObject childObj : childObjList) {
//            if (childObj.getIsVirtual()) {
//                continue;
//            }
//            String beChildObjId=((GspViewObject)childObj).getMapping().getTargetObjId();
//
//            if (!isExistedBizObject(be,beChildObjId)) {
//                viewObject.getContainChildObjects().remove(childObj);
//                continue;
//            }
//            GspBizEntityObject childBizObject=getBizObject(be,beChildObjId);
//            dealObject(be,childBizObject,(GspViewObject) childObj);
//        }
    }

    public static void dealObjectSelfInfo(GspBizEntityObject bizObject, GspViewObject viewObject) {
//        LinkBeUtils utils=new LinkBeUtils(true);
//        utils.linkObjectSelfInfo(viewObject,bizObject,null);
    }
    public static void dealElements(GspBusinessEntity be,GspBizEntityObject bizObject, GspViewObject viewObject) {
        //联动中处理
//        LinkBeUtils utils = new LinkBeUtils(true);
//        utils.linkElements(viewObject, be);
        //处理be上新增的字段
        dealBeAddElements(bizObject,viewObject);
    }

    public static void dealBeAddElements(GspBizEntityObject bizObject, GspViewObject viewObject) {
        ArrayList<IGspCommonField> bizElemens = bizObject.getContainElements().getAllItems(item -> item.getIsVirtual() == false);
        if(bizElemens==null ||bizElemens.size()==0)
            return;
        for (IGspCommonField beEle:bizElemens){
            if(isExistBizElement(viewObject,beEle.getID()))
                continue;
             IGspCommonElement newViewEle= ConvertUtils.toElement((IGspCommonElement) beEle,viewObject.getMapping().getTargetMetadataPkgName(),viewObject.getMapping().getTargetMetadataId(), GspVoElementSourceType.BeElement);
             newViewEle.setBelongObject(viewObject);
             viewObject.getContainElements().add(newViewEle);
        }

    }

    public static boolean isExistBizElement(GspViewObject bizObject, String elementId) {
        ArrayList<IGspCommonElement> list = bizObject.getAllElementList(false);
        for (IGspCommonElement ele : list) {
            String beIdInView=((GspViewModelElement)ele).getMapping().getTargetElementId();
            if (elementId.equals(beIdInView)) {
                return true;
            }
        }
        return false;
    }
    public static boolean isExistBizChildObject(GspViewObject bizObject, String objectId) {
        ViewObjectCollection viewObjectCollection=bizObject.getContainChildObjects();
        for (IGspCommonObject ele : viewObjectCollection) {
            String objectIdInView=((GspViewObject)ele).getMapping().getTargetObjId();
            if (objectId.equals(objectIdInView)) {
                return true;
            }
        }
        return false;
    }

    public static void dealObjectBasicInfo(GspBizEntityObject bizObject, GspViewObject viewObject) {
//        if (ViewModelUtils.checkNull(viewObject.getCode())) {
//            viewObject.setCode(bizObject.getCode());
//        }
////        viewObject.setIsVirtual(false);
//        viewObject.setIsReadOnly(bizObject.getIsReadOnly());
    }
}
