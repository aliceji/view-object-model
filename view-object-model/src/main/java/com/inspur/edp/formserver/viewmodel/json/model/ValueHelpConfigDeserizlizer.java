

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.entity.CustomizationInfo;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.common.ValueHelpConfig;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import com.inspur.edp.formserver.viewmodel.json.operation.VmActionCollectionDeserializer;
import java.io.IOException;

/**
 * The Josn Deserializer Of View Model Help Configuration
 *
 * @ClassName: ValueHelpConfigDeserizlizer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ValueHelpConfigDeserizlizer extends JsonDeserializer<ValueHelpConfig> {

  @Override
  public ValueHelpConfig deserialize(JsonParser jsonParser, DeserializationContext ctxt) {
    ValueHelpConfig config = new ValueHelpConfig();
    SerializerUtils.readStartObject(jsonParser);
    while (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
      String propName = SerializerUtils.readPropertyName(jsonParser);
      switch (propName) {
        case ViewModelJsonConst.HelperId:
          config.setHelperId(SerializerUtils.readPropertyValue_String(jsonParser));
          break;
        case ViewModelJsonConst.FilterExpression:
          config.setFilterExpression(SerializerUtils.readPropertyValue_String(jsonParser));
          break;
        case ViewModelJsonConst.ElementId:
          config.setElementId(SerializerUtils.readPropertyValue_String(jsonParser));
          break;
        case ViewModelJsonConst.EnableCustomHelpAuth:
          config.setEnableCustomHelpAuth(SerializerUtils.readPropertyValue_boolean(jsonParser));
          break;
        case CefNames.CustomizationInfo:
          config.setCustomizationInfo((CustomizationInfo) SerializerUtils
              .readPropertyValue_Object(CustomizationInfo.class, jsonParser));
          try {
            jsonParser.nextToken();
          } catch (IOException e) {
            throw new RuntimeException(
                String.format("GspCommonDataTypeDeserializer反序列化错误：%1$s", propName));
          }
          break;
        case ViewModelJsonConst.HelpExtend:
          SerializerUtils.readStartObject(jsonParser);
          if (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
            String propertyName = SerializerUtils.readPropertyName(jsonParser);
            switch (propertyName) {
              case ViewModelJsonConst.BeforeHelp:
                VmActionCollectionDeserializer deserializer1 = new VmActionCollectionDeserializer();
                config.getHelpExtend().setBeforeHelp(deserializer1.deserialize(jsonParser, null));
                break;
              default:
                throw new RuntimeException("未定义ValueHelpConfig.HelpExtend属性名" + propertyName);
            }
          }
          SerializerUtils.readEndObject(jsonParser);
          break;
        default:
          throw new RuntimeException("未定义ValueHelpConfig" + propName);
      }
    }
    SerializerUtils.readEndObject(jsonParser);
    return config;
  }
}
