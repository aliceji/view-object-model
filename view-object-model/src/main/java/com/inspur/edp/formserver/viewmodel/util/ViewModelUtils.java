

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.util;

import java.io.File;
import java.nio.file.Paths;

/**
 * VO模型工具类
 *
 * @author haoxiaofei
 */
public class ViewModelUtils {

  /**
   * 检查空对象，空字符串
   */
  public static boolean checkNull(Object propValue) {
    if (propValue == null) {
      return true;
    }
    if (propValue.getClass().isAssignableFrom(String.class)) {
      String stringValue = (String) propValue;
      if (stringValue == null || stringValue.isEmpty()) {
        return true;
      }
    }
    return false;
  }

  public static String getDirectoryName(String path) {
    return new File(path).getParent();
  }

  public static String getCombinePath(String path1, String path2) {
    String path = Paths.get(path1).resolve(path2).toString();
    return handlePath(path);
  }
  public static String getCombinePath(String path1, String path2, String path3) {
    String path = Paths.get(path1).resolve(path2).resolve(path3).toString();
    return handlePath(path);
  }

  public static String handlePath(String path) {
    return path.replace("\\", File.separator);
  }

  public  static String getSeparator(){
    return File.separator;
  }
  public static String getFileNameWithoutExtension(String path) {
    File file = new File(path);
    if (file.isDirectory()) {
      final String message = path + " is  a directory";
      throw new IllegalArgumentException(message);
    }
    String fileName = file.getName();
    int point = fileName.lastIndexOf(".");
    if (point == -1) {
      return fileName;
    } else {
      return fileName.substring(0, point);
    }
  }
}
