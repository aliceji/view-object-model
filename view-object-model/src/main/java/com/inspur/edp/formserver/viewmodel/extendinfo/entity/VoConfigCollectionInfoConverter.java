

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.extendinfo.entity;

import com.inspur.edp.cdp.common.utils.json.JsonUtil;
import io.swagger.util.Json;
import javax.persistence.AttributeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * config信息转换
 *
 * @author haoxiaofei
 */
public class VoConfigCollectionInfoConverter implements AttributeConverter<VoConfigCollectionInfo, String> {
    private static Logger logger = LoggerFactory.getLogger(VoConfigCollectionInfo.class);

    @Override
    public String convertToDatabaseColumn(VoConfigCollectionInfo attribute) {
        return Json.pretty(attribute);
       // return JsonSerializerUtils.writeValueAsString(attribute);
    }

    @Override
    public VoConfigCollectionInfo convertToEntityAttribute(String dbData) {
        if (dbData == null || dbData.isEmpty()) {
            return new VoConfigCollectionInfo();
        }
        VoConfigCollectionInfo configCollectionInfo = null;
        try {
            configCollectionInfo = JsonUtil.toObject(dbData, VoConfigCollectionInfo.class);
        } catch (JsonUtil.JsonParseException e) {
            logger.error("反序列化出错："+ dbData, e);
        }
        return configCollectionInfo;
    }
}
