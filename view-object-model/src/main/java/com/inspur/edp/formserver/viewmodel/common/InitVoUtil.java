

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.common;

import com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
import com.inspur.edp.cef.designtime.api.util.Guid;
import com.inspur.edp.das.commonmodel.entity.object.GspColumnGenerate;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.GspViewModelElement;
import com.inspur.edp.formserver.viewmodel.GspViewObject;
/**
 * The Tool Of  View  Model Init
 *
 * @ClassName: InitVoUtil
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public final class InitVoUtil {
    public static GspViewModel BuildVirtualVo(String metadataID, String metadataName, String metadataCode, String metadataAssembly) {
        GspViewModel vo = InitVo(metadataID, metadataName, metadataCode, metadataAssembly, true);
        return vo;
    }

    private static GspViewModel InitVo(String metadataID, String metadataName, String metadataCode, String metadataAssembly, boolean isVirtual) {
        GspViewModel vo = new GspViewModel();
        vo.setID(metadataID);
        vo.setCode(metadataCode);
        vo.setName(metadataName);
        vo.setDotnetGeneratingAssembly(metadataAssembly);
        vo.setIsUseNamespaceConfig(true);
        vo.setSimplifyGen(true);
        GspViewObject mainObject = InitMainObject(metadataName, metadataCode, isVirtual);
        vo.setMainObject(mainObject);
        // vo
        if (isVirtual) {
            vo.setIsVirtual(true);
            vo.setMapping(null);
        }
        return vo;
    }

    private static GspViewObject InitMainObject(String objName, String objCode, boolean isVirtual) {
        GspViewObject mainObj = new GspViewObject();
        GspViewModelElement idElement = GetIDElement(isVirtual);
        mainObj.setID(Guid.newGuid().toString());
        mainObj.setCode(objCode);
        mainObj.setName(objName);
        GspColumnGenerate columnGenerate=new GspColumnGenerate();
        columnGenerate.setElementID(idElement.getID());
        columnGenerate.setGenerateType("Guid");
        mainObj.setColumnGenerateID(columnGenerate);
        mainObj.getContainElements().add(idElement);
        // voObject
        if (isVirtual) {
            mainObj.setIsVirtual(true);
            mainObj.setMapping(null);
        }
        return mainObj;
    }

    private static GspViewModelElement GetIDElement(boolean isVirtual) {
        GspViewModelElement idElement = new GspViewModelElement();
        idElement.setID(Guid.newGuid().toString());
        idElement.setCode("ID");
        idElement.setName("ID");
        idElement.setLabelID("ID");
        idElement.setMDataType(GspElementDataType.String);
        idElement.setObjectType(GspElementObjectType.None);
        idElement.setLength(36);
        idElement.setPrecision(0);
        idElement.setIsRequire(true);
        // voElemet
        if (isVirtual) {
            idElement.setIsVirtual(true);
            idElement.setIsVirtualViewElement(true);
            idElement.setMapping(null);
        }
        return idElement;
    }

}
