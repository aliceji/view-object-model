

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs;

public class ChangingVoFieldLabelIdEventArgs extends  AbstractVofieldEventArgs{
  protected String newLabelId;
  protected String originalLabelId;

  public ChangingVoFieldLabelIdEventArgs() {
  }

  public ChangingVoFieldLabelIdEventArgs(String newLabelId, String originalLabelId) {
    this.newLabelId = newLabelId;
    this.originalLabelId = originalLabelId;
  }

  public String getNewLabelId() {
    return newLabelId;
  }

  public void setNewLabelId(String newLabelId) {
    this.newLabelId = newLabelId;
  }

  public String getOriginalLabelId() {
    return originalLabelId;
  }

  public void setOriginalLabelId(String originalLabelId) {
    this.originalLabelId = originalLabelId;
  }
}
