

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.increment;

import com.inspur.edp.das.commonmodel.json.increment.CommonElementIncrementSerializer;
import com.inspur.edp.das.commonmodel.json.increment.CommonObjectIncrementSerializer;
import com.inspur.edp.das.commonmodel.json.object.CmObjectSerializer;
import com.inspur.edp.formserver.viewmodel.json.object.ViewObjectSerializer;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewObjectIncrementSerializer extends CommonObjectIncrementSerializer {
    @Override
    protected CmObjectSerializer getCommonObjectSerializer() {
        return new ViewObjectSerializer();
    }

    @Override
    protected CommonElementIncrementSerializer getCmElementIncrementSerizlizer() {
        return new ViewElementIncrementSerializer();
    }

    @Override
    protected CommonObjectIncrementSerializer getCmObjectIncrementSerializer() {
        return new ViewObjectIncrementSerializer();
    }
}
