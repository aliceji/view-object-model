

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.mapping;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoObjectMapping;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoObjectSourceType;
import com.inspur.edp.formserver.viewmodel.common.ViewModelMapping;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;

/**
 * The  Josn Deserializer Of View Model Object Mapping
 *
 * @ClassName: GspVoObjectMappingDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspVoObjectMappingDeserializer extends ViewModelMappingDeserializer {
    @Override
    protected GspVoObjectMapping createVmMapping() {
        return new GspVoObjectMapping();
    }

    @Override
    protected boolean readExtendMappingProperty(JsonParser reader, ViewModelMapping mapping, String propertyName) {
        boolean hasProperty = true;
        GspVoObjectMapping voMapping = (GspVoObjectMapping) mapping;
        switch (propertyName) {
            case ViewModelJsonConst.SourceType:
                voMapping.setSourceType(SerializerUtils.readPropertyValue_Enum(reader, GspVoObjectSourceType.class, GspVoObjectSourceType.values(), GspVoObjectSourceType.VoObject));
                break;
            default:
                hasProperty = false;
                break;
        }
        return hasProperty;
    }
}
