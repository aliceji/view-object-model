
/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.action.internalexternalaction;

import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;

/**
 * The Definition Of Changeset Reversal Mapping Action
 *
 * @ClassName: ChangesetReversalMappingAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ChangesetReversalMappingAction extends MappedCdpAction implements IInternalExtendAction
{
	public static final String id = "301c5991-a32d-4221-88bf-8c9d07bdd884";
	public static final String code = "ChangesetReversalMapping";
	public static final String name = "内置变更集反向Mapping操作";
	public ChangesetReversalMappingAction()
	{
		setID(id);
		setCode(code);
		setName(name);
	}
}
