

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.action.mappedcdp.MappedCdpActionParameterCollection;
import com.inspur.edp.formserver.viewmodel.action.viewmodelbase.ViewModelParameterCollection;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
/**
 * The Json Deserializer Of Mapped Component Action Definition
 *
 * @ClassName: MappedCdpActionDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedCdpActionDeserializer extends VmActionDeserializer<MappedCdpAction> {
    @Override
    protected MappedCdpAction createOp() {
        return new MappedCdpAction();
    }

    @Override
    protected void beforeVnactionDeserializer(ViewModelAction op) {
        MappedCdpAction action = (MappedCdpAction)op;
        action.setIsGenerateComponent(true);
        action.setComponentPkgName("");
    }

    @Override
    protected VmParameterDeserializer createPrapDeserializer() {
        return new MappedCdpParaDeserializer();
    }

    @Override
    protected ViewModelParameterCollection createPrapCollection() {
        return new MappedCdpActionParameterCollection();
    }

    @Override
    protected boolean readExtendOpProperty(ViewModelAction op, String propName, JsonParser reader) {
        MappedCdpAction action = (MappedCdpAction)op;
        boolean hasProperty = true;
        switch (propName)
        {
            case ViewModelJsonConst.ComponentPkgName:
                action.setComponentPkgName(SerializerUtils.readPropertyValue_String(reader));
                break;
            case ViewModelJsonConst.ComponentEntityId:
                action.setComponentEntityId(SerializerUtils.readPropertyValue_String(reader));
                break;
            case ViewModelJsonConst.IsGenerateComponent:
                action.setIsGenerateComponent(SerializerUtils.readPropertyValue_boolean(reader));
                break;
            default:
                hasProperty = false;
                break;
        }
        return hasProperty;
    }
}
