

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.viewmodeldtevent;

import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.actioneventargs.ChangingVoActionCodeEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.actioneventargs.ChangingVoActionCollectTypeEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.actioneventargs.ChangingVoActionParamsEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.actioneventargs.ChangingVoActionReturnEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.actioneventargs.DeletingVoActionEventArgs;
import io.iec.edp.caf.commons.event.IEventListener;
/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface IVoActionDTEventListener extends IEventListener {

  public ChangingVoActionCodeEventArgs changingVoActionCode(ChangingVoActionCodeEventArgs args);

  public ChangingVoActionParamsEventArgs changingVoActionParams(
      ChangingVoActionParamsEventArgs args);

  public ChangingVoActionReturnEventArgs changingVoActionReturn(
      ChangingVoActionReturnEventArgs args);

  public DeletingVoActionEventArgs deletingVoAction(DeletingVoActionEventArgs args);
  public ChangingVoActionCollectTypeEventArgs changingVoActionCollectType(ChangingVoActionCollectTypeEventArgs args);
}
