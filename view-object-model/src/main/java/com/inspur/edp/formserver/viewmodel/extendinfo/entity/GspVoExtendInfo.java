

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.extendinfo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.Convert;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@Entity
@Table(name = "GspViewModel")
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GspVoExtendInfo {

    public GspVoExtendInfo(String id, String extendInfo, String configId, String createdBy,
        Date createdOn, String lastChangedBy, Date lastChangedOn) {
        this.id = id;
        this.extendInfo = extendInfo;
        this.configId = configId;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.lastChangedBy = lastChangedBy;
        this.lastChangedOn = lastChangedOn;
    }

    public GspVoExtendInfo(String id, String extendInfo, String configId, String createdBy,
        Date createdOn, String lastChangedBy, Date lastChangedOn, String beSourceId) {
        this(id,extendInfo,configId,createdBy,createdOn,lastChangedBy,lastChangedOn);
        this.beSourceId = beSourceId;
    }

    public GspVoExtendInfo(String id, String extendInfo, String configId, String createdBy,
        Date createdOn, String lastChangedBy, Date lastChangedOn, String beSourceId,
        VoConfigCollectionInfo voConfigCollectionInfo) {
        this(id, extendInfo, configId, createdBy, createdOn, lastChangedBy, lastChangedOn, beSourceId);
        this.voConfigCollectionInfo = voConfigCollectionInfo;
    }

    /**
     * ID
     */
    @Id
    private String id;

    /**
     * 扩展信息
     */
    private String extendInfo;

    /**
     * config ID
     */
    private String configId;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private Date createdOn;

    /**
     * 最后修改人
     */
    private String lastChangedBy;

    /**
     * 最后修改时间
     */
    private Date lastChangedOn;

    /**
     * 业务实体ID
     */
    private String beSourceId;

    /**
     * config信息
     */
    @Convert(converter = VoConfigCollectionInfoConverter.class)
    private VoConfigCollectionInfo voConfigCollectionInfo;

    public VoConfigCollectionInfo getVoConfigCollectionInfo() {
        return voConfigCollectionInfo;
    }

    public void setVoConfigCollectionInfo(VoConfigCollectionInfo voConfigCollectionInfo) {
        this.voConfigCollectionInfo = voConfigCollectionInfo;
    }
}
