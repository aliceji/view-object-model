

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.json.increment;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.entity.GspCommonDataType;
import com.inspur.edp.cef.designtime.api.entity.increment.AddedEntityIncrement;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.object.GspCommonDataTypeDeserializer;
import com.inspur.edp.formserver.viewmodel.common.ValueHelpConfig;
import com.inspur.edp.formserver.viewmodel.increment.entity.AddedHelpIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.HelpConfigIncrement;

import com.inspur.edp.formserver.viewmodel.increment.entity.ModifyHelpIncrement;
import java.io.IOException;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class HelpConfigIncrementDeserializer extends JsonDeserializer<HelpConfigIncrement> {

    @Override
    public HelpConfigIncrement deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(jsonParser);
        String incrementTypeStr = node.get(CefNames.IncrementType).textValue();
        if(incrementTypeStr == null || "".equals(incrementTypeStr))
            return null;
        IncrementType incrementType = IncrementType.valueOf(incrementTypeStr);
        switch (incrementType){
            case Added:
                return readAddIncrementInfo(node);
            case Modify:
                return readModifyIncrementInfo(node);
            case Deleted:
//                return readDeletedIncrementInfo(node);

        }
        return null;
    }

    private ModifyHelpIncrement readModifyIncrementInfo(JsonNode node) {
        ModifyHelpIncrement modifyHelpIncrement=new ModifyHelpIncrement();
        return modifyHelpIncrement;
    }

    private AddedHelpIncrement readAddIncrementInfo(JsonNode node){
        AddedHelpIncrement addIncrement = new AddedHelpIncrement();
        readBaseAddedInfo(addIncrement,node);
        return addIncrement;
    }

    private void readBaseAddedInfo(AddedHelpIncrement addIncrement, JsonNode node){
        JsonNode addVauleNode = node.get(CefNames.AddedDataType);
        if(addVauleNode == null)
            return;

        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        try {
            addIncrement.setHelpConfig((ValueHelpConfig)mapper.readValue(mapper.writeValueAsString(addVauleNode), ValueHelpConfig.class));
        } catch (IOException e) {
            throw new RuntimeException("IGspCommonDataType反序列化失败", e);
        }
    }
}
