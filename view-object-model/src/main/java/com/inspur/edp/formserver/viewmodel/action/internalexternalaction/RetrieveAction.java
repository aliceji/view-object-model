

/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.action.internalexternalaction;

import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;

/**
 * The Definition Of Retrieve Action
 *
 * @ClassName: RetrieveAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class RetrieveAction extends MappedCdpAction implements IInternalExtendAction
{
	public static final String id = "7a02f472-5bbd-424b-9d9e-f82e3f9f448e";
	public static final String code = "Retrieve";
	public static final String name = "内置检索操作";
	public RetrieveAction()
	{
		setID(id);
		setCode(code);
		setName(name);
	}
}
