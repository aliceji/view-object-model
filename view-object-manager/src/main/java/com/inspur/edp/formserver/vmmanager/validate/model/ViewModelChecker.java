/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.validate.model;

import com.inspur.edp.das.commonmodel.entity.GspCommonModel;
import com.inspur.edp.das.commonmodel.validate.model.CommonModelChecker;
import com.inspur.edp.das.commonmodel.validate.object.CMObjectChecker;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.vmmanager.validate.object.ViewObjInspaction;
import com.inspur.edp.formserver.vmmanager.validate.operation.ActionChecker;

public class ViewModelChecker extends CommonModelChecker {
    private static ViewModelChecker viewModelChecker;

    public static ViewModelChecker getInstance() {
        if (viewModelChecker == null) {
            viewModelChecker = new ViewModelChecker();
        }
        return viewModelChecker;
    }

    public final void check(GspViewModel viewModel) {
        checkCM(viewModel);
    }

    @Override
    protected void checkExtension(GspCommonModel viewModel) {
        ActionChecker.getInstance().checkVMAction((GspViewModel) viewModel);
    }

    @Override
    protected CMObjectChecker getCMObjectChecker() {
        return ViewObjInspaction.getInstance();
    }
}
