/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.util;

import java.io.File;
import java.nio.file.Paths;

public class CheckInfoUtil {

		/**
		 * 检查必填项是否为空
		 *
		 * @param propName
		 * @param propValue
		 */
		public static void checkNessaceryInfo(String propName, Object propValue) {
				if (checkNull(propValue)) {
						throw new RuntimeException(propName + "不可为空");
				}
		}


		/**
		 * 检查空对象，空字符串
		 *
		 * @param propValue
		 * @return
		 */
		public static boolean checkNull(Object propValue) {
				if (propValue == null) {
						return true;
				}
				if (propValue.getClass().isAssignableFrom(String.class)) {
						String stringValue = (String) propValue;
						if (stringValue == null || stringValue.isEmpty()) {
								return true;
						}
				}
				return false;
		}

	public static String getCombinePath(String path1, String path2) {
		String path = Paths.get(path1).resolve(path2).toString();
		return handlePath(path);
	}

	public static String handlePath(String path) {
		return path.replace("\\", File.separator);
	}
}
