/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.lowcode;

import com.inspur.edp.metadata.rtcustomization.spi.event.ExtMdSavedArgs;
import com.inspur.edp.metadata.rtcustomization.spi.event.IMetadataDeployEventListener;
import com.inspur.edp.metadata.rtcustomization.spi.event.MdPkgDeployedEventArgs;

/**
 * 元数据工具部署监听器
 *
 * @author haoxiaofei
 */
public class MdPkgToolsDeployListener implements IMetadataDeployEventListener {


  public MdPkgToolsDeployListener() {
  }

  @Override
  public void fireMdPkgDeployedEvent(MdPkgDeployedEventArgs mdPkgDeployedEventArgs) {
  }

  @Override
  public void fireExtMdSavedEvent(ExtMdSavedArgs extMdSavedArgs) {
  }
}
