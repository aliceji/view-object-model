/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.service;

import com.inspur.edp.bef.component.detailcmpentity.udtdetermination.UDTDtmComponent;
import com.inspur.edp.bef.component.detailcmpentity.vm.VMComponent;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.util.ViewModelUtils;
import com.inspur.edp.formserver.vmmanager.createvmmetadata.ComponentGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.JavaCodeFileGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.genutils.VoCommonUtilsGenerator;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import com.inspur.edp.lcm.metadata.api.service.RefCommonService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.ArrayList;

public class VmManagerService {
  public static void saveMetadata(GspMetadata metadata, String metadataPath)
  {
    SpringBeanUtils.getBean(MetadataService.class).saveMetadata(metadata, ViewModelUtils.getCombinePath(metadataPath, metadata.getHeader().getFileName()));
  }


  
//ORIGINAL LINE: public static ArrayList generateComponent(GspMetadata metadata, string metadataPath, bool isSave = true)
  public static ArrayList generateComponent(GspMetadata metadata, String metadataPath, boolean isSave)
  {
    GspViewModel vm = (GspViewModel)((metadata.getContent() instanceof GspViewModel) ? metadata.getContent() : null);
    String bizObjectID = metadata.getHeader().getBizobjectID();
    return ComponentGenerator.getInstance().GenerateComponent(vm, metadataPath, bizObjectID);
    //if (isSave)
    //    saveMetadata(metadata, metadataPath);
  }

  public static String getComponentJavaClassName(String componentEntityId)
  {
    RefCommonService lcmDtService = SpringBeanUtils.getBean(RefCommonService.class);
    GspMetadata metadata = lcmDtService.getRefMetadata(componentEntityId);
    String javaClassName = null;
    if (metadata.getContent() instanceof VMComponent)
    {
      javaClassName = ((VMComponent)metadata.getContent()).getVmMethod().getClassName();
    }
    if (metadata.getContent() instanceof UDTDtmComponent)
    {
      javaClassName = ((UDTDtmComponent)metadata.getContent()).getUdtDtmMethod().getClassName();
    }
    if (javaClassName == null || !javaClassName.contains("."))
    {
      throw new RuntimeException("没有找到构件，构件ID为:" + componentEntityId);
    }
    return javaClassName;
  }

  //public static bool IsMetaProjJava(string metadataPath)
  //{
  // string ProjectPath = metadataPath;
  // ProjectPath =
  //  ProjectPath.Remove(ProjectPath.LastIndexOf("/"), ProjectPath.Length - (ProjectPath.LastIndexOf("/")));

  // var service = ServiceManager.GetService<IMetadataProjectService>();
  // var projInfo = service.GetGspProjectInfo(ProjectPath);

  //       if ((projInfo.ProjectExtendItem != null) && (projInfo.ProjectExtendItem.Count > 0))
  //       {
  //           if (projInfo.ProjectExtendItem[0].TryGetValue("codelanguage", out var result) && result.Equals("java"))
  //           {
  //               return true;
  //           }
  //       }

  //           return false;

  //   }
//        *
//         * actionList 生成构件的动作编号集合（初次生成构件才生成构件代码）
//
  public static void generateComponentCode(GspMetadata metadata, String metadataPath, ArrayList actionList)
  {

    String ProjectPath = metadata.getRelativePath();

    new JavaCodeFileGenerator(metadata).generate(actionList);
    new VoCommonUtilsGenerator((GspViewModel) metadata.getContent(),metadata).generateCommonUtils();
  }


  /**
   判断当前是否创建Java模板，创建返回值为true,不能创建返回flase;

   @param ProjectPath
   @return
   */
  public static boolean creatJavaModule(String ProjectPath)
  {
    boolean moduleCreated = true;
//
//    MetadataService service =SpringBeanUtils.getBean(MetadataService.class);
//    GspProject projInfo = service.getGspProjectInfo(ProjectPath);
//
//
//    if ((projInfo.getProjectExtendItem() != null) && (projInfo.getProjectExtendItem().size() > 0))
//    {
//      if ((projInfo.getProjectExtendItem().get(0).containsKey("codelanguage")) && (result.Contains("java")))
//      {
//        moduleCreated = true;
//      }
//    }
    return moduleCreated;
  }


  /**
   判断当前是否创建donet模板，创建返回值为true,不能创建返回flase;

   @param
   @return
   */
//  public static boolean CreatDotnetModule(String ProjectPath)
//  {
//    boolean moduleCreated = false;
//
//    // ProjectPath = ProjectPath.Remove(ProjectPath.LastIndexOf("/"), ProjectPath.Length - (ProjectPath.LastIndexOf("/")));
//    var service = ServiceManager.<IMetadataProjectService>GetService();
//    var projInfo = service.GetGspProjectInfo(ProjectPath);
//
//    if ((projInfo.ProjectExtendItem != null) && (projInfo.ProjectExtendItem.size() > 0))
//    {
//      if ((projInfo.ProjectExtendItem[0].TryGetValue("codelanguage", out var result)) && (!result.Contains("java")))
//      {
//        //如果键值对存在并且不存在java字符串（java模版没有生成）
//        moduleCreated = true;
//      }
//      else
//      {
//        //如果java模版生成，并且检测字符串中有donet
//        if (result.Contains("dotnet"))
//        {
//          moduleCreated = true;
//        }
//      }
//
//    } //如果存储键值list对为空或者键值对不存在)
//    else
//    {
//      moduleCreated = true;
//    }
//    return moduleCreated;
//  }

//  public static void PreBuild(GspMetadata metadata, String metadataPath)
//  {
//    //VmGeneratorService.PreGenerate(metadataPath, metadata);
//    ServiceManager.<IMetadataJitService>GetService().GenerateApi(metadataPath);
//  }
}
