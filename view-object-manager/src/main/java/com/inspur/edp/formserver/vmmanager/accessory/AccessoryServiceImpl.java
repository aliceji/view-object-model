/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.accessory;

import com.inspur.common.component.api.service.workflow.ApprovalCommentsPropMapping;
import com.inspur.edp.bef.component.detailcmpentity.vm.VMComponent;
import com.inspur.edp.bef.component.detailcmpentity.vm.VMMethodParameter;
import com.inspur.edp.cef.designtime.api.element.GspAssociationKey;
import com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import com.inspur.edp.cef.designtime.api.util.Guid;
import com.inspur.edp.cef.designtime.api.util.MetadataUtil;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.entity.object.GspColumnGenerate;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.GspViewModelElement;
import com.inspur.edp.formserver.viewmodel.GspViewObject;
import com.inspur.edp.formserver.viewmodel.accessory.AccessoryService;
import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelActionType;
import com.inspur.edp.formserver.viewmodel.action.mappedcdp.MappedCdpActionParameter;
import com.inspur.edp.formserver.viewmodel.common.VMCollectionParameterType;
import com.inspur.edp.formserver.viewmodel.common.VMParameterType;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.task.entity.CommentFieldInfo;
import com.inspur.edp.task.payload.GetTaskLogsPayload;
import com.inspur.edp.task.service.TaskCommentService;
import io.iec.edp.caf.common.JSONSerializer;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.rpc.client.RpcClassHolder;

import java.util.*;

public class AccessoryServiceImpl implements AccessoryService {

  @Override
  public void addApprovalComments(GspViewModel model, String parentNodeCode,
      String processInstLabelId, boolean includeBacklog) {
    Objects.requireNonNull(model, "model");
    Objects.requireNonNull(parentNodeCode, "parentNodeCode");
    Objects.requireNonNull(processInstLabelId, "processInstLabelId");

    GspViewObject node = (GspViewObject) model.findObjectByCode(parentNodeCode);
    if (node == null) {
      throw new RuntimeException("找不到节点" + model.getCode() + "." + parentNodeCode);
    }
    GspViewModelElement element = (GspViewModelElement) node
        .getElementByLabelId(processInstLabelId);
    if (element == null) {
      throw new RuntimeException("找不到流程实例字段" + model.getCode() + "." + processInstLabelId);
    }

    GspViewObject childObj = addVirtualChildObject(model, node);
    addAfterRetrieveAction(model, childObj, processInstLabelId,RETRIEVE_ACTION_APPROVALPROCESSINS_ID,RETRIEVE_ACTION_APPROVALPROCESSINS_LABELID,RETRIEVE_ACTION_APPROVALCHILDCODE,includeBacklog);
  }

  @Override
  public void addApprovalWorkItenLogs(GspViewModel model, String nodeCode,
      String billCodeLabelId, boolean includeBacklog) {

    Objects.requireNonNull(model, "model");
    Objects.requireNonNull(nodeCode, "parentNodeCode");
    Objects.requireNonNull(billCodeLabelId, "billCodeLabelId");

    GspViewObject node = (GspViewObject) model.findObjectByCode(nodeCode);
    if (node == null) {
      throw new RuntimeException("找不到节点" + model.getCode() + "." + nodeCode);
    }
    GspViewModelElement element = (GspViewModelElement) node
        .getElementByLabelId(billCodeLabelId);
    if (element == null) {
      throw new RuntimeException("找不到单据内码字段" + model.getCode() + "." + billCodeLabelId);
    }

    GspViewObject childObj = addVirtualChildObject(model, node);
    addAfterRetrieveAction(model, childObj, billCodeLabelId,RETRIEVE_ACTION_APPROVALBILLCODE_ID,RETRIEVE_ACTION_APPROVALBILLCODE_LABELID,RETRIEVE_ACTION_APPROVALCHILDCODE,includeBacklog);
  }

  private static final String COL_ID = "ID";
  private static final String COL_ParentID = "ParentID";
  private static final String COL_ProcessInstanceId = "ProcessInstanceId";
  private static final String COL_ProcessInstanceId_Name = "流程实例标识符";
  private static final String COL_RootProcessInstanceId = "RootProcessInstanceId";
  private static final String COL_RootProcessInstanceId_Name = "父流程实例标识符";
  private static final String COL_ActivityInstanceId = "ActivityInstanceId";
  private static final String COL_ActivityInstanceId_Name = "活动实例标识符";
  private static final String COL_ActivityName = "ActivityName";
  private static final String COL_ActivityName_Name = "活动实例名称";
  private static final String COL_WorkItemId = "WorkItemId";
  private static final String COL_WorkItemId_Name = "工作项标识符";
  private static final String COL_OperatorId = "OperatorId";
  private static final String COL_OperatorId_Name = "操作人标识符";
  private static final String COL_OperatorName = "OperatorName";
  private static final String COL_OperatorName_Name = "操作人名称";
  private static final String COL_Type = "Type";
  private static final String COL_Type_Name = "日志类型";
  private static final String COL_ActionCode = "Action";
  private static final String COL_ActionCode_Name = "审批动作编号";
  private static final String COL_ActionName = "ActionName";
  private static final String COL_ActionName_Name = "审批动作名称";
  private static final String COL_Message = "Message";
  private static final String COL_Message_Name = "审批信息";
  private static final String COL_Time = "Time";
  private static final String COL_Time_Name = "审批时间";
  private static final String COL_Attachment = "Attachment";
  private static final String COL_Attachment_Name = "附件标识符";
  private static final String COL_SignatureImage = "SignatureImage";
  private static final String COL_SignatureImage_Name = "签名图片";

  //流程实例构件元数据
  private static final String RETRIEVE_ACTION_COMP_ID = "91ee8167-891b-4308-afdf-db4319a918dd";
  private static final String RETRIEVE_ACTION_PAR_CHILD_CODE = "commentsChildCode";
  private static final String RETRIEVE_ACTION_PAR_PROCESS_INST_LABELID = "processInstLabelId";
  private static final String RETRIEVE_ACTION_PAR_PROPERTY_MAPPING = "propMapping";

  //单据内码构件元数据
  private static final String RETRIEVE_ACTION_BILLODE_ID = "d3fc5bb2-6de0-4413-842c-03e7f7fffc83";
  private static final String RETRIEVE_ACTION_BILLODE_CHILD_CODE = "workItemLogChildCode";
  private static final String RETRIEVE_ACTION_BILLODE_LABELID = "billCodeLabelId";

  private static final String RETRIEVE_ACTION_APPROVALBILLCODE_ID="3645d756-4b57-4d12-99e2-db5453e598ad";
  private static final String RETRIEVE_ACTION_APPROVALBILLCODE_LABELID="commentsLabelId";
  private static final String RETRIEVE_ACTION_APPROVALCHILDCODE="commentsChildCode";
  private static final String RETRIEVE_ACTION_APPROVALGETLOGS_PAYLOAD = "taskLogsPayload";

  private static final String RETRIEVE_ACTION_APPROVALPROCESSINS_ID="727f9648-ce0d-43ab-9ddd-b421affc4b31";
  private static final String RETRIEVE_ACTION_APPROVALPROCESSINS_LABELID="commentsLabelId";

  //添加vo子表信息
  private GspViewObject addChildObject(GspViewModel model, GspViewObject parentObj) {
    GspViewObject childObj = new GspViewObject();
    childObj.setID(Guid.newGuid().toString());
    String nodeCode = "ApprovalComment";
    int index = 0;
    while (model.findObjectByCode(nodeCode) != null) {
      nodeCode = "ApprovalComment" + (++index);
    }
    childObj.setCode(nodeCode);
    childObj.setName("审批日志" + (index == 0 ? "" : index));
    childObj.setIsVirtual(true);
    childObj.setParent(parentObj);

    //id
    GspViewModelElement idElement = new GspViewModelElement();
    idElement.setID(Guid.newGuid().toString());
    idElement.setCode(COL_ID);
    idElement.setName(COL_ID);
    idElement.setLabelID(COL_ID);
    idElement.setIsVirtualViewElement(true);
    idElement.setIsVirtual(true);
    idElement.setMDataType(GspElementDataType.String);
    idElement.setLength(36);
    childObj.getContainElements().add(idElement);
    GspColumnGenerate genId = new GspColumnGenerate();
    genId.setElementID(idElement.getID());
    childObj.setColumnGenerateID(genId);

    //parentid
    GspViewModelElement parentIdElement = new GspViewModelElement();
    parentIdElement.setID(Guid.newGuid().toString());
    parentIdElement.setCode(COL_ParentID);
    parentIdElement.setName(COL_ParentID);
    parentIdElement.setLabelID(COL_ParentID);
    parentIdElement.setIsVirtualViewElement(true);
    parentIdElement.setIsVirtual(true);
    parentIdElement.setMDataType(GspElementDataType.String);
    parentIdElement.setLength(36);
    childObj.getContainElements().add(parentIdElement);
    GspAssociationKey assoKey = new GspAssociationKey();
    assoKey.setSourceElement(parentIdElement.getID());
    assoKey.setSourceElementDisplay(parentIdElement.getName());
    assoKey.setTargetElement(parentObj.getIDElement().getID());
    assoKey.setTargetElementDisplay(parentObj.getIDElement().getName());
    childObj.getKeys().add(assoKey);

    //processInstanceId
    GspViewModelElement processInstanceIdElement = new GspViewModelElement();
    processInstanceIdElement.setID(Guid.newGuid().toString());
    processInstanceIdElement.setCode(COL_ProcessInstanceId);
    processInstanceIdElement.setName(COL_ProcessInstanceId_Name);
    processInstanceIdElement.setLabelID(COL_ProcessInstanceId);
    processInstanceIdElement.setIsVirtualViewElement(true);
    processInstanceIdElement.setIsVirtual(true);
    processInstanceIdElement.setMDataType(GspElementDataType.String);
    processInstanceIdElement.setLength(36);
    childObj.getContainElements().add(processInstanceIdElement);

    //rootprocessInstanceId
    GspViewModelElement rootProcessInstanceIdElement = new GspViewModelElement();
    rootProcessInstanceIdElement.setID(Guid.newGuid().toString());
    rootProcessInstanceIdElement.setCode(COL_RootProcessInstanceId);
    rootProcessInstanceIdElement.setName(COL_RootProcessInstanceId_Name);
    rootProcessInstanceIdElement.setLabelID(COL_RootProcessInstanceId);
    rootProcessInstanceIdElement.setIsVirtualViewElement(true);
    rootProcessInstanceIdElement.setIsVirtual(true);
    rootProcessInstanceIdElement.setMDataType(GspElementDataType.String);
    rootProcessInstanceIdElement.setLength(36);
    childObj.getContainElements().add(rootProcessInstanceIdElement);

    //activityInstanceId
    GspViewModelElement activityInstanceIdElement = new GspViewModelElement();
    activityInstanceIdElement.setID(Guid.newGuid().toString());
    activityInstanceIdElement.setCode(COL_ActivityInstanceId);
    activityInstanceIdElement.setName(COL_ActivityInstanceId_Name);
    activityInstanceIdElement.setLabelID(COL_ActivityInstanceId);
    activityInstanceIdElement.setIsVirtualViewElement(true);
    activityInstanceIdElement.setIsVirtual(true);
    activityInstanceIdElement.setMDataType(GspElementDataType.String);
    activityInstanceIdElement.setLength(36);
    childObj.getContainElements().add(activityInstanceIdElement);

    //审批活动
    GspViewModelElement activityNameElement = new GspViewModelElement();
    activityNameElement.setID(Guid.newGuid().toString());
    activityNameElement.setCode(COL_ActivityName);
    activityNameElement.setName(COL_ActivityName_Name);
    activityNameElement.setLabelID(COL_ActivityName);
    activityNameElement.setIsVirtualViewElement(true);
    activityNameElement.setIsVirtual(true);
    activityNameElement.setMDataType(GspElementDataType.Text);
    childObj.getContainElements().add(activityNameElement);

    //workItemIdElement
    GspViewModelElement workItemIdElement = new GspViewModelElement();
    workItemIdElement.setID(Guid.newGuid().toString());
    workItemIdElement.setCode(COL_WorkItemId);
    workItemIdElement.setName(COL_WorkItemId_Name);
    workItemIdElement.setLabelID(COL_WorkItemId);
    workItemIdElement.setIsVirtualViewElement(true);
    workItemIdElement.setIsVirtual(true);
    workItemIdElement.setMDataType(GspElementDataType.String);
    workItemIdElement.setLength(36);
    childObj.getContainElements().add(workItemIdElement);

    //operatorId
    GspViewModelElement operatorIdElement = new GspViewModelElement();
    operatorIdElement.setID(Guid.newGuid().toString());
    operatorIdElement.setCode(COL_OperatorId);
    operatorIdElement.setName(COL_OperatorId_Name);
    operatorIdElement.setLabelID(COL_OperatorId);
    operatorIdElement.setIsVirtualViewElement(true);
    operatorIdElement.setIsVirtual(true);
    operatorIdElement.setMDataType(GspElementDataType.String);
    operatorIdElement.setLength(36);
    childObj.getContainElements().add(operatorIdElement);

    //审批人
    GspViewModelElement operatorNameElement = new GspViewModelElement();
    operatorNameElement.setID(Guid.newGuid().toString());
    operatorNameElement.setCode(COL_OperatorName);
    operatorNameElement.setName(COL_OperatorName_Name);
    operatorNameElement.setLabelID(COL_OperatorName);
    operatorNameElement.setIsVirtualViewElement(true);
    operatorNameElement.setIsVirtual(true);
    operatorNameElement.setMDataType(GspElementDataType.Text);
    childObj.getContainElements().add(operatorNameElement);

    //type
    GspViewModelElement typeElement = new GspViewModelElement();
    typeElement.setID(Guid.newGuid().toString());
    typeElement.setCode(COL_Type);
    typeElement.setName(COL_Type_Name);
    typeElement.setLabelID(COL_Type);
    typeElement.setIsVirtualViewElement(true);
    typeElement.setIsVirtual(true);
    typeElement.setMDataType(GspElementDataType.String);
    typeElement.setLength(36);
    childObj.getContainElements().add(typeElement);

    //action
    GspViewModelElement actionElement = new GspViewModelElement();
    actionElement.setID(Guid.newGuid().toString());
    actionElement.setCode(COL_ActionCode);
    actionElement.setName(COL_ActionCode_Name);
    actionElement.setLabelID(COL_ActionCode);
    actionElement.setIsVirtualViewElement(true);
    actionElement.setIsVirtual(true);
    actionElement.setMDataType(GspElementDataType.String);
    actionElement.setLength(36);
    childObj.getContainElements().add(actionElement);

    //审批动作
    GspViewModelElement actionNameElement = new GspViewModelElement();
    actionNameElement.setID(Guid.newGuid().toString());
    actionNameElement.setCode(COL_ActionName);
    actionNameElement.setName(COL_ActionName_Name);
    actionNameElement.setLabelID(COL_ActionName);
    actionNameElement.setIsVirtualViewElement(true);
    actionNameElement.setIsVirtual(true);
    actionNameElement.setMDataType(GspElementDataType.Text);
    childObj.getContainElements().add(actionNameElement);

    //审批时间
    GspViewModelElement actionTimeElement = new GspViewModelElement();
    actionTimeElement.setID(Guid.newGuid().toString());
    actionTimeElement.setCode(COL_Time);
    actionTimeElement.setName(COL_Time_Name);
    actionTimeElement.setLabelID(COL_Time);
    actionTimeElement.setIsVirtualViewElement(true);
    actionTimeElement.setIsVirtual(true);
    actionTimeElement.setMDataType(GspElementDataType.DateTime);
    childObj.getContainElements().add(actionTimeElement);

    //审批信息
    GspViewModelElement messageElement = new GspViewModelElement();
    messageElement.setID(Guid.newGuid().toString());
    messageElement.setCode(COL_Message);
    messageElement.setName(COL_Message_Name);
    messageElement.setLabelID(COL_Message);
    messageElement.setIsVirtualViewElement(true);
    messageElement.setIsVirtual(true);
    messageElement.setMDataType(GspElementDataType.Text);
    childObj.getContainElements().add(messageElement);

    //附件信息
    GspViewModelElement attachmentElement = new GspViewModelElement();
    attachmentElement.setID(Guid.newGuid().toString());
    attachmentElement.setCode(COL_Attachment);
    attachmentElement.setName(COL_Attachment_Name);
    attachmentElement.setLabelID(COL_Attachment);
    attachmentElement.setIsVirtualViewElement(true);
    attachmentElement.setIsVirtual(true);
    attachmentElement.setMDataType(GspElementDataType.String);
    attachmentElement.setLength(36);
    childObj.getContainElements().add(attachmentElement);

    //签名图片信息
    GspViewModelElement signatureImageElement = new GspViewModelElement();
    signatureImageElement.setID(Guid.newGuid().toString());
    signatureImageElement.setCode(COL_SignatureImage);
    signatureImageElement.setName(COL_SignatureImage_Name);
    signatureImageElement.setLabelID(COL_SignatureImage);
    signatureImageElement.setIsVirtualViewElement(true);
    signatureImageElement.setIsVirtual(true);
    signatureImageElement.setMDataType(GspElementDataType.Text);
    childObj.getContainElements().add(signatureImageElement);

    parentObj.getContainChildObjects().add(childObj);
    return childObj;
  }

  private void addIdAndparentId(GspViewObject childObj, GspViewObject parentObj){
    GspViewModelElement idElement = new GspViewModelElement();
    idElement.setID(Guid.newGuid().toString());
    idElement.setCode(COL_ID);
    idElement.setName(COL_ID);
    idElement.setLabelID(COL_ID);
    idElement.setIsVirtualViewElement(true);
    idElement.setIsVirtual(true);
    idElement.setMDataType(GspElementDataType.String);
    idElement.setLength(36);
    childObj.getContainElements().add(idElement);
    GspColumnGenerate genId = new GspColumnGenerate();
    genId.setElementID(idElement.getID());
    childObj.setColumnGenerateID(genId);

    GspViewModelElement parentIdElement = new GspViewModelElement();
    parentIdElement.setID(Guid.newGuid().toString());
    parentIdElement.setCode(COL_ParentID);
    parentIdElement.setName(COL_ParentID);
    parentIdElement.setLabelID(COL_ParentID);
    parentIdElement.setIsVirtualViewElement(true);
    parentIdElement.setIsVirtual(true);
    parentIdElement.setMDataType(GspElementDataType.String);
    parentIdElement.setLength(36);
    childObj.getContainElements().add(parentIdElement);
    GspAssociationKey assoKey = new GspAssociationKey();
    assoKey.setSourceElement(parentIdElement.getID());
    assoKey.setSourceElementDisplay(parentIdElement.getName());
    assoKey.setTargetElement(parentObj.getIDElement().getID());
    assoKey.setTargetElementDisplay(parentObj.getIDElement().getName());
    childObj.getKeys().add(assoKey);
  }
  private GspViewObject addVirtualChildObject(GspViewModel model,GspViewObject parentObj){
    GspViewObject childObj = new GspViewObject();
    childObj.setID(Guid.newGuid().toString());
    String nodeCode = "ApprovalComment";
    int index = 0;
    while (model.findObjectByCode(nodeCode) != null) {
      nodeCode = "ApprovalComment" + (++index);
    }
    childObj.setCode(nodeCode);
    childObj.setName("审批日志" + (index == 0 ? "" : index));
    childObj.setIsVirtual(true);
    childObj.setParent(parentObj);
    TaskCommentService service=SpringBeanUtils.getBean(RpcClassHolder.class).getRpcClass(TaskCommentService.class);
    List<CommentFieldInfo> commentFieldInfos=service.getTaskCommentFields();
    if(commentFieldInfos==null || commentFieldInfos.size()<0)
      throw new RuntimeException("根据工作流接口获取的审批日志字段列表为空！");
    addIdAndparentId(childObj,parentObj);
    for (CommentFieldInfo commentFieldInfo : commentFieldInfos) {
      if(COL_ID.equals(commentFieldInfo.getVoCode()))
        continue;
      GspViewModelElement element=getElement(commentFieldInfo);
      childObj.getContainElements().add(element);
    }
    parentObj.getContainChildObjects().add(childObj);
    return childObj;
  }
  private GspViewModelElement getElement(CommentFieldInfo info){
    GspViewModelElement viewModelElement=new GspViewModelElement();
    viewModelElement.setID(UUID.randomUUID().toString());
    viewModelElement.setCode(info.getVoCode());
    viewModelElement.setName(info.getName());
    viewModelElement.setLabelID(info.getVoCode());
    viewModelElement.setIsVirtualViewElement(true);
    viewModelElement.setIsVirtual(true);
    viewModelElement.setMDataType(converFromCommoFieldType(info));
    if(dealLength(info))
      viewModelElement.setLength(info.getLength());
    return  viewModelElement;
  }

  private boolean dealLength(CommentFieldInfo info) {
    if(info.getLength()>0)
      return true;
    else
      return false;
  }

  private void addAfterRetrieveAction(GspViewModel model,GspViewObject childObj,String approvalCommentLabel,String componentID,String approvalLabelId,String approvalChildCode,boolean includeBacklogs){
    MappedCdpAction action = new MappedCdpAction();
    action.setID(Guid.newGuid().toString());
    String code = "InitApprovalComments";
    while (true) {
      final String tempCode = code;
      if (model.getDataExtendInfo().getAfterRetrieveActions().stream()
              .anyMatch(item -> item.getCode().equalsIgnoreCase(tempCode))) {
        code = "InitApprovalComments" + Guid.newGuid().toString().substring(0, 7);
        continue;
      }
      break;
    }
    action.setCode(code);
    action.setType(ViewModelActionType.VMAction);
    action.setName(code);
    action.setComponentEntityId(componentID);

    GspMetadata retrieveActionMetadata = MetadataUtil.getCustomMetadata(componentID);
    VMComponent retrieveComponent = (VMComponent) retrieveActionMetadata.getContent();

    VMMethodParameter childCodePar = null;
    VMMethodParameter approvalLabelIdPar = null;
    VMMethodParameter getPayLoadPar = null;

    for (VMMethodParameter param : retrieveComponent.getVmMethod().getParams()) {
      if (param.getParamCode().equalsIgnoreCase(approvalChildCode)) {
        childCodePar = param;
      } else if (param.getParamCode().equalsIgnoreCase(approvalLabelId)) {
        approvalLabelIdPar = param;
      } else if (param.getParamCode().equalsIgnoreCase(RETRIEVE_ACTION_APPROVALGETLOGS_PAYLOAD)) {
        getPayLoadPar = param;
      }
    }
    MappedCdpActionParameter parChildCode = new MappedCdpActionParameter();
    parChildCode.setID(childCodePar.getID());
    parChildCode.setParamName(childCodePar.getParamName());
    parChildCode.setParamCode(childCodePar.getParamCode());
    parChildCode.setParameterType(VMParameterType.String);
    parChildCode.setCollectionParameterType(VMCollectionParameterType.List);
    List<String> codes = new ArrayList<>();
    IGspCommonObject currentObj = childObj;
    while (currentObj.getParentObject() != null) {
      codes.add(currentObj.getCode());
      currentObj = currentObj.getParentObject();
    }
    Collections.reverse(codes);
    parChildCode.getActualValue().setHasValue(true);
    parChildCode.getActualValue().setEnable(true);
    parChildCode.getActualValue().setValue(JSONSerializer.serialize(codes));
    action.getParameterCollection().add(parChildCode);

    MappedCdpActionParameter parProcessInstLabelId = new MappedCdpActionParameter();
    parProcessInstLabelId.setID(approvalLabelIdPar.getID());
    parProcessInstLabelId.setParamCode(approvalLabelIdPar.getParamName());
    parProcessInstLabelId.setParamCode(approvalLabelIdPar.getParamCode());
    parProcessInstLabelId.setParameterType(VMParameterType.String);
    parProcessInstLabelId.getActualValue().setHasValue(true);
    parProcessInstLabelId.getActualValue().setEnable(true);
    parProcessInstLabelId.getActualValue().setValue(approvalCommentLabel);
    action.getParameterCollection().add(parProcessInstLabelId);

    MappedCdpActionParameter parPropertyMapping = new MappedCdpActionParameter();
    parPropertyMapping.setID(getPayLoadPar.getID());
    parPropertyMapping.setParamCode(getPayLoadPar.getParamName());
    parPropertyMapping.setParamCode(getPayLoadPar.getParamCode());
    parPropertyMapping.setParameterType(VMParameterType.Custom);
    parPropertyMapping.setAssembly("com.inspur.edp.task.payload");
    parPropertyMapping.setClassName(
            "com.inspur.edp.task.payload.GetTaskLogsPayload");
    parPropertyMapping.getActualValue().setHasValue(true);
    parPropertyMapping.getActualValue().setEnable(true);
    GetTaskLogsPayload taskLogsPayload=new GetTaskLogsPayload();
    taskLogsPayload.setIsIncludeBackLogs(includeBacklogs);
    taskLogsPayload.setIsIncludeSignImageBase64(true);
    parPropertyMapping.getActualValue().setValue(JSONSerializer.serialize(taskLogsPayload));
    action.getParameterCollection().add(parPropertyMapping);

    model.getDataExtendInfo().getAfterRetrieveActions().add(action);
  }

  private GspElementDataType converFromCommoFieldType(CommentFieldInfo info){
    switch (info.getFieldType()){
      case String:
        return GspElementDataType.String;
      case Text:
        return GspElementDataType.Text;
      case Boolean:
        return GspElementDataType.Boolean;
      case DateTime:
        return GspElementDataType.DateTime;
      case Date:
        return GspElementDataType.Date;
      default:
        throw new RuntimeException(String.format("目前暂不支持审批日志CommentFieldType类型为：%$s1的字段，当前字段编号为：%$s2,当前字段名称为：%$s2",info.getFieldType(),info.getVoCode(),info.getName()));
    }
  }
  private void addDataExtendInfo(GspViewModel model, GspViewObject childObj,
      String processInstLabelId,String componentID,String childNodeCode,String approvalFieldLabelID ,boolean includeBacklogs) {
    MappedCdpAction action = new MappedCdpAction();
    action.setID(Guid.newGuid().toString());
    String code = "InitApprovalComments";
    while (true) {
      final String tempCode = code;
      if (model.getDataExtendInfo().getAfterRetrieveActions().stream()
          .anyMatch(item -> item.getCode().equalsIgnoreCase(tempCode))) {
        code = "InitApprovalComments" + Guid.newGuid().toString().substring(0, 7);
        continue;
      }
      break;
    }
    action.setCode(code);
    action.setType(ViewModelActionType.VMAction);
    action.setName(code);
    action.setComponentEntityId(componentID);

    GspMetadata retrieveActionMetadata = MetadataUtil.getCustomMetadata(componentID);
    VMComponent retrieveComponent = (VMComponent) retrieveActionMetadata.getContent();

    VMMethodParameter childCodePar = null;
    VMMethodParameter processInstLabelIdPar = null;
    VMMethodParameter propertyMappingPar = null;

    for (VMMethodParameter param : retrieveComponent.getVmMethod().getParams()) {
      if (param.getParamCode().equalsIgnoreCase(childNodeCode)) {
        childCodePar = param;
      } else if (param.getParamCode().equalsIgnoreCase(approvalFieldLabelID)) {
        processInstLabelIdPar = param;
      } else if (param.getParamCode().equalsIgnoreCase(RETRIEVE_ACTION_PAR_PROPERTY_MAPPING)) {
        propertyMappingPar = param;
      }
    }
    MappedCdpActionParameter parChildCode = new MappedCdpActionParameter();
    parChildCode.setID(childCodePar.getID());
    parChildCode.setParamName(childCodePar.getParamName());
    parChildCode.setParamCode(childCodePar.getParamCode());
    parChildCode.setParameterType(VMParameterType.String);
    parChildCode.setCollectionParameterType(VMCollectionParameterType.List);
    ArrayList<String> codes = new ArrayList<>();
    IGspCommonObject currentObj = childObj;
    while (currentObj.getParentObject() != null) {
      codes.add(currentObj.getCode());
      currentObj = currentObj.getParentObject();
    }
    Collections.reverse(codes);
    parChildCode.getActualValue().setHasValue(true);
    parChildCode.getActualValue().setEnable(true);
    parChildCode.getActualValue().setValue(JSONSerializer.serialize(codes));
    action.getParameterCollection().add(parChildCode);

    MappedCdpActionParameter parProcessInstLabelId = new MappedCdpActionParameter();
    parProcessInstLabelId.setID(processInstLabelIdPar.getID());
    parProcessInstLabelId.setParamCode(processInstLabelIdPar.getParamName());
    parProcessInstLabelId.setParamCode(processInstLabelIdPar.getParamCode());
    parProcessInstLabelId.setParameterType(VMParameterType.String);
    parProcessInstLabelId.getActualValue().setHasValue(true);
    parProcessInstLabelId.getActualValue().setEnable(true);
    parProcessInstLabelId.getActualValue().setValue(processInstLabelId);
    action.getParameterCollection().add(parProcessInstLabelId);

    MappedCdpActionParameter parPropertyMapping = new MappedCdpActionParameter();
    parPropertyMapping.setID(propertyMappingPar.getID());
    parPropertyMapping.setParamCode(propertyMappingPar.getParamName());
    parPropertyMapping.setParamCode(propertyMappingPar.getParamCode());
    parPropertyMapping.setParameterType(VMParameterType.Custom);
    parPropertyMapping.setAssembly("com.inspur.edp.common.component.api");
    parPropertyMapping.setClassName(
        "com.inspur.common.component.api.service.workflow.ApprovalCommentsPropMapping");
    parPropertyMapping.getActualValue().setHasValue(true);
    parPropertyMapping.getActualValue().setEnable(true);
    ApprovalCommentsPropMapping mapping = new ApprovalCommentsPropMapping();
    mapping.setProcessInstanceId(COL_ProcessInstanceId);
    mapping.setRootProcessInstanceId(COL_RootProcessInstanceId);
    mapping.setActivityInstanceId(COL_ActivityInstanceId);
    mapping.setActivityName(COL_ActivityName);
    mapping.setWorkItemId(COL_WorkItemId);
    mapping.setOperatorId(COL_OperatorId);
    mapping.setOperatorName(COL_OperatorName);
    mapping.setType(COL_Type);
    mapping.setActionCode(COL_ActionCode);
    mapping.setActionName(COL_ActionName);
    mapping.setMessage(COL_Message);
    mapping.setTime(COL_Time);
    mapping.setAttachment(COL_Attachment);
    mapping.setSignatureImage(COL_SignatureImage);
    mapping.setIncludeBackLogs(includeBacklogs);
    parPropertyMapping.getActualValue().setValue(JSONSerializer.serialize(mapping));
    action.getParameterCollection().add(parPropertyMapping);

    model.getDataExtendInfo().getAfterRetrieveActions().add(action);
  }
}
