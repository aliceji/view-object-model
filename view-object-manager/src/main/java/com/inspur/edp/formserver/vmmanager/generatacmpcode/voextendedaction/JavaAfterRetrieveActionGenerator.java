/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction;

import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.JavaCompCodeNames;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.JavaMappedCdpActionGenerator;

public class JavaAfterRetrieveActionGenerator extends JavaMappedCdpActionGenerator {

  private MappedCdpAction action;
  
  //private String allInterfaceName;
  //private String beMgrInterfaceName;
  protected  String getBaseClassName(){
  {

      if(getBelongElement())
        return "AfterRetrieveAction";
      else
        //return "AfterQueryAction<" + ReturnTypeName + ">";
        return "AfterRetrieveAction";
    }
  }

  protected  void generateExtendUsing(StringBuilder result)
  {
    result.append(getUsingStr(JavaCompCodeNames.RetrieveContextNameSpace));
    result.append(getUsingStr(JavaCompCodeNames.AfterRetrieveActionNameSpace));

  }

  public JavaAfterRetrieveActionGenerator(GspViewModel vm, MappedCdpAction vmAction, String nameSpace, String path)
  {
    super(vm, vmAction, nameSpace, path);
    this.action = vmAction;

  }
  public  String getNameSpaceSuffix(){
    
    return JavaCompCodeNames.VOActionNameSpaceSuffix;
  }

  public  void generateConstructor(StringBuilder result)
  {
    result.append(getIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ")
        .append(getCompName()).append("(").append(JavaCompCodeNames.KeyWordRetrieveContext).append(" ").append(JavaCompCodeNames.Keywordcontext);

    //result.append(getDoubleIndentationStr()).append(getIndentationStr()).append("IBEManagerContext managerContext");

    if (hasCustomConstructorParams())
    {
      result.append(getNewLine());
      generateConstructorParams(result);
      result.append(getDoubleIndentationStr()).append(getIndentationStr()).append("{").append(")").append(getNewLine());
    }
    else
    {
      result.append(")").append("{").append(getNewLine());
    }

    result.append(getDoubleIndentationStr()).append(JavaCompCodeNames.KeywordSuper).append("(").append(JavaCompCodeNames.Keywordcontext).append(")").append(";").append(getNewLine());

    //result.append(getDoubleIndentationStr()).append(getIndentationStr()).append(getNewLine()).append(") : base(managerContext)");

    //result.append(getDoubleIndentationStr()).append(getNewLine()).append("{");
    generateConstructorContent(result);
    result.append(getIndentationStr()).append("}");
    result.append(getNewLine());

    // GenerateExecute(result);

  }
        
  public   String getInitializeCompName()
  {
    return String.format("%1$s%2$s",VMAction.getCode(), "VO");
  }
}
