/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction;


import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.JavaCompCodeNames;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.JavaMappedCdpActionGenerator;

public class JavaBeforeRetrieveActionsGenerator extends JavaMappedCdpActionGenerator
{
	private MappedCdpAction action;

		///#region 属性
	//private string allInterfaceName;
	//private string beMgrInterfaceName;
	@Override
	protected String getBaseClassName()
	{
		if(getBelongElement())
		{
			return "BeforeRetrieveAction";
		}
		else
				//return "AfterQueryAction<" + ReturnTypeName + ">";
		{
			return "BeforeRetrieveAction";
		}
	}


		///#endregion

		///#region import
	@Override
	protected void generateExtendUsing(StringBuilder result)
	{
		result.append(getUsingStr(JavaCompCodeNames.RetrieveContextNameSpace));
		result.append(getUsingStr(JavaCompCodeNames.BeforeRetrieveActionNameSpace));
		//if (BaseClassName.Contains("Date"))
		//{
		//    result.append(getUsingStrNoStar(JavaCompCodeNames.DateNameSpace));
		//}
		//if (BaseClassName.Contains("BigDecimal"))
		//{
		//    result.append(getUsingStrNoStar(JavaCompCodeNames.BigDecimalNameSpace));
		//}

		//参数列表
		//if (usingList == null || usingList.Count < 1)
		//    return;

		//foreach (var usingName in usingList)
		//{
		//    result.append(getUsingStr(usingName));
		//}
	}

		///#endregion


		///#region 构造函数
	public JavaBeforeRetrieveActionsGenerator(GspViewModel vm, MappedCdpAction vmAction, String nameSpace, String path)
	{
		super(vm, vmAction, nameSpace, path);
		this.action = vmAction;
		//allInterfaceName = ApiHelper.GetBEAllInterfaceClassName(vm);
		//beMgrInterfaceName = ApiHelper.GetBEMgrInterfaceClassName(vm);
	}
	@Override
	public  String getNameSpaceSuffix()
	{
		return JavaCompCodeNames.VOActionNameSpaceSuffix;
	}


		///#endregion


		///#region generateConstructor

	@Override
	public void generateConstructor(StringBuilder result)
	{
		result.append(getIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ").append(getCompName()).append("(").append(JavaCompCodeNames.KeyWordRetrieveContext).append(" ").append(JavaCompCodeNames.Keywordcontext);

		//result.append(getDoubleIndentationStr()).append(getIndentationStr()).append("IBEManagerContext managerContext");

		if (hasCustomConstructorParams())
		{
			result.append("\n");
			generateConstructorParams(result);
			result.append(getDoubleIndentationStr()).append(getIndentationStr()).append("{").append(")").append(getNewLine());
		}
		else
		{
			result.append(")").append("{").append(getNewLine());
		}

		result.append(getDoubleIndentationStr()).append(JavaCompCodeNames.KeywordSuper).append("(").append(JavaCompCodeNames.Keywordcontext).append(")").append(";").append(getNewLine());

		//result.append(getDoubleIndentationStr()).append(getIndentationStr()).append(") : base(managerContext)");

		//result.append(getDoubleIndentationStr()).append("{");
		generateConstructorContent(result);
		result.append(getIndentationStr()).append("}");
		result.append("\n");

	   // GenerateExecute(result);

	}

		///#endregion

	///#region GenerateExtendMethod
	//protected override void GenerateExtendMethod(StringBuilder result)
	//{
		//GenerateGetEntityMethod(result);
		//GenerateGetMgrMethod(result);
	//}

	//{
	//	if (param.CollectionParameterType == VMCollectionParameterType.List)
	//		AddUsing("System.Collections.Generic");
	//          if (param.ParameterType == VMParameterType.DateTime)
	//              AddUsing("System");
	//          if (param.ParameterType == VMParameterType.Custom)
	//          {

	//              if (!IsGeneric(param))
	//              {
	//                  var usingName = param.ClassName.Substring(0, param.ClassName.LastIndexOf(".", StringComparison.Ordinal));
	//                  AddUsing(usingName);
	//              }
	//              else
	//              {
	//                  var psn = Assembly.Load(param.Assembly);
	//                  Type type = psn.GetType(param.ClassName);
	//                  GetAllGenericParameterType(type);
	//                  foreach (Type s in allGenericParamType)
	//                  {
	//                      AddUsing(s.ToString().Split('`')[0].Substring(0, s.ToString().Split('`')[0].LastIndexOf(".", StringComparison.Ordinal)));
	//                  }
	//              }
	//          }
	//      }

	//private void GenerateGetEntityMethod(StringBuilder result)
	//{
	//    result.append(getDoubleIndentationStr()).append(CompCodeNames.KeywordPrivate).append(" ")
	//        .append(allInterfaceName).append(" GetEntity(string dataId) ");
	//    result.append(getDoubleIndentationStr()).append("{");

	//    result.append(getDoubleIndentationStr()).append(getIndentationStr())
	//        .append("return base.BEManagerContext.GetEntity(dataId) as ").append(allInterfaceName).append(" ;");

	//    result.append(getDoubleIndentationStr()).append("}");
	//}

	//private void GenerateGetMgrMethod(StringBuilder result)
	//{
	//    result.append(getDoubleIndentationStr()).append(CompCodeNames.KeywordPrivate).append(" ")
	//        .append(beMgrInterfaceName).append(" GetMgr() ");
	//    result.append(getDoubleIndentationStr()).append("{");

	//    result.append(getDoubleIndentationStr()).append(getIndentationStr())
	//        .append("return base.BEManagerContext.BEManager as ").append(beMgrInterfaceName).append(" ;");

	//    result.append(getDoubleIndentationStr()).append("}");
	//}
	///#endregion

	@Override
	public String getInitializeCompName()
	{
		return String.format("%1$s%2$s",VMAction.getCode(), "VO");
	}
}
