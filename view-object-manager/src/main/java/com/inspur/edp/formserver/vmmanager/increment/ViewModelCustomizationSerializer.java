/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.increment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.formserver.viewmodel.increment.ViewModelIncrement;
import com.inspur.edp.metadata.rtcustomization.api.ICustomizedContent;
import com.inspur.edp.metadata.rtcustomization.spi.CustomizationSerializer;

public class ViewModelCustomizationSerializer implements CustomizationSerializer {

    private ObjectMapper mapper;
    private ObjectMapper getMapper(){
        if(mapper == null){
            mapper = new ObjectMapper();
//            SimpleModule module = new SimpleModule();
//            module.addSerializer(ViewModelIncrement.class, new ViewModelIncrementSerializer());
//            module.addDeserializer(ViewModelIncrement.class, new ViewModelIncrementDeserializer());
//            mapper.registerModule(module);
        }
        return mapper;
    }

    @Override
    public String serialize(ICustomizedContent increament) {
        try {
            return getMapper().writeValueAsString(increament);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("vo元数据增量序列化失败",e);
        }
    }

    @Override
    public ICustomizedContent deSerialize(String increamentStr) {
        try {
            return getMapper().readValue(increamentStr,ViewModelIncrement.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("vo元数据增量反序列化失败",e);
        }
    }
}
