/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.increment;

import com.inspur.edp.metadata.rtcustomization.spi.CustomizationExtChecker;
import com.inspur.edp.metadata.rtcustomization.spi.args.ConflictCheckArgs;
import com.inspur.edp.metadata.rtcustomization.spi.args.MdConflictCheckArgs;

public class ViewModelExtChecker implements CustomizationExtChecker {

    @Override
    public void checkMergeConflict(ConflictCheckArgs conflictCheckArgs) {

    }

    @Override
    public void checkMergeConflict(MdConflictCheckArgs mdConflictCheckArgs) {

    }
}
