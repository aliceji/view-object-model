/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager;

import com.inspur.edp.bef.bizentity.pushchangesetargs.MetadataInfo;
import com.inspur.edp.bef.bizentity.pushchangesetargs.PushChangeSet;
import com.inspur.edp.bef.bizentity.pushchangesetargs.PushChangeSetArgs;
import com.inspur.edp.bef.bizentity.pushchangesetargs.RelatedMetadata;
import com.inspur.edp.bef.bizentity.pushchangesetlistener.IPushChangeSetListener;
import com.inspur.edp.formserver.viewmodel.pushchangesetargs.VmPushChangeSet;
import com.inspur.edp.formserver.viewmodel.pushchangesetargs.VmPushChangeSetArgs;
import com.inspur.edp.formserver.vmmanager.pushchangesetevent.PushVmChangeSetEventBroker;
import com.inspur.edp.formserver.vmmanager.util.PushChangeSets;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PushChangeSetListener implements IPushChangeSetListener {

  @Override
  public void bePushChangeSet(PushChangeSetArgs args) {
    PushChangeSet changeSet = args.getPushChangeSet();
    List<GspMetadata> metadataList = getRelatedMetadataList(changeSet.getMetadataInfo(),
        changeSet.getRelatedMetadatas());
    if (metadataList == null && metadataList.isEmpty()) {
      return;
    }
    metadataList.forEach(metadata -> {
      new PushChangeSets(metadata, changeSet).pushChangeSetToMetadata();
    });
    pushVmChangeSet(metadataList);
  }

  /**
   * 过滤需要推送的元数据
   *
   * @param metadataInfo
   * @param relatedMetadataInfo
   * @return
   */
  private List<GspMetadata> getRelatedMetadataList(MetadataInfo metadataInfo,
      List<RelatedMetadata> relatedMetadataInfo) {
    if (metadataInfo == null | relatedMetadataInfo == null) {
      return null;
    }
    ;
    MetadataService metadataService = SpringBeanUtils
        .getBean(com.inspur.edp.lcm.metadata.api.service.MetadataService.class);
    List<GspMetadata> gspMetadataList = metadataService
        .getMetadataListByRefedMetadataId(metadataInfo.getPath(), metadataInfo.getEntityId());
    List<GspMetadata> selectMetadata = gspMetadataList.stream().filter(item ->
        relatedMetadataInfo.stream().anyMatch(
            metadata -> metadata.getContentCode().equals(item.getHeader().getCode()) && metadata
                .getNameSpace().equals(item.getHeader().getNameSpace()))
    ).collect(Collectors.toList());
    return getMetadataListWithContent(selectMetadata, metadataService);
  }


  private List<GspMetadata> getMetadataListWithContent(List<GspMetadata> metadataList,
      MetadataService service) {
    List<GspMetadata> newMetadataList = new ArrayList<>(metadataList.size());
    metadataList.forEach(metadata -> {
      newMetadataList
          .add(service.getRefMetadata(metadata.getRelativePath(), metadata.getHeader().getId()));
    });
    return newMetadataList;
  }

  private void pushVmChangeSet(List<GspMetadata> metadataList) {
    PushVmChangeSetEventBroker pushEventBroker = SpringBeanUtils
        .getBean(PushVmChangeSetEventBroker.class);
    VmPushChangeSet vmPushChangeSet = new VmPushChangeSet(metadataList);
    pushEventBroker.firePushChangeSet(new VmPushChangeSetArgs(vmPushChangeSet));
  }


}
