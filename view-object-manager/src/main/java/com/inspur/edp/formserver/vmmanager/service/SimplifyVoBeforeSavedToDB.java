/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.service;

/*import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.common.Utils;
import com.inspur.edp.metadata.rtcustomization.spi.MetadataBeforeSaveToDBExtend;
import com.inspur.edp.formserver.vmmanager.ContentSerializer;

public class SimplifyVoBeforeSavedToDB implements MetadataBeforeSaveToDBExtend {

    public String execute(GspMetadata gspMetadata){
        ContentSerializer serializer = new ContentSerializer();
        JsonNode jsonNode = serializer.Serialize(gspMetadata.getContent());
        ObjectMapper mapper = Utils.getMapper();
        String mdValue;
        try {
            mdValue = mapper.writeValueAsString(jsonNode);
        }catch (JsonProcessingException e){
            throw new RuntimeException("序列化失败，元数据id："+gspMetadata.getHeader().getId());
        }
        return mdValue;
    }

}*/
