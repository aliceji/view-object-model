/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.extendinfo.repository;

import com.inspur.edp.formserver.viewmodel.extendinfo.entity.GspVoExtendInfo;
import io.iec.edp.caf.data.orm.DataRepository;

import java.util.List;

public interface GspVoExtendInfoRepository extends DataRepository<GspVoExtendInfo, String> {

    /**
     * 根据configId获取某条BE扩展信息
     * @param configId
     * @return
     */
    GspVoExtendInfo getVoExtendInfoByConfigId(String configId);

    List<GspVoExtendInfo> getVoExtendInfosByConfigId(String configId);

    List<GspVoExtendInfo> getVoExtendInfoByBeSourceId(String beId);

}

