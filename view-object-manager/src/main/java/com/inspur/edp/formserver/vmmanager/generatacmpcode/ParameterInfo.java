/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.generatacmpcode;

public class ParameterInfo
{
	private String privateParamName;
	public final String getParamName()
	{
		return privateParamName;
	}
	public final void setParamName(String value)
	{
		privateParamName = value;
	}
	private String privateParamType;
	public final String getParamType()
	{
		return privateParamType;
	}
	public final void setParamType(String value)
	{
		privateParamType = value;
	}
	private String privateParameterMode;
	public final String getParameterMode()
	{
		return privateParameterMode;
	}
	public final void setParameterMode(String value)
	{
		privateParameterMode = value;
	}
}
