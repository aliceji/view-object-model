/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.generatacmpcode;


import com.inspur.edp.cef.designtime.api.collection.CommonDtmCollection;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.variable.dtgenerator.vardtmGenerate.javageneratecmpcode.JavaCommonDeterminationGenerator;
import com.inspur.edp.cef.variable.dtgenerator.vardtmGenerate.javageneratecmpcode.JavaIBaseCompCodeGen;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelActionType;
import com.inspur.edp.formserver.viewmodel.common.InternalExtendActionUtil;
import com.inspur.edp.formserver.viewmodel.common.ValueHelpConfig;
import com.inspur.edp.formserver.viewmodel.dataextendinfo.VoDataExtendInfo;
import com.inspur.edp.formserver.viewmodel.util.ViewModelUtils;
import com.inspur.edp.formserver.vmmanager.createvmmetadata.ComponentGenUtil;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.*;
import com.inspur.edp.formserver.vmmanager.service.VmManagerService;
import com.inspur.edp.lcm.fs.api.IFsService;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.GspProject;
import com.inspur.edp.lcm.metadata.api.service.FileService;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.ArrayList;

public class JavaCodeFileGenerator {

  private String relativePath;
  private GspViewModel viewModel;
  private String compAssemblyName;
  private FileService fsService;
  IFsService iFsService;
  private static final String codeFileExtension = ".java";


  public JavaCodeFileGenerator(GspMetadata metadata) {
    relativePath = metadata.getRelativePath();

    GspProject metadataProj = SpringBeanUtils.getBean(MetadataService.class)
        .getGspProjectInfo(relativePath);
    viewModel = (GspViewModel) metadata.getContent();
    compAssemblyName = metadataProj.getProjectNameSpace();
    fsService = SpringBeanUtils.getBean(FileService.class);
    iFsService = SpringBeanUtils.getBean(IFsService.class);
  }

  public final void generate(ArrayList actionList) {
    generateVMActions(actionList);
    generateVariableDtms(actionList);
  }


  private boolean isGenCompCode(ArrayList<String> actionList, String actionCode) {
    return actionList.contains(actionCode);
  }

  private void generateVMActions(ArrayList actionList) {
    //var path = prepareDir(viewModel.Code, voFileName);
    String path = null;

    for (ViewModelAction action : viewModel.getActions()) {
      if (action.getType() == ViewModelActionType.VMAction && isGenCompCode(actionList,
          action.getCode())) {
        if (path == null) {
          path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
        }
        VMCodeGenerate(viewModel, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), path, false);
      }
    }

    for (ValueHelpConfig valueHelpConfig : viewModel.getValueHelpConfigs()) {
      if (valueHelpConfig.getHelpExtend() != null && valueHelpConfig.getHelpExtend().getBeforeHelp() != null) {

        for (ViewModelAction action : valueHelpConfig.getHelpExtend().getBeforeHelp()) {
          if (isGenCompCode(actionList, action.getCode())) {
            if (path == null) {
              path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
            }
            VMCodeGenerate(viewModel, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), path, true);
          }

        }
      }
    }
    //扩展操作
    VoDataExtendInfo extendInfo = viewModel.getDataExtendInfo();
    //创建BeforeCreateActions

    for (ViewModelAction action : extendInfo.getBeforeCreateActions()) {
      if (isGenCompCode(actionList, action.getCode())) {
        JavaBeforeRetrieveDefaultActionGenerator gen = new JavaBeforeRetrieveDefaultActionGenerator(
            viewModel, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
            compAssemblyName, relativePath);
        gen.setBelongElement(false);
        if (path == null) {
          path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
        }
        extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
      }

    }
    //创建AfterCreateActions

    for (ViewModelAction action : extendInfo.getAfterCreateActions()) {
      if (isGenCompCode(actionList, action.getCode())) {
        JavaAfterRetrieveDefaultActionGenerator gen = new JavaAfterRetrieveDefaultActionGenerator(
            viewModel, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
            compAssemblyName, relativePath);
        gen.setBelongElement(false);
        if (path == null) {
          path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
        }
        extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
      }

    }
    //创建CreateActions

    for (ViewModelAction action : extendInfo.getCreateActions()) {

      //if (!action.ID.Equals("52479451-8e22-4751-8684-80489ce5786b"))
      if (!action.getID().equals(InternalExtendActionUtil.CreateActionId)) {
        if(isGenCompCode(actionList,action.getCode())){
          JavaAbstractRetrieveDefaultActionGenerator gen = new JavaAbstractRetrieveDefaultActionGenerator(
              viewModel, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
              compAssemblyName, relativePath);
          gen.setBelongElement(false);
          if (path == null) {
            path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
          }
          extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
        }
      }
    }
    //创建BeforeModifyActions

    for (ViewModelAction action : extendInfo.getBeforeModifyActions()) {
      if(isGenCompCode(actionList,action.getCode())){
        JavaBeforeModifyActionGenerator gen = new JavaBeforeModifyActionGenerator(viewModel,
            (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
            relativePath);
        gen.setBelongElement(false);
        if (path == null) {
          path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
        }
        extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
      }

    }
    //创建AfterModifyActions

    for (ViewModelAction action : extendInfo.getAfterModifyActions()) {
      if(isGenCompCode(actionList,action.getCode())){
        JavaAfterModifyActionGenerator gen = new JavaAfterModifyActionGenerator(viewModel,
            (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
            relativePath);
        gen.setBelongElement(false);
        if (path == null) {
          path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
        }
        extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
      }

    }
    //创建ModifyActions

    for (ViewModelAction action : extendInfo.getModifyActions()) {
      //if (!action.ID.Equals("47dd3752-72a3-4c56-81c0-ae8ccfe5eb98"))
      if (!action.getID().equals(InternalExtendActionUtil.ModifyActionId)) {
        if(isGenCompCode(actionList,action.getCode())){
          JavaAbstractModifyActionGenerator gen = new JavaAbstractModifyActionGenerator(viewModel,
              (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
              compAssemblyName, relativePath);
          gen.setBelongElement(false);
          if (path == null) {
            path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
          }
          extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
        }

      }
    }
    //创建ChangesetMappingActions

    for (ViewModelAction action : extendInfo.getChangesetMappingActions()) {
      //if (!action.ID.Equals("5798f884-c222-47f4-8bbe-685c7013dee4"))
      if (!action.getID().equals(InternalExtendActionUtil.ChangesetMappingActionId)) {
        if(isGenCompCode(actionList,action.getCode())){
          JavaChangeMappingActionGenerator gen = new JavaChangeMappingActionGenerator(viewModel,
              (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
              compAssemblyName, relativePath);
          gen.setBelongElement(false);
          if (path == null) {
            path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
          }
          extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
        }

      }
    }
    //创建ChangesetReversalMappingActions

    for (ViewModelAction action : extendInfo.getChangesetReversalMappingActions()) {
      if (!action.getID().equals(InternalExtendActionUtil.ChangesetReversalMappingActionId)) {
        if(isGenCompCode(actionList,action.getCode())){
          JavaChangeReversalMappingActionGenerator gen = new JavaChangeReversalMappingActionGenerator(
              viewModel, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
              compAssemblyName, relativePath);
          gen.setBelongElement(false);
          if (path == null) {
            path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
          }
          extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
        }

      }
    }
    //创建AfterQueryActions

    for (ViewModelAction action : extendInfo.getAfterQueryActions()) {
      if(isGenCompCode(actionList,action.getCode())){
        JavaAfterQueryActionGenerator gen = new JavaAfterQueryActionGenerator(viewModel,
            (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
            relativePath);
        gen.setBelongElement(false);
        if (path == null) {
          path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
        }
        extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
      }

    }
    //创建BeforeQueryActions

    for (ViewModelAction action : extendInfo.getBeforeQueryActions()) {
      if(isGenCompCode(actionList,action.getCode())){
        JavaBeforeQueryActionGenerator gen = new JavaBeforeQueryActionGenerator(viewModel,
            (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
            relativePath);
        gen.setBelongElement(false);
        if (path == null) {
          path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
        }
        extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
      }

    }

    for (ViewModelAction action : extendInfo.getQueryActions()) {

      if (!action.getID().equals(InternalExtendActionUtil.QueryActionId)) {
        if(isGenCompCode(actionList,action.getCode())){
          JavaQueryActionGenerator gen = new JavaQueryActionGenerator(viewModel,
              (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
              compAssemblyName, relativePath);
          gen.setBelongElement(false);
          if (path == null) {
            path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
          }
          extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
        }

      }
    }

    for (ViewModelAction action : extendInfo.getDataMappingActions()) {
      if (!action.getID().equals(InternalExtendActionUtil.DataMappingActionId)) {
        if(isGenCompCode(actionList,action.getCode())){
          JavaDataMappingActionsGenerator gen = new JavaDataMappingActionsGenerator(viewModel,
              (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
              compAssemblyName, relativePath);
          gen.setBelongElement(false);
          if (path == null) {
            path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
          }
          extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
        }

      }
    }

    for (ViewModelAction action : extendInfo.getDataReversalMappingActions()) {
      if (!action.getID().equals(InternalExtendActionUtil.DataReversalMappingActionId)) {
        if(isGenCompCode(actionList,action.getCode())){
          JavaDataReversalMappingActionsGenerator gen = new JavaDataReversalMappingActionsGenerator(
              viewModel, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
              compAssemblyName, relativePath);
          gen.setBelongElement(false);
          if (path == null) {
            path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
          }
          extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
        }

      }
    }

    for (ViewModelAction action : extendInfo.getBeforeRetrieveActions()) {
      if(isGenCompCode(actionList,action.getCode())){
        JavaBeforeRetrieveActionsGenerator gen = new JavaBeforeRetrieveActionsGenerator(viewModel,
            (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
            relativePath);
        gen.setBelongElement(false);
        if (path == null) {
          path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
        }
        extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
      }

    }

    for (ViewModelAction action : extendInfo.getAfterRetrieveActions()) {
      if(isGenCompCode(actionList,action.getCode())){
        JavaAfterRetrieveActionGenerator gen = new JavaAfterRetrieveActionGenerator(viewModel,
            (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
            relativePath);
        gen.setBelongElement(false);
        if (path == null) {
          path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
        }
        extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
      }

    }

    for (ViewModelAction action : extendInfo.getRetrieveActions()) {
      if (!action.getID().equals(InternalExtendActionUtil.RetrieveActionId)) {
        if(isGenCompCode(actionList,action.getCode())){
          JavaRetrieveActionGenerator gen = new JavaRetrieveActionGenerator(viewModel,
              (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
              compAssemblyName, relativePath);
          gen.setBelongElement(false);
          if (path == null) {
            path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
          }
          extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
        }

      }
    }

    for (ViewModelAction action : extendInfo.getBeforeMultiDeleteActions()) {
      if(isGenCompCode(actionList,action.getCode())){
        JavaBeforeMultiDeleteActionGenerator gen = new JavaBeforeMultiDeleteActionGenerator(viewModel,
            (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
            relativePath);
        gen.setBelongElement(false);
        if (path == null) {
          path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
        }
        extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
      }

    }

    for (ViewModelAction action : extendInfo.getMultiDeleteActions()) {
      if (!action.getID().equals(InternalExtendActionUtil.MultiDeleteActionId)) {
        if(isGenCompCode(actionList,action.getCode())){
          JavaAbstractMultiDeleteActionGenerator gen = new JavaAbstractMultiDeleteActionGenerator(
              viewModel, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
              compAssemblyName, relativePath);
          gen.setBelongElement(false);
          if (path == null) {
            path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
          }
          extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
        }

      }
    }

    for (ViewModelAction action : extendInfo.getAfterMultiDeleteActions()) {
      if(isGenCompCode(actionList,action.getCode())){
        JavaAfterMultiDeleteActionGenerator gen = new JavaAfterMultiDeleteActionGenerator(viewModel,
            (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
            relativePath);
        gen.setBelongElement(false);
        if (path == null) {
          path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
        }
        extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
      }

    }

    for (ViewModelAction action : extendInfo.getBeforeDeleteActions()) {
      if(isGenCompCode(actionList,action.getCode())){
        JavaBeforeDeleteActionGenerator gen = new JavaBeforeDeleteActionGenerator(viewModel,
            (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
            relativePath);
        gen.setBelongElement(false);
        if (path == null) {
          path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
        }
        extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
      }

    }

    for (ViewModelAction action : extendInfo.getDeleteActions()) {
      if (!action.getID().equals(InternalExtendActionUtil.DeleteActionId)) {
        if(isGenCompCode(actionList,action.getCode())){
          JavaAbstractDeleteActionGenerator gen = new JavaAbstractDeleteActionGenerator(viewModel,
              (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
              compAssemblyName, relativePath);
          gen.setBelongElement(false);
          if (path == null) {
            path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
          }
          extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
        }

      }
    }

    for (ViewModelAction action : extendInfo.getAfterDeleteActions()) {
      if(isGenCompCode(actionList,action.getCode())){
        JavaAfterDeleteActionGenerator gen = new JavaAfterDeleteActionGenerator(viewModel,
            (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
            relativePath);
        gen.setBelongElement(false);
        if (path == null) {
          path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
        }
        extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
      }

    }

    for (ViewModelAction action : extendInfo.getBeforeSaveActions()) {
      if(isGenCompCode(actionList,action.getCode())){
        JavaBeforeSaveActionGenerator gen = new JavaBeforeSaveActionGenerator(viewModel,
            (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
            relativePath);
        gen.setBelongElement(false);
        if (path == null) {
          path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
        }
        extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
      }

    }

    for (ViewModelAction action : extendInfo.getAfterSaveActions()) {
      if(isGenCompCode(actionList,action.getCode())){
        JavaAfterSaveActionGenerator gen = new JavaAfterSaveActionGenerator(viewModel,
            (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
            relativePath);
        gen.setBelongElement(false);
        if (path == null) {
          path = prepareDir(((MappedCdpAction) action).getComponentEntityId());
        }
        extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
      }

    }
  }

  /**
   * 变量联动计算构件代码生成
   */
  private void generateVariableDtms(ArrayList actionList) {
    if (viewModel.getVariables() == null) {
      return;
    }
    //var path = prepareDir(viewModel.Code,voVarFileName);
    javaGenerateVariableDtms(viewModel.getVariables().getDtmAfterCreate(), actionList);
    javaGenerateVariableDtms(viewModel.getVariables().getDtmAfterModify(), actionList);
    javaGenerateVariableDtms(viewModel.getVariables().getDtmBeforeSave(), actionList);
  }

  private void javaGenerateVariableDtms(CommonDtmCollection dtms, ArrayList actionList) {
    String dirPath = null;

    for (CommonDetermination dtm : dtms) {
      if (dirPath == null) {
        dirPath = prepareDir(dtm.getComponentId());
      }
      if (!dtm.getIsRef() && dtm.getIsGenerateComponent() && actionList.contains(dtm.getCode())) {
        javaGenerateVariableDtm(dtm, dirPath);
      }
    }
  }

  private void javaGenerateVariableDtm(CommonDetermination dtm, String dirPath) {
    JavaCommonDeterminationGenerator gen = new JavaCommonDeterminationGenerator(viewModel, dtm,
        compAssemblyName, relativePath);
    generateSplitFile(gen, dirPath, false);
  }

  private String getMetaProjName(GspProject projInfo) {
    String boProjName = projInfo.getMetadataProjectName().toLowerCase();
    return boProjName.substring(0, 0) + boProjName.substring(0 + boProjName.indexOf('-') + 1);
  }

  private String prepareDir(String componentEntityId) {
    MetadataProjectService service = SpringBeanUtils.getBean(MetadataProjectService.class);

    String compModulePath = service.getJavaCompProjectPath(relativePath);

    String className = VmManagerService.getComponentJavaClassName(componentEntityId);
    //构件中获取javaClassName,需要截取掉一级ClassName拼接相对路径

    String path = String.format("%1$s%2$s%3$s", compModulePath, ViewModelUtils.getSeparator(),
        (className.substring(0, className.lastIndexOf(".")).replace(".", ViewModelUtils.getSeparator())));
    if (!fsService.isDirectoryExist(path)) {
      fsService.createDirectory(path);
    }
    return path;
  }


  //ORIGINAL LINE: private void extendActionCodeGen(MappedCdpAction vmAction, JavaIBaseCompCodeGen codeGen, string dirPath, bool belongElement = false)
  private void extendActionCodeGen(MappedCdpAction vmAction, JavaIBaseCompCodeGen codeGen,
      String dirPath, boolean belongElement) {
    if (vmAction.getIsGenerateComponent()) {
      generateSplitFile(codeGen, dirPath, false);
    }
  }

  private void VMCodeGenerate(GspViewModel viewModel, MappedCdpAction vmAction, String dirPath,
      boolean belongElement) {
    if (vmAction.getIsGenerateComponent()) {
      JavaMappedCdpActionGenerator gen = new JavaMappedCdpActionGenerator(viewModel, vmAction,
          compAssemblyName, relativePath);
      gen.setBelongElement(belongElement);
      generateSplitFile(gen, dirPath, belongElement);
    }
  }

  // belongElement 默认为false,
  private void generateSplitFile(JavaIBaseCompCodeGen codeGen, String dirPath,
      boolean belongElement) {
    belongElement = true; //已存在文件时，true:不覆盖；false:覆盖
    String compName = codeGen.getCompName();

    String originalFilePath = ViewModelUtils
        .getCombinePath(relativePath, ComponentGenUtil.ComponentDir, compName + codeFileExtension);

    String filePathCommon = fsService.getCombinePath(dirPath, compName + codeFileExtension);
    if (fsService.isFileExist(filePathCommon) && belongElement) {
      return;
    }
    if (fsService.isFileExist(originalFilePath)) {
      return;
    }

    if (codeGen.getIsCommonGenerate()) {
      iFsService.createFile(filePathCommon, codeGen.generateCommon());
    } else {
      iFsService.createFile(filePathCommon, codeGen.generateExecute());
    }
  }
}
