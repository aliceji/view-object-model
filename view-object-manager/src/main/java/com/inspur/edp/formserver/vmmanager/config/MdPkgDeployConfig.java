/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.config;

import io.iec.edp.caf.commons.event.config.EventListenerData;
import io.iec.edp.caf.commons.event.config.EventManagerData;
import lombok.var;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration(proxyBeanMethods=false)
public class MdPkgDeployConfig {

    @Bean
    public EventManagerData createEventManagerData(){
        EventManagerData data= new EventManagerData();
        data.setName("MdpkgChangedEventManager");
        List<EventListenerData> listeners = new ArrayList<>();
        data.setListeners(listeners);
        var listenerData = new EventListenerData();
        listenerData.setName("VoListener");
        listenerData.setImplClassName("com.inspur.edp.formserver.vmmanager.lowcode.MdPkgDeployListener");
        data.getListeners().add(listenerData);
        return data;
    }
}
