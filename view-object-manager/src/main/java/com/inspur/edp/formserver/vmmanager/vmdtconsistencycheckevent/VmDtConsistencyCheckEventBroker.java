/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.vmdtconsistencycheckevent;

import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.actioneventargs.*;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.entityeventargs.ChangingViewObjectCodeEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.entityeventargs.RemovingViewObjectEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.ChangingVoFieldDataTypeEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.ChangingVoFieldLabelIdEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.ChangingVoFieldObjectTypeEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.RemovingVoFieldEventArgs;
import io.iec.edp.caf.commons.event.EventBroker;
import io.iec.edp.caf.commons.event.config.EventListenerSettings;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public class VmDtConsistencyCheckEventBroker extends EventBroker {

  private VmDtConsistencyCheckEventManager eventManager = SpringBeanUtils.getBean(
      VmDtConsistencyCheckEventManager.class);

  public VmDtConsistencyCheckEventBroker(EventListenerSettings settings) {
    super(settings);
    this.init();
  }

  @Override
  protected void onInit() {
    this.getEventManagerCollection().add(eventManager);
  }

  public void fireChangingViewObjectCode(ChangingViewObjectCodeEventArgs args) {
    eventManager.fireChangingViewObjectCode(args);
  }

  public void fireRemovingViewObject(RemovingViewObjectEventArgs args) {
    eventManager.fireRemovingViewObject(args);
  }

  public void fireRemovingVoField(RemovingVoFieldEventArgs args) {
    eventManager.fireRemovingVoField(args);
  }

  public void fireChangingVoFieldDataType(ChangingVoFieldDataTypeEventArgs args) {
    eventManager.fireChangingVoFieldDataType(args);
  }

  public void fireChangingVoFieldObjectType(ChangingVoFieldObjectTypeEventArgs args) {
    eventManager.fireChangingVoFieldObjectType(args);
  }

  public void fireChangingVoFieldLabelId(ChangingVoFieldLabelIdEventArgs args) {
    eventManager.fireChangingVoFieldLabelId(args);
  }

  public void fireChangingVoActionCode(ChangingVoActionCodeEventArgs args) {
    eventManager.fireChangingVoActionCode(args);
  }

  public void fireChangingVoActionParams(ChangingVoActionParamsEventArgs args) {
    eventManager.fireChangingVoActionParams(args);
  }

  public void fireChangingVoActionReturn(ChangingVoActionReturnEventArgs args) {
    eventManager.fireChangingVoActionReturn(args);
  }

  public void fireDeletingVoAction(DeletingVoActionEventArgs args) {
    eventManager.fireDeletingVoAction(args);
  }
  public void fireChangingVoActionCollectType(ChangingVoActionCollectTypeEventArgs args){
    eventManager.fireChangingVoActionCollectType(args);
  }
}
