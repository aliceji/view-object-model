/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.generatacmpcode;

import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;

public class JavaMappedCdpActionGenerator extends JavaBaseActionGenerator
{
	private MappedCdpAction action;

		///#region 属性
	//private string allInterfaceName;
	//private string beMgrInterfaceName;
	@Override
	protected String getBaseClassName()
	{
		if(getBelongElement())
		{
			return "AbstractHelpAction";
		}
		else
		{
			return "AbstractFSAction<" + ReturnTypeName + ">";
		}
	}


		///#endregion


		///#region 构造函数
	public JavaMappedCdpActionGenerator(GspViewModel vm, MappedCdpAction vmAction, String nameSpace, String path)
	{
		super(vm, vmAction, nameSpace, path);
		this.action = vmAction;
		//allInterfaceName = ApiHelper.getBEAllInterfaceClassName(vm);
		//beMgrInterfaceName = ApiHelper.getBEMgrInterfaceClassName(vm);
	}
	@Override
	public String getNameSpaceSuffix()
	{
		return JavaCompCodeNames.VOActionNameSpaceSuffix;
	}


		///#endregion


		///#region generateConstructor

	@Override
	public void generateConstructor(StringBuilder result)
	{
		result.append(getIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ").append(getCompName()).append("(");

		if (hasCustomConstructorParams())
		{
			generateConstructorParams(result);
			result.append(") ");
		}
		else
		{
			result.append(") ");
		}

		result.append("{").append(getNewLine());
		generateConstructorContent(result);
		result.append(getNewLine()).append(getIndentationStr()).append("}").append(getNewLine());

		//generateExecute(result);

	}

	protected final void generateExecute(StringBuilder result)
	{
		result.append(getIndentationStr()).append("@").append(JavaCompCodeNames.KeywordOverride).append(getNewLine());
		result.append(getIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ").append(JavaCompCodeNames.KeywordVoid).append(" ").append("execute()").append(" ").append("{").append(getNewLine());
		result.append(getIndentationStr()).append("}").append(getNewLine());
	}

	@Override
	protected void generateExtendMethod(StringBuilder result)
	{
		//generategetEntityMethod(result);
		//generategetMgrMethod(result);
	}

	//private void generategetEntityMethod(StringBuilder result)
	//{
	//    result.append(getDoubleIndentationStr()).append(JavaCompCodeNames.KeywordPrivate).append(" ")
	//        .append(allInterfaceName).append(getNewLine()).append(" getEntity(string dataId) ");
	//    result.append(getDoubleIndentationStr()).append(getNewLine()).append("{");

	//    result.append(getDoubleIndentationStr()).append(getIndentationStr())
	//        .append("return base.BEManagerContext.getEntity(dataId) as ").append(allInterfaceName).append(getNewLine()).append(" ;");

	//    result.append(getDoubleIndentationStr()).append(getNewLine()).append("}");
	//}

	//private void generategetMgrMethod(StringBuilder result)
	//{
	//    result.append(getDoubleIndentationStr()).append(JavaCompCodeNames.KeywordPrivate).append(" ")
	//        .append(beMgrInterfaceName).append(getNewLine()).append(" getMgr() ");
	//    result.append(getDoubleIndentationStr()).append(getNewLine()).append("{");

	//    result.append(getDoubleIndentationStr()).append(getIndentationStr())
	//        .append("return base.BEManagerContext.BEManager as ").append(beMgrInterfaceName).append(getNewLine()).append(" ;");

	//    result.append(getDoubleIndentationStr()).append(getNewLine()).append("}");
	//}

		///#endregion

	@Override
	public String getInitializeCompName()
	{
		return String.format("%1$s%2$s",this.VMAction, "VO");
	}
}
