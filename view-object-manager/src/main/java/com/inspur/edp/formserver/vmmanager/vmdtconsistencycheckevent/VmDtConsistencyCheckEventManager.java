/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.vmdtconsistencycheckevent;

import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.actioneventargs.*;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.entityeventargs.ChangingViewObjectCodeEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.entityeventargs.RemovingViewObjectEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.ChangingVoFieldDataTypeEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.ChangingVoFieldLabelIdEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.ChangingVoFieldObjectTypeEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.RemovingVoFieldEventArgs;
import com.inspur.edp.formserver.viewmodel.viewmodeldtevent.IVoActionDTEventListener;
import com.inspur.edp.formserver.viewmodel.viewmodeldtevent.IVoEntityDTEventListener;
import com.inspur.edp.formserver.viewmodel.viewmodeldtevent.IVoFieldDTEventListener;
import io.iec.edp.caf.commons.event.EventManager;
import io.iec.edp.caf.commons.event.IEventListener;

public class VmDtConsistencyCheckEventManager extends EventManager {

  @Override
  public String getEventManagerName() {
    return "VmDtConsistencyCheckEventManager";
  }

  @Override
  public boolean isHandlerListener(IEventListener listener) {
    return (listener instanceof IVoFieldDTEventListener
        || listener instanceof IVoEntityDTEventListener
        || listener instanceof IVoActionDTEventListener);
  }

  @Override
  public void addListener(IEventListener listener) {
    if (!((listener instanceof IVoFieldDTEventListener
        || listener instanceof IVoEntityDTEventListener
        || listener instanceof IVoActionDTEventListener))) {
      throw new RuntimeException(
          "指定的监听者没有实现IVoFieldDTEventListener接口、IVoEntityDTEventListener接口或IVoActionDTEventListener接口！");
    }
    if (listener instanceof IVoEntityDTEventListener) {
      IVoEntityDTEventListener voEntityDTEventListener = (IVoEntityDTEventListener) listener;
      this.addEventHandler(VmDtConsistencyCheckType.changingViewObjectCode, voEntityDTEventListener,
          "changingViewObjectCode");
      this.addEventHandler(VmDtConsistencyCheckType.removingViewObject, voEntityDTEventListener,
          "removingViewObject");
    }
    if (listener instanceof IVoFieldDTEventListener) {
      IVoFieldDTEventListener voFieldDTEventListener = (IVoFieldDTEventListener) listener;
      this.addEventHandler(VmDtConsistencyCheckType.removingVoField,
          voFieldDTEventListener, "removingVoField");
      this.addEventHandler(VmDtConsistencyCheckType.changingVoFieldDataType,
          voFieldDTEventListener,
          "changingVoFieldDataType");
      this.addEventHandler(VmDtConsistencyCheckType.changingVoFieldObjectType,
          voFieldDTEventListener, "changingVoFieldObjectType");
      this.addEventHandler(VmDtConsistencyCheckType.changingVoFieldLabelId,
          voFieldDTEventListener, "changingVoFieldLabelId");
    }
    if(listener instanceof IVoActionDTEventListener){
      IVoActionDTEventListener voActionDTEventListener = (IVoActionDTEventListener) listener;
      this.addEventHandler(VmDtConsistencyCheckType.changingVoActionCode,voActionDTEventListener,"changingVoActionCode");
      this.addEventHandler(VmDtConsistencyCheckType.changingVoActionParams,voActionDTEventListener,"changingVoActionParams");
      this.addEventHandler(VmDtConsistencyCheckType.changingViewObjectCode,voActionDTEventListener,"changingViewObjectCode");
      this.addEventHandler(VmDtConsistencyCheckType.deletingVoAction,voActionDTEventListener,"deletingVoAction");
      this.addEventHandler(VmDtConsistencyCheckType.changingVoActionCollectType,voActionDTEventListener,"ChangingVoActionCollectType");
    }
  }

  @Override
  public void removeListener(IEventListener listener) {
    if (!((listener instanceof IVoFieldDTEventListener
        || listener instanceof IVoEntityDTEventListener
        || listener instanceof IVoActionDTEventListener))) {
      throw new RuntimeException(
          "指定的监听者没有实现IVoFieldDTEventListener接口、IVoEntityDTEventListener接口或IVoActionDTEventListener接口！");
    }
    if (listener instanceof IVoEntityDTEventListener) {
      IVoEntityDTEventListener voEntityDTEventListener = (IVoEntityDTEventListener) listener;
      this.removeEventHandler(VmDtConsistencyCheckType.changingViewObjectCode,
          voEntityDTEventListener, "changingViewObjectCode");
      this.removeEventHandler(VmDtConsistencyCheckType.removingViewObject, voEntityDTEventListener,
          "removingViewObject");
    }
    if (listener instanceof IVoFieldDTEventListener) {
      IVoFieldDTEventListener voFieldDTEventListener = (IVoFieldDTEventListener) listener;
      this.removeEventHandler(VmDtConsistencyCheckType.removingVoField,
          voFieldDTEventListener, "removingVoField");
      this.removeEventHandler(VmDtConsistencyCheckType.changingVoFieldDataType,
          voFieldDTEventListener,
          "changingVoFieldDataType");
      this.removeEventHandler(VmDtConsistencyCheckType.changingVoFieldObjectType,
          voFieldDTEventListener, "ChangingVoFieldObjectType");
      this.removeEventHandler(VmDtConsistencyCheckType.changingVoFieldLabelId,
          voFieldDTEventListener, "ChangingVoFieldLabelId");
    }
    if(listener instanceof IVoActionDTEventListener){
      IVoActionDTEventListener voActionDTEventListener = (IVoActionDTEventListener) listener;
      this.removeEventHandler(VmDtConsistencyCheckType.changingVoActionCode,voActionDTEventListener,"changingVoActionCode");
      this.removeEventHandler(VmDtConsistencyCheckType.changingVoActionParams,voActionDTEventListener,"changingVoActionParams");
      this.removeEventHandler(VmDtConsistencyCheckType.changingViewObjectCode,voActionDTEventListener,"changingViewObjectCode");
      this.removeEventHandler(VmDtConsistencyCheckType.deletingVoAction,voActionDTEventListener,"deletingVoAction");
      this.removeEventHandler(VmDtConsistencyCheckType.changingVoActionCollectType,voActionDTEventListener,"changingVoActionCollectType");
    }
  }

  //ViewModel
  public final void fireChangingViewObjectCode(ChangingViewObjectCodeEventArgs args) {
    this.fire(VmDtConsistencyCheckType.changingViewObjectCode, args);
  }

  public final void fireRemovingViewObject(RemovingViewObjectEventArgs args) {
    this.fire(VmDtConsistencyCheckType.removingViewObject, args);
  }

  public final void fireRemovingVoField(RemovingVoFieldEventArgs args){
    this.fire(VmDtConsistencyCheckType.removingVoField,args);
  }
  public final void fireChangingVoFieldDataType(ChangingVoFieldDataTypeEventArgs args){
    this.fire(VmDtConsistencyCheckType.changingVoFieldDataType,args);
  }
  public final void fireChangingVoFieldObjectType(ChangingVoFieldObjectTypeEventArgs args){
    this.fire(VmDtConsistencyCheckType.changingVoFieldObjectType,args);
  }
  public final void fireChangingVoFieldLabelId(ChangingVoFieldLabelIdEventArgs args){
    this.fire(VmDtConsistencyCheckType.changingVoFieldLabelId,args);
  }

  public final void fireChangingVoActionCode(ChangingVoActionCodeEventArgs args){
    this.fire(VmDtConsistencyCheckType.changingVoActionCode,args);
  }
  public final void fireChangingVoActionParams(ChangingVoActionParamsEventArgs args){
    this.fire(VmDtConsistencyCheckType.changingVoActionParams,args);
  }
  public final void fireChangingVoActionReturn(ChangingVoActionReturnEventArgs args){
    this.fire(VmDtConsistencyCheckType.changingVoActionReturn,args);
  }
  public final void fireDeletingVoAction(DeletingVoActionEventArgs args){
    this.fire(VmDtConsistencyCheckType.deletingVoAction,args);
  }
  public final void fireChangingVoActionCollectType(ChangingVoActionCollectTypeEventArgs args){
    this.fire(VmDtConsistencyCheckType.changingVoActionCollectType,args);
  }

}
