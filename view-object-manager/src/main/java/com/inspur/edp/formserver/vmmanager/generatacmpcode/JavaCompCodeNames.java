/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.generatacmpcode;

public final class JavaCompCodeNames
{
	public static String VMActionNameSpaceSuffix = "VO";
	public static String VOActionNameSpaceSuffix = "voactions";
	public static String VMActionClassNameSuffix = "VOAction";
	public static String VMControllerName = "VOController";

	public static String AbstractFSActionClassName = "AbstractFSAction<VoidActionResult>";

	public static String KeywordImport = "import";
	public static String KeywordPackage = "package";
	public static String KeywordPublic = "public";
	public static String KeywordProtected = "protected";
	public static String KeywordInternal = "internal";
	public static String KeywordPrivate = "private";
	public static String KeywordClass = "class";
	public static String KeywordOverride = "Override";
	public static String KeywordVoid = "void";
	public static String KeywordQueryContext = "QueryContext";
	public static String KeyWordRetrieveContext = "RetrieveContext";
	public static String KeyWordDataMapperContext = "DataMapperContext";
	public static String Keywordcontext = "context";
	public static String KeywordSuper = "super";

	public static String VMActionEntityNameSpace = "CardSOBizAction";

	public static String FormServerSpiNameSpace = "com.inspur.edp.bff.spi.*";
	public static String BefApiActionNameSpace = "com.inspur.edp.bef.api.action.*";
	public static String VMQueryContextNameSpace = "com.inspur.edp.bff.api.manager.context.QueryContext";
	public static String VMAfterQueryNameSpace = "com.inspur.edp.bff.spi.action.query.AfterQueryAction";
	public static String VMBeforeQueryNameSpace = "com.inspur.edp.bff.spi.action.query.BeforeQueryAction";
	public static String VMQueryNameSpace = "com.inspur.edp.bff.spi.action.query.AbstractQueryAction";
	public static String DataMapperContextNameSpace = "com.inspur.edp.bff.api.manager.context.DataMapperContext";
	public static String DataMappingActionsNameSpace = "com.inspur.edp.bff.spi.action.datamapping.DataMappingAction";
	public static String DataReversalMappingActionNameSpace = "com.inspur.edp.bff.spi.action.datamapping.DataReversalMappingAction";
	public static String RetrieveContextNameSpace = "com.inspur.edp.bff.api.manager.context.RetrieveContext";
	public static String BeforeRetrieveActionNameSpace = "com.inspur.edp.bff.spi.action.retrieve.BeforeRetrieveAction";
	public static String AfterRetrieveActionNameSpace = "com.inspur.edp.bff.spi.action.retrieve.AfterRetrieveAction";
	public static String AbstractRetrieveActionNameSpace = "com.inspur.edp.bff.spi.action.retrieve.AbstractRetrieveAction";

	public static String DeleteContextNameSpace = "com.inspur.edp.bff.api.manager.context.DeleteContext";
	public static String BeforeDeleteActionNameSpace = "com.inspur.edp.bff.spi.action.delete.BeforeDeleteAction";
	public static String AbstractDeleteActionNameSpace = "com.inspur.edp.bff.spi.action.delete.AbstractDeleteAction";
	public static String AfterDeleteActionNameSpace = "com.inspur.edp.bff.spi.action.delete.AfterDeleteAction";

	public static String BeforeMultiDeleteActionNameSpace = "com.inspur.edp.bff.spi.action.multidelete.BeforeMultiDeleteAction";
	public static String AbstractMultiDeleteActionNameSpace = "com.inspur.edp.bff.spi.action.multidelete.AbstractMultiDeleteAction";
	public static String AfterMultiDeleteActionNameSpace = "com.inspur.edp.bff.spi.action.multidelete.AfterMultiDeleteAction";

	public static String SaveContextNameSpace = "com.inspur.edp.bff.api.manager.context.SaveContext";
	public static String BeforeSaveActionNameSpace = "com.inspur.edp.bff.spi.action.save.BeforeSaveAction";
	public static String AfterSaveActionNameSpace = "com.inspur.edp.bff.spi.action.save.AfterSaveAction";



	public static String BeforeRetrieveDefaultActionNameSpace = "com.inspur.edp.bff.spi.action.retrievedefault.BeforeRetrieveDefaultAction";
	public static String RetrieveDefaultContextNameSpace = "com.inspur.edp.bff.api.manager.context.RetrieveDefaultContext";
	public static String AfterRetrieveDefaultActionNameSpace = "com.inspur.edp.bff.spi.action.retrievedefault.AfterRetrieveDefaultAction";
	public static String AbstractRetrieveDefaultActionNameSpace = "com.inspur.edp.bff.spi.action.retrievedefault.AbstractRetrieveDefaultAction";
	public static String AbstractModifyActionNameSpace = "com.inspur.edp.bff.spi.action.modify.AbstractModifyAction";
	public static String BeforeModifyActionNameSpace = "com.inspur.edp.bff.spi.action.modify.BeforeModifyAction";
	public static String AfterModifyActionNameSpace = "com.inspur.edp.bff.spi.action.modify.AfterModifyAction";
	public static String ModifyContextNameSpace = "com.inspur.edp.bff.api.manager.context.ModifyContext";
	public static String ChangeMapperContextSpace = "com.inspur.edp.bff.api.manager.context.ChangeMapperContext";
	public static String ChangeMappingActionNameSpace = "com.inspur.edp.bff.spi.action.changemapping.ChangeMappingAction";
	public static String ChangeReversalMappingActionNameSpace = "com.inspur.edp.bff.spi.action.changemapping.ChangeReversalMappingAction";


	public static String DateNameSpace = "java.util.Date";
	public static String BigDecimalNameSpace = "java.math.BigDecimal";
	public static String ArrayListNameSpace = "java.util.ArrayList";

	//public static string VoidActionResult = "com.inspur.edp.bef.api.action.VoidActionResult";
	//public static string AbstractFSAction = "com.inspur.edp.bff.spi.AbstractFSAction";
	//public static string AbstractFSAction = "com.inspur.edp.bff.spi";


	public static String HelpUsing = " using Inspur.Gsp.Web.LookupManager.Api;";

	public static String VMCmpExtendName = ".vmCmp";
}
