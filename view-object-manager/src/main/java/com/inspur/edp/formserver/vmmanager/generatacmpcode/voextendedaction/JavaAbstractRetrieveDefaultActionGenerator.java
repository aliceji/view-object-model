/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction;


import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.JavaCompCodeNames;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.JavaMappedCdpActionGenerator;

public class JavaAbstractRetrieveDefaultActionGenerator extends JavaMappedCdpActionGenerator
{
	private MappedCdpAction action;

		///#region 属性
	//private string allInterfaceName;
	//private string beMgrInterfaceName;
	@Override
	protected String getBaseClassName()
	{
		return "AbstractRetrieveDefaultAction";
	}


		///#endregion


		///#region 构造函数
	public JavaAbstractRetrieveDefaultActionGenerator(GspViewModel vm, MappedCdpAction vmAction, String nameSpace, String path)
	{
		super(vm, vmAction, nameSpace, path);
		this.action = vmAction;
		//allInterfaceName = ApiHelper.GetBEAllInterfaceClassName(vm);
		//beMgrInterfaceName = ApiHelper.GetBEMgrInterfaceClassName(vm);
	}
	@Override
	public  String getNameSpaceSuffix()
	{
		return JavaCompCodeNames.VOActionNameSpaceSuffix;
	}


		///#endregion


		///#region generateConstructor

	@Override
	public void generateConstructor(StringBuilder result)
	{
		result.append(getIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ").append(getCompName()).append("(RetrieveDefaultContext context)").append(" ").append("{").append(getNewLine());

		result.append(getIndentationStr()).append(getIndentationStr()).append("super(context)").append(";").append(getNewLine());
		result.append(getIndentationStr()).append("}").append(getNewLine());


		//将部分类合并，加入GenerateExecute方法
		//GenerateExecute(result);

	}

		///#endregion

		///#region Using
	@Override
	protected void generateExtendUsing(StringBuilder result)
	{
		result.append(getUsingStr(JavaCompCodeNames.AbstractRetrieveDefaultActionNameSpace));
		result.append(getUsingStr(JavaCompCodeNames.RetrieveDefaultContextNameSpace));
	}

		///#endregion

}
