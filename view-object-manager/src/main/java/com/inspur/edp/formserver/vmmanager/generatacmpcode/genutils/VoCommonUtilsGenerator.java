/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.generatacmpcode.genutils;

import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.GspViewObject;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.ProcessMode;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.io.File;

public class VoCommonUtilsGenerator {
    private final GspViewModel viewModel;
    private final GspMetadata metadata;
    private String relativePath;
    private String compAssemblyName;

    public VoCommonUtilsGenerator(GspViewModel viewModel, GspMetadata metadata)
    {
        this.relativePath = metadata.getRelativePath();
        this.compAssemblyName = viewModel.getGeneratedConfigID().toLowerCase()+".common";
        this.viewModel = viewModel;
        this.metadata = metadata;
    }

    public void generateCommonUtils()
    {
        MetadataProjectService service = SpringBeanUtils.getBean(MetadataProjectService.class);
        ProcessMode mode =  service.getProcessMode(relativePath);
        if(mode!=ProcessMode.interpretation)
            return;
        String compModulePath = service.getJavaCompProjectPath(relativePath);
        File folder =new File(compModulePath);
        if(folder.exists()==false)
            return;
        compAssemblyName=compAssemblyName.replace(".vo.",".");
        for (IGspCommonObject entityObject:viewModel.getAllObjectList())
        {
            new VoEntityUtilsGenerator((GspViewObject) entityObject,compAssemblyName,relativePath).generate();
        }
    }
}
