/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.generatacmpcode.genutils;

import com.inspur.edp.cef.designtime.core.utilsgenerator.DataTypeUtilsGenerator;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.formserver.viewmodel.GspViewObject;

public class VoEntityUtilsGenerator extends DataTypeUtilsGenerator {
    private final GspViewObject viewObject;
    private final String packageName;

    public VoEntityUtilsGenerator(GspViewObject viewObject, String packageName, String basePath) {
        super(viewObject, basePath);
        this.viewObject = viewObject;
        this.packageName = packageName;
    }

    @Override
    protected String getClassName() {
        return viewObject.getCode()+"Utils";
    }

    @Override
    protected String getClassPackage() {
        return packageName;
    }

    @Override
    protected void generateExtendInfos() {
        super.generateExtendInfos();
        generateGetChildObjectDatas();
    }

    private void generateGetChildObjectDatas() {
        if(viewObject.getContainChildObjects()==null||viewObject.getContainChildObjects().size()==0)
            return;
        for(IGspCommonObject childObject:viewObject.getContainChildObjects())
        {
            new EntityGetChildDatasMethodGenerator(childObject.getCode(),getClassInfo()).generate();
        }
    }
}
