/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.util;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.Metadata4Ref;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationRtService;
import com.inspur.edp.metadata.rtcustomization.api.entity.MetadataFilter;
import com.inspur.edp.metadata.rtcustomization.api.entity.SourceTypeEnum;
import com.inspur.edp.metadata.rtcustomization.serverapi.CustomizationRtServerService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SimplifyMetadataUtil {

    private static final Logger logger = LoggerFactory.getLogger(SimplifyMetadataUtil.class);

    public void Simplify() {

        CustomizationRtService customizationRtService = SpringBeanUtils.getBean(CustomizationRtService.class);
        CustomizationRtServerService customizationRtServerService = SpringBeanUtils.getBean(CustomizationRtServerService.class);
        List<String> metadataTypes = new ArrayList<>();
        metadataTypes.add("GSPBusinessEntity");
        metadataTypes.add("GSPViewModel");
        metadataTypes.add("UnifiedDataType");

        List<Metadata4Ref> metadataListByFilter = customizationRtService.getMetadataListByFilter(new MetadataFilter(SourceTypeEnum.MDPKG, metadataTypes));
        metadataListByFilter.forEach(metadata4Ref -> {
            try{
                GspMetadata metadata = customizationRtService.getMetadata(metadata4Ref.getMetadata().getHeader().getId(), false);
                customizationRtServerService.updateMetadata(metadata);
            }catch (Exception e) {
                //ignore
                logger.info("元数据ID为 "+metadata4Ref.getMetadata().getHeader().getId()+" 的元数据精简失败",e);
            }
        });
        logger.info("批量精简元数据完成");
    }
}
