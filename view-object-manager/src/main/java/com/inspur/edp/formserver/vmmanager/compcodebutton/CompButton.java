/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.compcodebutton;

import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.variable.dtgenerator.vardtmGenerate.javageneratecmpcode.JavaCommonDeterminationGenerator;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.common.ValueHelpConfig;
import com.inspur.edp.formserver.viewmodel.util.ViewModelUtils;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.JavaMappedCdpActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.*;
import com.inspur.edp.formserver.vmmanager.service.VmManagerService;
import com.inspur.edp.formserver.vmmanager.util.CheckComUtil;

import java.util.ArrayList;
import java.util.List;

public class CompButton {

    private static final String javaCodeFileExtension = ".java";
    private static final String javaPathInfo = "java\\code\\comp\\src\\main\\java";

    public String getCompPath(GspViewModel vo, String type, String relativePath, ArrayList<String> actionList, String compAssemblyName) {
        String componentEntityId = "";
        if(!"VarDeterminations".equals(type)){
            componentEntityId =((MappedCdpAction)getAction(actionList.get(0),vo,type)).getComponentEntityId();
        }else {
            componentEntityId = getDtm(actionList.get(0),vo).getComponentId();
        }
        String classname = VmManagerService.getComponentJavaClassName(componentEntityId).replace(".", ViewModelUtils.getSeparator());
        String path = ViewModelUtils.getCombinePath(javaPathInfo, classname+javaCodeFileExtension);
        return path;
    }

    public String getCompCode(GspViewModel vo, String type, String relativePath, ArrayList<String> actionList, String compAssemblyName) {
        String code = "";
        if(!"VarDeterminations".equals(type)){
            ViewModelAction action = getAction(actionList.get(0),vo,type);
            code = getActionCode(action,vo,compAssemblyName,relativePath,type);
        }else {
            CommonDetermination action = getDtm(actionList.get(0),vo);
            code = getDtmCode(action,vo,compAssemblyName,relativePath);
        }
        return code;
    }

    private String getDtmCode(CommonDetermination action, GspViewModel vo, String compAssemblyName, String relativePath){
        JavaCommonDeterminationGenerator gen = new JavaCommonDeterminationGenerator(vo, action, compAssemblyName, relativePath);
        if(gen.getIsCommonGenerate()){
            return gen.generateCommon();
        }else {
            return gen.generateExecute();
        }
    }

    private String getActionCode(ViewModelAction action, GspViewModel vo, String compAssemblyName, String relativePath, String type){
        switch (type){
            case "VMActions":
                JavaMappedCdpActionGenerator gen = new JavaMappedCdpActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(gen.getIsCommonGenerate()){
                    return gen.generateCommon();
                }else {
                    return gen.generateExecute();
                }
            case "BeforeHelp":
                JavaMappedCdpActionGenerator gena = new JavaMappedCdpActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                gena.setBelongElement(true);
                if(gena.getIsCommonGenerate()){
                    return gena.generateCommon();
                }else {
                    return gena.generateExecute();
                }
            case "AfterMultiDeleteActions":
                JavaAfterMultiDeleteActionGenerator genb = new JavaAfterMultiDeleteActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(genb.getIsCommonGenerate()){
                    return genb.generateCommon();
                }else {
                    return genb.generateExecute();
                }
            case "MultiDeleteActions":
                JavaAbstractMultiDeleteActionGenerator genc = new JavaAbstractMultiDeleteActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(genc.getIsCommonGenerate()){
                    return genc.generateCommon();
                }else {
                    return genc.generateExecute();
                }
            case "BeforeMultiDeleteActions":
                JavaBeforeMultiDeleteActionGenerator gend = new JavaBeforeMultiDeleteActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(gend.getIsCommonGenerate()){
                    return gend.generateCommon();
                }else {
                    return gend.generateExecute();
                }
            case "ModifyActions":
                JavaAbstractModifyActionGenerator gene = new JavaAbstractModifyActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(gene.getIsCommonGenerate()){
                    return gene.generateCommon();
                }else {
                    return gene.generateExecute();
                }
            case "AfterModifyActions":
                JavaAfterModifyActionGenerator genf = new JavaAfterModifyActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(genf.getIsCommonGenerate()){
                    return genf.generateCommon();
                }else {
                    return genf.generateExecute();
                }
            case "BeforeModifyActions":
                JavaBeforeModifyActionGenerator geng = new JavaBeforeModifyActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(geng.getIsCommonGenerate()){
                    return geng.generateCommon();
                }else {
                    return geng.generateExecute();
                }
            case "BeforeSaveActions":
                JavaBeforeSaveActionGenerator genh = new JavaBeforeSaveActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(genh.getIsCommonGenerate()){
                    return genh.generateCommon();
                }else {
                    return genh.generateExecute();
                }
            case "AfterSaveActions":
                JavaAfterSaveActionGenerator geni = new JavaAfterSaveActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(geni.getIsCommonGenerate()){
                    return geni.generateCommon();
                }else {
                    return geni.generateExecute();
                }
            case "BeforeCreateActions":
                JavaBeforeRetrieveDefaultActionGenerator genj = new JavaBeforeRetrieveDefaultActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(genj.getIsCommonGenerate()){
                    return genj.generateCommon();
                }else {
                    return genj.generateExecute();
                }
            case "CreateActions":
                JavaAbstractRetrieveDefaultActionGenerator genk = new JavaAbstractRetrieveDefaultActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(genk.getIsCommonGenerate()){
                    return genk.generateCommon();
                }else {
                    return genk.generateExecute();
                }
            case "AfterCreateActions":
                JavaAfterRetrieveDefaultActionGenerator genl = new JavaAfterRetrieveDefaultActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(genl.getIsCommonGenerate()){
                    return genl.generateCommon();
                }else {
                    return genl.generateExecute();
                }
            case "BeforeDeleteActions":
                JavaBeforeDeleteActionGenerator genm = new JavaBeforeDeleteActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(genm.getIsCommonGenerate()){
                    return genm.generateCommon();
                }else {
                    return genm.generateExecute();
                }
            case "DeleteActions":
                JavaAbstractDeleteActionGenerator genn = new JavaAbstractDeleteActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(genn.getIsCommonGenerate()){
                    return genn.generateCommon();
                }else {
                    return genn.generateExecute();
                }
            case "AfterDeleteActions":
                JavaAfterDeleteActionGenerator geno = new JavaAfterDeleteActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(geno.getIsCommonGenerate()){
                    return geno.generateCommon();
                }else {
                    return geno.generateExecute();
                }
            case "BeforeQueryActions":
                JavaBeforeQueryActionGenerator genp = new JavaBeforeQueryActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(genp.getIsCommonGenerate()){
                    return genp.generateCommon();
                }else {
                    return genp.generateExecute();
                }
            case "QueryActions":
                JavaQueryActionGenerator genq = new JavaQueryActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(genq.getIsCommonGenerate()){
                    return genq.generateCommon();
                }else {
                    return genq.generateExecute();
                }
            case "AfterQueryActions":
                JavaAfterQueryActionGenerator genr = new JavaAfterQueryActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(genr.getIsCommonGenerate()){
                    return genr.generateCommon();
                }else {
                    return genr.generateExecute();
                }
            case "BeforeRetrieveActions":
                JavaBeforeRetrieveActionsGenerator gens = new JavaBeforeRetrieveActionsGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(gens.getIsCommonGenerate()){
                    return gens.generateCommon();
                }else {
                    return gens.generateExecute();
                }
            case "RetrieveActions":
                JavaRetrieveActionGenerator gent = new JavaRetrieveActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(gent.getIsCommonGenerate()){
                    return gent.generateCommon();
                }else {
                    return gent.generateExecute();
                }
            case "AfterRetrieveActions":
                JavaAfterRetrieveActionGenerator genu = new JavaAfterRetrieveActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(genu.getIsCommonGenerate()){
                    return genu.generateCommon();
                }else {
                    return genu.generateExecute();
                }
            case "DataMappingActions":
                JavaDataMappingActionsGenerator genv = new JavaDataMappingActionsGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(genv.getIsCommonGenerate()){
                    return genv.generateCommon();
                }else {
                    return genv.generateExecute();
                }
            case "ChangesetReversalMappingActions":
                JavaChangeReversalMappingActionGenerator genw = new JavaChangeReversalMappingActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(genw.getIsCommonGenerate()){
                    return genw.generateCommon();
                }else {
                    return genw.generateExecute();
                }
            case "DataReversalMappingActions":
                JavaDataReversalMappingActionsGenerator genx = new JavaDataReversalMappingActionsGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(genx.getIsCommonGenerate()){
                    return genx.generateCommon();
                }else {
                    return genx.generateExecute();
                }
            case "ChangesetMappingActions":
                JavaChangeMappingActionGenerator geny = new JavaChangeMappingActionGenerator(vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if(geny.getIsCommonGenerate()){
                    return geny.generateCommon();
                }else {
                    return geny.generateExecute();
                }
            default:
                throw new RuntimeException("构件类型 "+type+" 无法识别");
        }
    }

    private ViewModelAction getAction(String code , GspViewModel vo, String type){
        switch (type){
            case "VMActions":
                for(ViewModelAction action : vo.getActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "BeforeHelp":
                for (ValueHelpConfig valueHelpConfig : vo.getValueHelpConfigs()) {
                    for(ViewModelAction action : valueHelpConfig.getHelpExtend().getBeforeHelp()){
                        if(code.equals(action.getCode()))
                            return action;
                    }
                }
            case "AfterMultiDeleteActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getAfterMultiDeleteActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "MultiDeleteActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getMultiDeleteActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "BeforeMultiDeleteActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getBeforeMultiDeleteActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "ModifyActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getModifyActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "AfterModifyActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getAfterModifyActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "BeforeModifyActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getBeforeModifyActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "BeforeSaveActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getBeforeSaveActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "AfterSaveActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getAfterSaveActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "BeforeCreateActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getBeforeCreateActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "CreateActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getCreateActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "AfterCreateActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getAfterCreateActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "BeforeDeleteActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getBeforeDeleteActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "DeleteActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getDeleteActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "AfterDeleteActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getAfterDeleteActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "BeforeQueryActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getBeforeQueryActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "QueryActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getQueryActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "AfterQueryActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getAfterQueryActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "BeforeRetrieveActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getBeforeRetrieveActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "RetrieveActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getRetrieveActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "AfterRetrieveActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getAfterRetrieveActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "DataMappingActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getDataMappingActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "ChangesetReversalMappingActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getChangesetReversalMappingActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "DataReversalMappingActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getDataReversalMappingActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }
            case "ChangesetMappingActions":
                for(ViewModelAction action : vo.getDataExtendInfo().getChangesetMappingActions()){
                    if(code.equals(action.getCode()))
                        return action;
                }

            default:
                throw new RuntimeException("构件类型 "+type+" 无法识别");
        }
    }

    private CommonDetermination getDtm(String code , GspViewModel vo){
        for(CommonDetermination action : vo.getVariables().getDtmAfterModify()){
            if(code.equals(action.getCode()))
                return action;
        }
        throw new RuntimeException("变量联动计算"+code+"不存在，请检查元数据ID:"+vo.getID());
    }


    //保存前校验遍历获取所有新增动作信息
    public void getNewActions(GspViewModel vo, List<CheckComUtil> checkInfo){
        for(ViewModelAction action : vo.getActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("VO动作");
                info.setType("VMActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ValueHelpConfig valueHelpConfig : vo.getValueHelpConfigs()) {
            for(ViewModelAction action : valueHelpConfig.getHelpExtend().getBeforeHelp()) {
                if (action.getComponentName() == null || "".equals(action.getComponentName())) {
                    CheckComUtil info = new CheckComUtil();
                    info.setAction(action.getCode());
                    info.setCheckInfo("帮助前事件");
                    info.setType("BeforeHelp");
                    info.setActionName(action.getName());
                    checkInfo.add(info);
                }
            }
        }
        for(CommonDetermination action : vo.getVariables().getDtmAfterModify()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("自定义变量联动计算");
                info.setType("VarDeterminations");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action : vo.getDataExtendInfo().getMultiDeleteActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("批量删除扩展动作");
                info.setType("MultiDeleteActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action : vo.getDataExtendInfo().getBeforeMultiDeleteActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("批量删除前扩展动作");
                info.setType("BeforeMultiDeleteActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action :vo.getDataExtendInfo().getAfterMultiDeleteActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("批量删除后扩展动作");
                info.setAction("AfterMultiDeleteActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action : vo.getDataExtendInfo().getQueryActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("查询数据扩展动作");
                info.setType("QueryActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action : vo.getDataExtendInfo().getBeforeQueryActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("查询数据前扩展动作");
                info.setAction("BeforeQueryActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action : vo.getDataExtendInfo().getAfterQueryActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("查询数据后扩展动作");
                info.setAction("AfterQueryActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action : vo.getDataExtendInfo().getRetrieveActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("检索数据扩展动作");
                info.setType("RetrieveActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action : vo.getDataExtendInfo().getBeforeRetrieveActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("检索数据前扩展动作");
                info.setType("BeforeRetrieveActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action : vo.getDataExtendInfo().getAfterRetrieveActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("检索数据后扩展动作");
                info.setType("AfterRetrieveActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action :  vo.getDataExtendInfo().getCreateActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("新增数据扩展动作");
                info.setType("CreateActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action :  vo.getDataExtendInfo().getBeforeCreateActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("新增数据前扩展动作");
                info.setType("BeforeCreateActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action :  vo.getDataExtendInfo().getAfterCreateActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("新增数据后扩展动作");
                info.setType("AfterCreateActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action :  vo.getDataExtendInfo().getDeleteActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("删除数据扩展动作");
                info.setType("DeleteActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action :  vo.getDataExtendInfo().getBeforeDeleteActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("删除数据前扩展动作");
                info.setType("BeforeDeleteActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action : vo.getDataExtendInfo().getAfterDeleteActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("删除数据后扩展动作");
                info.setType("AfterDeleteActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action :  vo.getDataExtendInfo().getModifyActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("修改数据扩展动作");
                info.setType("ModifyActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action :  vo.getDataExtendInfo().getBeforeModifyActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("修改数据前扩展动作");
                info.setType("BeforeModifyActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action :  vo.getDataExtendInfo().getAfterModifyActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("修改数据后扩展动作");
                info.setType("AfterModifyActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action :  vo.getDataExtendInfo().getBeforeSaveActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("保存数据前扩展动作");
                info.setType("BeforeSaveActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action :  vo.getDataExtendInfo().getAfterSaveActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("保存数据后扩展动作");
                info.setType("AfterSaveActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action :  vo.getDataExtendInfo().getChangesetMappingActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("变更集Mapping扩展动作");
                info.setType("ChangesetMappingActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action :  vo.getDataExtendInfo().getChangesetReversalMappingActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("变更集反向Mapping扩展动作");
                info.setType("ChangesetReversalMappingActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action :  vo.getDataExtendInfo().getDataReversalMappingActions()){
            if(action.getComponentName()==null||"".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("数据反向Mapping扩展动作");
                info.setType("DataReversalMappingActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for(ViewModelAction action :  vo.getDataExtendInfo().getDataMappingActions()) {
            if (action.getComponentName() == null || "".equals(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setCheckInfo("数据Mapping扩展动作");
                info.setType("DataMappingActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
    }
    public String getFilePath(GspViewModel vo, CheckComUtil info,String compModulePath,String compAssemblyName){
        String path = getClassName(vo,info.getType(),compModulePath,compAssemblyName,info.getAction());
        return path;
    }

    private String getClassName(GspViewModel vo,String type,String compModulePath,String compAssemblyName,String actionCode){
        String classPath = "";
        if(!"VarDeterminations".equals(type)){
            classPath = compModulePath+"\\com\\"+compAssemblyName.replace(".", ViewModelUtils.getSeparator())+"\\"+vo.getName()+"\\"+"voactions"+"\\"+actionCode+"VOAction.java";
        }else {
            classPath = compModulePath+"\\com\\"+compAssemblyName.replace(".", ViewModelUtils.getSeparator())+"\\"+vo.getName()+"\\"+"vardeterminations"+"\\"+vo.getName()+"Variable"+actionCode+"VODtm.java";
        }
        return classPath;
    }

}
