/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.createvmmetadata;


import com.inspur.edp.formserver.viewmodel.util.ViewModelUtils;
import com.inspur.edp.lcm.fs.api.IFsService;
import com.inspur.edp.lcm.metadata.api.entity.MetadataProject;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public final class ComponentGenUtil
{
	public static final String ComponentDir = "component";

	public static String getComponentAssemblyName(String path)
	{
		MetadataProjectService service= SpringBeanUtils.getBean(MetadataProjectService.class);
		MetadataProject projSvr = service.getMetadataProjInfo(path);

		if (projSvr == null)
		{
			throw new RuntimeException("请先建立元数据工程！");
		}
		return projSvr.getNameSpace();
	}

	public static String prepareComponentDir(String vmPath)
	{
		String  path = ViewModelUtils.getCombinePath(vmPath, ComponentDir);
		IFsService fsSvr = SpringBeanUtils.getBean(IFsService.class);
		if (!fsSvr.existsAsDir(path))
		{
			fsSvr.createDir(path);
		}
		return path;
	}
}
