/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.voextendinfo.server.core.rpcserviceimp;

import com.inspur.edp.formserver.viewmodel.extendinfo.entity.GspVoExtendInfo;
import com.inspur.edp.formserver.vmmanager.extendinfo.repository.GspVoExtendInfoRepository;
import com.inspur.edp.formserver.voextendinfo.server.api.GspVoExtendInfoRpcService;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class GspVoExtendInfoRpcServiceImp implements GspVoExtendInfoRpcService {
  private static ConcurrentHashMap<String, GspVoExtendInfo> gspVOExtendInfos = new ConcurrentHashMap<>();
  private GspVoExtendInfoRepository voExtendInfoRepository;

  public GspVoExtendInfoRpcServiceImp(GspVoExtendInfoRepository voExtendInfoRepository){
    this.voExtendInfoRepository=voExtendInfoRepository;
  }

  @Override
  public GspVoExtendInfo getVoExtendInfo(String id) {
    if(id == null || id.length() == 0){
      return null;
    }
    if(!gspVOExtendInfos.containsKey(id)) {
      GspVoExtendInfo voExtendInfo = voExtendInfoRepository.findById(id).orElse(null);
      if(voExtendInfo == null)
        return null;
      gspVOExtendInfos.put(id,voExtendInfo);
    }
      return gspVOExtendInfos.get(id);
  }

  @Override
  public GspVoExtendInfo getVoExtendInfoByConfigId(String configId) {
    if (configId == null || configId.length() == 0) {
      return null;
    }
    if (!gspVOExtendInfos.containsKey(configId)) {
      List<GspVoExtendInfo> infos = voExtendInfoRepository.getVoExtendInfosByConfigId(configId);
      if (infos.size() > 1) {
        StringBuilder exceptionInfo = new StringBuilder("存在重复configID: [" + configId + "],请处理数据库中的异常重复数据。");
        int index = 1;
        for (GspVoExtendInfo info : infos) {
          exceptionInfo.append("\n重复元数据[").append(index).append("]的Id为：[").append(info.getId()).append("]");
          index++;
        }
        throw new CAFRuntimeException("", "", exceptionInfo.toString(), null, ExceptionLevel.Error);
      } else if (infos.size() == 0) {
        return null;
      } else
        gspVOExtendInfos.put(configId, infos.get(0));
    }
    return gspVOExtendInfos.get(configId);
  }

  @Override
  public List<GspVoExtendInfo> getVoExtendInfos() {
    return voExtendInfoRepository.findAll();
  }

  @Override
  public List<GspVoExtendInfo> getVoId(String beId) {
    return voExtendInfoRepository.getVoExtendInfoByBeSourceId(beId);
  }


  public void saveGspVoExtendInfos(List<GspVoExtendInfo> infos){
    voExtendInfoRepository.saveAll(infos);
  }

  public void deleteVoExtendInfo(String id){
    GspVoExtendInfo voExtendInfo = voExtendInfoRepository.findById(id).orElse(null);
    if(id != null && gspVOExtendInfos.containsKey(id)){
      gspVOExtendInfos.remove(id);
    }else if(voExtendInfo!=null && voExtendInfo.getConfigId()!=null && gspVOExtendInfos.containsKey(voExtendInfo.getConfigId())){
      gspVOExtendInfos.remove(voExtendInfo.getConfigId());
    }
    if(voExtendInfo != null) {
      voExtendInfoRepository.deleteById(id);
    }
  }
}
