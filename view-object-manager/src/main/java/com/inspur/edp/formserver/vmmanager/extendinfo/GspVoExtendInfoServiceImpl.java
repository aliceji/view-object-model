/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.extendinfo;

import com.inspur.edp.formserver.viewmodel.extendinfo.api.GspVoExtendInfoService;
import com.inspur.edp.formserver.viewmodel.extendinfo.entity.GspVoExtendInfo;
import com.inspur.edp.formserver.vmmanager.extendinfo.repository.GspVoExtendInfoRepository;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.rpc.api.service.RpcClient;
import io.iec.edp.caf.rpc.api.support.Type;
import org.springframework.dao.IncorrectResultSizeDataAccessException;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class GspVoExtendInfoServiceImpl implements GspVoExtendInfoService {

    private GspVoExtendInfoRepository extendInfoRepository;
    protected static ConcurrentHashMap<String, GspVoExtendInfo> configIdGspVoExtendInfo;
    protected static ConcurrentHashMap<String, GspVoExtendInfo> idGspVoExtendInfo;

    private String Lcm_SU = "Lcm";
    private String VIEWWMODEL_SERVICE="com.inspur.edp.formserver.voextendinfo.server.api.GspVoExtendInfoRpcService";

    public GspVoExtendInfoServiceImpl(GspVoExtendInfoRepository extendInfoRepository) {
        this.extendInfoRepository = extendInfoRepository;
        configIdGspVoExtendInfo = new ConcurrentHashMap<>();
        idGspVoExtendInfo = new ConcurrentHashMap<>();
    }
    // 使用be Id的取消缓存
    @Override
    public GspVoExtendInfo getVoExtendInfo(String id) {
        GspVoExtendInfo voExtendInfo = idGspVoExtendInfo.get(id);
        if(voExtendInfo == null) {
            RpcClient client = SpringBeanUtils.getBean(RpcClient.class);
            LinkedHashMap<String, Object> params = new LinkedHashMap<>();
            params.put("id", id);
            voExtendInfo = (GspVoExtendInfo) client.invoke(GspVoExtendInfo.class,
                getMethodString("getVoExtendInfo"), Lcm_SU, params, null);
            if(voExtendInfo != null) {
                idGspVoExtendInfo.put(id, voExtendInfo);
            }
        }
        return voExtendInfo;
    }

    @Override
    public GspVoExtendInfo getVoExtendInfoByConfigId(String configId) {
        if(configIdGspVoExtendInfo.containsKey(configId)){
            return configIdGspVoExtendInfo.get(configId);
        }
        try{
            RpcClient client= SpringBeanUtils.getBean(RpcClient.class);
            LinkedHashMap<String, Object> params = new LinkedHashMap<>();
            params.put("configId", configId);
            GspVoExtendInfo voExtendInfo=(GspVoExtendInfo)client.invoke(GspVoExtendInfo.class,
                getMethodString("getVoExtendInfoByConfigId"),Lcm_SU,params,null);
            putConfigIdGspVoExtendInfo(configId,voExtendInfo);
            return voExtendInfo;
        }catch (IncorrectResultSizeDataAccessException e){
            throw new CAFRuntimeException("", "",
                    "ConfigId为"+configId+"的vo有多个，请检查", null, ExceptionLevel.Error);
        }

    }

    @Override
    public List<GspVoExtendInfo> getVoExtendInfos() {
        RpcClient client= SpringBeanUtils.getBean(RpcClient.class);
        LinkedHashMap<String, Object> params = new LinkedHashMap<>();
        Type type=getViewModelListType();
        List<GspVoExtendInfo>  voExtendInfos=(List<GspVoExtendInfo>) client.invoke(type,
            getMethodString("getVoExtendInfos"),Lcm_SU,params,null);
        return voExtendInfos;
    }

    @Override
    public void saveGspVoExtendInfos(List<GspVoExtendInfo> infos) {
        infos.stream().forEach(item -> {
            if(item.getId() == null ||"".equals(item.getId()))
                throw new RuntimeException("id不允许为空");
        });
        RpcClient client= SpringBeanUtils.getBean(RpcClient.class);
        LinkedHashMap<String, Object> params = new LinkedHashMap<>();
        params.put("infos",infos);
        client.invoke(Void.class,
            getMethodString("saveGspVoExtendInfos"),Lcm_SU,params,null);
    }

    @Override
    public void deleteVoExtendInfo(String id) {
        if (!extendInfoRepository.existsById(id)) {
            return;
        }
        RpcClient client= SpringBeanUtils.getBean(RpcClient.class);
        LinkedHashMap<String, Object> params = new LinkedHashMap<>();
        params.put("id",id);
        client.invoke(Void.class,
            getMethodString("deleteVoExtendInfo"),Lcm_SU,params,null);
        if(id != null)
            idGspVoExtendInfo.remove(id);
        if (id == null || configIdGspVoExtendInfo == null
            || configIdGspVoExtendInfo.values().size() == 0) {
            return;
        }
        String deleteConfigId = null;
        for (GspVoExtendInfo item : configIdGspVoExtendInfo.values()) {
            if (item.getConfigId().equals(id) || item.getId().equals(id)) {
                deleteConfigId = item.getConfigId();
                break;
            }
        }
        if (deleteConfigId != null) {
            configIdGspVoExtendInfo.remove(deleteConfigId);
        }
    }

    @Override
    public List<GspVoExtendInfo> getVoId(String beId) {
        if(beId==null || "".equals(beId)){

            throw new RuntimeException("beId不能为空！");
        }
        RpcClient client= SpringBeanUtils.getBean(RpcClient.class);
        LinkedHashMap<String, Object> params = new LinkedHashMap<>();
        params.put("beId",beId);
        Type type=getViewModelListType();
        List<GspVoExtendInfo>  voExtendInfos=(List<GspVoExtendInfo>) client.invoke(type,
            getMethodString("getVoId"),Lcm_SU,params,null);
        return voExtendInfos;
    }

    private boolean putConfigIdGspVoExtendInfo(String id, GspVoExtendInfo info){
        if(id == null || info == null)
            return false;
        configIdGspVoExtendInfo.put(id,info);
        return true;
    }
    private String getMethodString(String methodName) {
        return VIEWWMODEL_SERVICE + "." + methodName;
    }

    private Type<List> getViewModelListType() {
        Type<List> type = new Type<>(List.class, GspVoExtendInfo.class);
        return type;
    }
}
