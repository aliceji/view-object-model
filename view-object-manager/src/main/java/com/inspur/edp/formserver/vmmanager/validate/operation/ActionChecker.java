/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.validate.operation;

import com.alibaba.druid.util.StringUtils;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.action.mappedcdp.MappedCdpActionParameter;

import static com.inspur.edp.cef.designtime.api.validate.common.CheckUtil.exception;
import static com.inspur.edp.cef.designtime.api.validate.common.CheckUtil.isLegality;

public class ActionChecker {
    private static ActionChecker actionChecker;

    public static ActionChecker getInstance() {
        if (actionChecker == null) {
            actionChecker = new ActionChecker();
        }
        return actionChecker;
    }

    protected final void opException(String code) {
        if (code == null || code.isEmpty())
            return;
        exception(code);
    }

    public void checkVMAction(GspViewModel vm) {
        for (ViewModelAction vmAction : vm.getActions()) {
            actionCodeInspaction(vmAction);
            parametersInspaction(vmAction);
        }
    }

    private void actionCodeInspaction(ViewModelAction vmAction) {
        if (vmAction == null)
            return;
        if(StringUtils.isEmpty(vmAction.getCode())){
            opException("视图操作对象[" + vmAction.getName() + "]的编号不能为空,请修改！");
        }
        if (isLegality(vmAction.getCode()))
            return;
        opException("视图操作对象编号[" + vmAction.getCode() + "]是Java关键字,请修改！");
    }

    private void parametersInspaction(ViewModelAction action) {
        for (Object par : action.getParameterCollection()) {
            par = par instanceof MappedCdpActionParameter ? (MappedCdpActionParameter) par : null;
            if (par == null)
                continue;
            if(StringUtils.isEmpty(((MappedCdpActionParameter) par).getParamCode())){
                opException("视图对象["+action.getCode()+"]的动作参数["+((MappedCdpActionParameter) par).getParamCode()+"]不能为空,请修改！");
            }
            if (isLegality(((MappedCdpActionParameter) par).getParamCode()))
                continue;
            opException("视图对象["+action.getCode()+"]的动作参数["+((MappedCdpActionParameter) par).getParamCode()+"]是Java关键字,请修改！");
        }
    }
}
