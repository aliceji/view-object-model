/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.commonstructure;

import com.inspur.edp.caf.cef.dt.spi.CommonStructureSchemaExtension;
import com.inspur.edp.caf.cef.schema.common.CommonStructureContext;
import com.inspur.edp.caf.cef.schema.common.CommonStructureInfo;
import com.inspur.edp.caf.cef.schema.structure.CommonStructure;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;

public class VMComStructureSchemaExtension implements CommonStructureSchemaExtension {

  @Override
  public String getEntityType() {
    return "GSPViewModel";
  }

  @Override
  public CommonStructure getCommonStructure(IMetadataContent iMetadataContent,
      CommonStructureContext commonStructureContext) {
    return (GspViewModel)iMetadataContent;
  }

  @Override
  public CommonStructureInfo getCommonStructureSummary(IMetadataContent iMetadataContent) {
    return null;
  }
}
