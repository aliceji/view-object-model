/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.pushchangesetevent;

import com.inspur.edp.formserver.viewmodel.pushchangesetargs.VmPushChangeSetArgs;
import com.inspur.edp.formserver.viewmodel.pushchangesetlistener.IVmPushChangeSetListener;
import io.iec.edp.caf.commons.event.EventManager;
import io.iec.edp.caf.commons.event.IEventListener;

public class PushVmChangeSetEventManager extends EventManager {

  @Override
  public String getEventManagerName() {
    return "PushVmChangeSetEventManager";
  }

  @Override
  public boolean isHandlerListener(IEventListener listener) {
    return listener instanceof IVmPushChangeSetListener;
  }

  /**
   * 注册事件
   * @param listener 监听者
   */
  @Override
  public void addListener(IEventListener listener) {
    if(!(listener instanceof  IVmPushChangeSetListener)) {
      throw new RuntimeException("指定的监听者没有实现 IVmPushChangeSetListener 接口！");
    }
    IVmPushChangeSetListener pushChangeSetListener = (IVmPushChangeSetListener)listener;
    this.addEventHandler(PushVmChangeSetType.vmPushChangeSet, pushChangeSetListener,"vmPushChangeSet");
  }

  /***
   * 注销事件
   * @param listener  监听者
   */
  @Override
  public void removeListener(IEventListener listener) {
    if(!(listener instanceof  IVmPushChangeSetListener)) {
      throw new RuntimeException("指定的监听者没有实现 IVmPushChangeSetListener 接口！");
    }
    IVmPushChangeSetListener pushChangeSetListener = (IVmPushChangeSetListener)listener;
    this.removeEventHandler(PushVmChangeSetType.vmPushChangeSet, pushChangeSetListener,"vmPushChangeSet");
  }

  // fire
  public final void firePushChangeSet(VmPushChangeSetArgs args) {
    this.fire(PushVmChangeSetType.vmPushChangeSet, args);
  }
}
