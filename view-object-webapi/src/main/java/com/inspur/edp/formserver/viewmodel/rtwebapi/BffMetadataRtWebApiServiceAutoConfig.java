/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.rtwebapi;

import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.context.annotation.Bean;

/**
 * 元数据config信息
 *
 * @author haoxiaofei
 */
public class BffMetadataRtWebApiServiceAutoConfig {
  @Bean
  public VMMetadataController getRtBffMetadataRtWebApi() {
    return new VMMetadataController();
  }

  @Bean
  public RESTEndpoint getBffRtMetadataRtWebApiEndpoint(VMMetadataController genService){
    return new RESTEndpoint(
        "/runtime/lcm/v1.0/bff/metadata",
        genService
    );
  }
}
